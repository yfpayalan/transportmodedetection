#include <jni.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>


#include <android/log.h>

#define  LOG_TAG    "VEHCLA"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
//#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
// If you want you can add other log definition for info, warning etc
//Now just call LOGD("Hello world") or LOGE("Number = %d", any_int) like printf in c.

#define LINESIZE 10000
#define MAX_UC -100000
#define MIN_UC 100000
#define OZELLIK_SAYISI 29
#define OKLID_BOYUTU 3
#define LABELSAYISI 18
#define LABELSAYISI1 8
#define LABELSAYISI2 10

#define FILENAMESIZE 100
#define EPSILON 0.1

#define atlamaMik 3600
#define kontrolAdet 6000
//int atlamaMik; ***
//int kontrolAdet; ***

#define MINRED 0
#define  MAXRED 1
#define  MININC 2
#define  MAXINC 3
#define  MIN 4
#define  MAX 5
#define  RANJ 6
#define  ARITMETIKORTALAMA 7
#define  HARMONIKORTALAMA 8
#define  KUADRATIKORTALAMA 9
#define  MOD 10
#define  MEDIAN 11
#define  STDSAPMA 12
#define  VARYANS 13
#define  ANLIKARITMETIKORT 14
#define  ANLIKKUADRATIKORT 15
#define  VARYASYONKATSAYI 16
#define  USTMAX_SIFIR 17
#define  ORTMAX_SIFIR 18
#define  ALTMAX_SIFIR 19
#define   USTMAX_ORTALAMA 20
#define  ORTMAX_ORTALAMA 21
#define  ALTMAX_ORTALAMA 22
#define  USTMAX_SIFIR_FREQ 23
#define  ORTMAX_SIFIR_FREQ 24
#define  ALTMAX_SIFIR_FREQ 25
#define  USTMAX_ORTALAMA_FREQ 26
#define  ORTMAX_ORTALAMA_FREQ 27
#define  ALTMAX_ORTALAMA_FREQ 28






/**
 *Hazırlanmıs olan ozellik cikarma fonksiyonlarının
 *sira ile fonksiyonlara parametre gonderip degerleri dosyaya yazdiriyoruz.
 */


double ortalamaAritmetikBul(double *degerler)
{
    int i;

    double toplam = 0.0;
    double ort;

    //kontrol adet pencere boyutunu verir.
    //pencerede ki degerlerin toplamı yapılmaktadır.
    for(i=0; i<kontrolAdet; i++)
    {
        toplam += degerler[i];
    }


    //    //daha onceden olusabilecek hataları kontrol etmek icin koydugumuz bir kontrol.
    //    if(toplam == HUGE_VAL)
    //    {
    //        printf("\n---------------------------------\n");
    //        for(i=0; i<kontrolAdet; i++)
    //        {
    //            printf("%f,",degerler[i]);
    //        }
    //        exit(0);
    //    }
    //cast etme islemi 23.11.2016 00:53 te eklenmistir.
    ort = toplam / (double) kontrolAdet;


    //normalize islemlerinde gerekli olan min max kontrolleri yapılmaktadır.

    return ort;

}


double ortalamaHarmonikBul(double *degerler)
{
    int i;

    double toplam = 0.0;
    double ort;

    // harmonik ortalama pencere boyutunun 1/deger 'ler toplamına esittir.
    for(i=0; i<kontrolAdet; i++)
    {
        if (degerler[i] != 0) {
            toplam += 1/degerler[i];
        }

    }




    ort = kontrolAdet/toplam;


    return ort;


}

double ortalamaKuadratikBul(double *degerler)
{
    int i;

    double toplam = 0.0;
    double ort;
    //degerlerin karelerinin pencere boyutuna bolumunun kare koku kuadratik ortalamayı vermektedir.
    for(i=0; i<kontrolAdet; i++)
    {
        toplam += (degerler[i]*degerler[i]);
    }

    ort = sqrt(toplam/kontrolAdet);


    return ort;
}

double standartSapma(double varyans)
{

    //standart sapma varyansın karekokune esittir.
    //ilk olarak varyans bulma foksiyonu cagirilmaktadir.
    double stdSapma = sqrt(varyans);


    return stdSapma;
}



double varyansBul(double *degerler,double ortalama)
{
    int i;

    double toplam = 0.0;
    double varyans;

    //degerler ile ortalama arasında ki farkın kareleri toplamının pencere boyutu - 1 e bolumu varyansı vermektedir.
    for(i=0; i<kontrolAdet; i++)
    {
        toplam += pow((degerler[i]-ortalama),2);
    }

    varyans = toplam/(kontrolAdet-1);


    return varyans;

}


void anlikOrtalamalar(double *degerler,int baslangic, double *anlikAritmetikOrt,double *anlikKuadratikOrt)
{
    int i;

    double fark;
    double aritmatikToplam , kuadratikToplam ;
    aritmatikToplam = kuadratikToplam = 0.0;


    //baslangici bilmek isteme sebebimiz kullandigimiz dizinin dongusel olmasidir.
    //bu yuzden artımdan sonra mod alıyoruz
    int j = baslangic;
    baslangic++;
    baslangic %= kontrolAdet;

    //degerler arasinda ki farkı alıp onlardan ortalama ozellikleri cikarilmaktadir.
    for(i=1; i<kontrolAdet; i++)
    {

        fark = degerler[baslangic]-degerler[j];
        aritmatikToplam += fark;
        kuadratikToplam += fark*fark;

        j = baslangic;
        baslangic++;
        baslangic %= kontrolAdet;

    }

    *anlikAritmetikOrt = aritmatikToplam/kontrolAdet;


    *anlikKuadratikOrt = sqrt(kuadratikToplam/kontrolAdet);


}


double varyasyonKatSayisi(double stdSapma,double ortalama,double min,double max)
{
    double vk;

    //!!Anlamlı olması için ortalamanın sıfır olmaması gerekir. Negatif sonuçlara da uygulanması mümkün olmakla beraber genel olarak varyasyon katsayısı pozitif olarak kullanılır.
    //!!Bu kavram genellikle 100le çarparak bir yüzdelik (yani %) olarak ifade edilir. Yüzdelik olarak ifade edilen varyasyon katsayısının mutlak değerine relatif standart sapma adı da verilmektedir.
    //biz sadece boluyoruz. gerekirse degistirilir. (cok kolay)
    //Olasılık kuramı ve istatistik bilim dallarında varyasyon katsayısı bir olasılık dağılımı için bir normalize edilmiş istatistiksel yayılma ölçüsüdür. Standart sapma, nin ortalama degerine orantisi
    if (ortalama == 0)
    {
        if(stdSapma>0)
        {
            vk = max;
        }
        else if (stdSapma<0)
        {
            vk = min;
        }
        else
        {
            vk = 0;
        }
    }
    else
    {
        vk = stdSapma/ortalama;
    }



    return vk;
}


double minBul(double *degerler)
{

    int i;

    double min;
    min = degerler[0];

    for(i=1; i<kontrolAdet; i++)
    {
        if (degerler[i]<min)
        {
            min = degerler[i];
        }

    }


    return min;

}

double maxBul(double *degerler)
{
    int i;

    double max;
    max = degerler[0];

    for(i=1; i<kontrolAdet; i++)
    {
        if (degerler[i]>max)
        {
            max = degerler[i];
        }

    }



    return max;
}



double ranjBul(double min, double max)
{
    double ranj;
    ranj = max - min;


    return ranj;
}

double minIncBul(double *degerler,int baslangic)  //minimum artis
{

    int i;

    double min = 100000.0,fark;
    int j = baslangic;

    //artım azalım bulmasından dolayı baslangıc ve bitis noktaları bizim icin onemlidir.
    //dongusel bir yapı kullandıgımız icin artımdan sonra mod alınır.
    baslangic++;
    baslangic %= kontrolAdet;

    for(i = 1; i<kontrolAdet; i++)
    {
        fark = degerler[baslangic]-degerler[j];
        if ((fark>0) && (fark<min))
        {
            min = fark;
        }
        j = baslangic;
        baslangic++;
        baslangic %= kontrolAdet;

    }

    if(min == 100000.0)
    {
        min = 0;
    }



    return min;
}

double maxIncBul(double *degerler,int baslangic)  //maximum artis
{

    int i;

    double max = 0,fark;
    int j = baslangic;
    //artım azalım bulmasından dolayı baslangıc ve bitis noktaları bizim icin onemlidir.
    //dongusel bir yapı kullandıgımız icin artımdan sonra mod alınır.
    baslangic++;
    baslangic %= kontrolAdet;

    for(i = 1; i<kontrolAdet; i++)
    {
        fark = degerler[baslangic]-degerler[j];
        if ((fark>0) && (fark>max))
        {
            max = fark;
        }
        j = baslangic;
        baslangic++;
        baslangic %= kontrolAdet;

    }


    return max;
}




double minRedBul(double *degerler,int baslangic)  //minimum azalis
{
    int i;

    double min = -100000.0,fark;
    int j = baslangic;
    baslangic++;
    baslangic %= kontrolAdet;

    for(i = 1; i<kontrolAdet; i++)
    {
        fark = degerler[baslangic]-degerler[j];
        if ((fark<0) && (fark>min))
        {
            min = fark;
        }
        j = baslangic;
        baslangic++;
        baslangic %= kontrolAdet;

    }
    if(min == -100000.0)
    {
        min = 0;
    }


    return min;
}



double maxRedBul(double *degerler,int baslangic)  //maximum artis
{
    int i;

    double max = 0,fark;
    int j = baslangic;
    baslangic++;
    baslangic %= kontrolAdet;

    for(i = 1; i<kontrolAdet; i++)
    {
        fark = degerler[baslangic]-degerler[j];
        if ((fark<0) && (fark<max))
        {
            max = fark;
        }
        j = baslangic;
        baslangic++;
        baslangic %= kontrolAdet;

    }

    return max;

}



int partition(double dizi[],int l, int r)
{
    double pivot = dizi[l];
    int i = l;
    int j = r+1;
    double tmp;

    do
    {
        do
        {
            i++;
        }
        while(i<j && dizi[i]<=pivot);

        do
        {
            j--;
        }
        while(dizi[j]>pivot);

        tmp = dizi[i];
        dizi[i]=dizi[j];
        dizi[j]=tmp;
    }
    while(i<j);

    tmp = dizi[i];
    dizi[i]=dizi[j];
    dizi[j]=tmp;

    tmp = dizi[l];
    dizi[l]=dizi[j];
    dizi[j]=tmp;

    return j;
}
void quickS(double dizi[],int l, int r)
{
    int pivotAddr;
    if(l<r)
    {
        pivotAddr = partition(dizi,l,r);
        quickS(dizi,l,pivotAddr-1);
        quickS(dizi,pivotAddr+1,r);
    }
}

void modMedianBul (double *degerler,double *median, double *mod)
{

    int i,j=0;

    double *tmp; // degerler dizisinin klonu
    double maxMod=1.0;
    double tmpMod=1.0;
    int maxModI=0;
    int bolum,kalan,indis;
    tmp = (double *) malloc(kontrolAdet*sizeof(double));


    //klonlama islemi
    for(i=0; i<kontrolAdet; i++)
    {
        tmp[i]=degerler[i];
    }


    quickS(tmp,0,kontrolAdet-1);
    /**
     buradaki islemlerin amaci cift sayida veri oldugu taktirde ortadaki iki sayiyi bulup ortalamasini almak. if kullanmak pahali geldi.
     */

    //median pencerenin ortanca degerine esittir
    bolum = kontrolAdet/2;
    kalan = kontrolAdet%2;
    indis = bolum + kalan - 1;
    *median = (tmp[bolum] + tmp[indis])/2;

    //cift sayıda olma durumunda kontrol edip ortalamasını almak lazım. BUNU YAPMIYORUZ

    //mod pencerede en çok hangi eleman ın oldugudur.

    j=1;
    while (j<kontrolAdet)
    {
        while(j<kontrolAdet && tmp[j]==tmp[j-1])
        {
            j++;
            tmpMod++;
        }
        if(tmpMod>maxMod)
        {
            maxModI=j-1;
            maxMod=tmpMod;
        }
        j++;
        tmpMod=1;
    }

    *mod = tmp[maxModI];

    free(tmp);
}

/**
 *mod da sinir noktasi olarak alinabilir. oneri
 */
void zeroCrossingWithEps(double *degerler,double sinirDeger, double *ortMax, double *ustMax, double *altMax)
{

    int i;


    for (i = 0; i < kontrolAdet; i++) {

        if (degerler[i] > sinirDeger + EPSILON) {
            (*ustMax)++;
        }else if(degerler[i] < sinirDeger - EPSILON){
            (*altMax)++;
        }else{
            (*ortMax)++;
        }
    }

    //ustmax normalize

}


void zeroCrossingWithEps_Freq(double *degerler,int baslangic,double sinirDeger, double *ortMax, double *ustMax, double *altMax)
{
    int i, j = baslangic;
    int sayac;
    int yer;
    int maxf[3]; // 0:up, 1:mid, 2:down
    maxf[0] = 0;
    maxf[1] = 0;
    maxf[2] = 0;


    //for olmıcak while gibi duruyor
    //bir baska yaklasim da for ile yap if kullan her sart icin diger iki sartin sayaclarini 0 la ve her 0 lamada sayaclari buyuk is
    sayac = 0;
    yer = 0;
    for ( i = 0; i < kontrolAdet; i++) {

        if (degerler[j] > (sinirDeger + EPSILON)) {
            if (yer != 0) {
                if (sayac > maxf[yer]) {
                    maxf[yer] = sayac;
                }
                sayac = 0;
                yer = 0;
            }

        } else if (degerler[j] < (sinirDeger - EPSILON)) {
            if (yer != 2) {
                if (sayac > maxf[yer]) {
                    maxf[yer] = sayac;
                }
                sayac = 0;
                yer = 2;
            }
        }else{
            if (yer != 1) {
                if (sayac > maxf[yer]) {
                    maxf[yer] = sayac;
                }
                sayac = 0;
                yer = 1;
            }
        }
        sayac++;
        j++;
        j %= kontrolAdet;
    }

    if (sayac > maxf[yer]) {
        maxf[yer] = sayac;
    }

    *ustMax = maxf[0];
    *ortMax = maxf[1];
    *altMax = maxf[2];


    //ustmax normalize

}

//konusmadan bu kısma dokunma
int Java_com_example_bdusun_vehicleclassification_MainActivity_islem(JNIEnv *env, jobject obj, jdoubleArray *degerlerJ, int baslangic, int argSayisi, jdoubleArray *sonuclar)
{
    //burda dizi veya matris falan tanımlanacak o mühim karar verildikten sonra halledilsin
    // degiskenler silinecek büyük ihtimal
    // bide walking kontrolü var tabi

    int x;
    double sonuc[OZELLIK_SAYISI];
    jdoubleArray sonucM;

    jdoubleArray degerlerM;
    double *degerler;
    //    FILE *fyaz;
    //    fyaz = fopen("LOGsonuc.txt","w");
    x = 0;
    //    int i;
    degerlerM = (*env)->GetObjectArrayElement(env, degerlerJ, 3);
    degerler = (*env)->GetDoubleArrayElements(env, degerlerM, NULL);
    //    yaz(degerler);
    //    exit(0);

    int result = -1;

    //burda while olacak durama göre doğrudan cikabilir walking icin
    //while()
    for(x=0; x < argSayisi; x++) {
        //        LOGD("x: %d ", x);
        degerlerM = (*env)->GetObjectArrayElement(env, degerlerJ, x);
        //        LOGD("DegerlerM atamasi");
        degerler = (*env)->GetDoubleArrayElements(env, degerlerM, NULL);
        //        LOGD("Degerler atamasi");
        sonucM = (*env)->GetObjectArrayElement(env, sonuclar, x);
        //        LOGD("SonucM atamasi");

        sonuc[USTMAX_SIFIR] = 0;
        sonuc[ORTMAX_SIFIR] = 0;
        sonuc[ALTMAX_SIFIR] = 0;

        sonuc[USTMAX_ORTALAMA] = 0;
        sonuc[ORTMAX_ORTALAMA] = 0;
        sonuc[ALTMAX_ORTALAMA] = 0;

        //        LOGD("sifir atamalari");

        sonuc[MIN] = minBul(degerler);
        //        LOGD("Minimum ");
        sonuc[MAX] = maxBul(degerler);
        //        LOGD("Maksimum ");
        sonuc[RANJ] = ranjBul(sonuc[MIN],sonuc[MAX]);
        //        LOGD("Ranj");
        sonuc[MININC] = minIncBul(degerler,baslangic);
        //        LOGD("Minimum increment ");
        sonuc[MAXINC] = maxIncBul(degerler,baslangic);
        //        LOGD("Maksimum increment ");
        sonuc[MINRED] = minRedBul(degerler,baslangic);
        //        LOGD("x: %d, m.Red: %.22E ", x, sonuc[MINRED]);
        //        LOGD("Minimum reduction ");
        sonuc[MAXRED] = maxRedBul(degerler,baslangic);
        //        LOGD("Maksimum reduction ");

        sonuc[ARITMETIKORTALAMA] = ortalamaAritmetikBul(degerler);
        //        LOGD("Aritmetik ortalama ");
        sonuc[HARMONIKORTALAMA] = ortalamaHarmonikBul(degerler);
        //        LOGD("Harmonik ortalama ");
        sonuc[KUADRATIKORTALAMA] = ortalamaKuadratikBul(degerler);
        //        LOGD("Kuadratik ortalama ");
        sonuc[VARYANS] = varyansBul(degerler,sonuc[ARITMETIKORTALAMA]);
        //        LOGD("Varyans ");
        sonuc[STDSAPMA] = standartSapma(sonuc[VARYANS]);
        //        LOGD("Standart sapma ");
        anlikOrtalamalar(degerler,baslangic,&sonuc[ANLIKARITMETIKORT],&sonuc[ANLIKKUADRATIKORT]);
        //        LOGD("Anlik ortalamalar ");
        sonuc[VARYASYONKATSAYI] = varyasyonKatSayisi(sonuc[STDSAPMA],sonuc[ARITMETIKORTALAMA],sonuc[MIN],sonuc[MAX]);
        //        LOGD("Varyasyon kat sayisi ");
        modMedianBul(degerler,&sonuc[MEDIAN],&sonuc[MOD]);
        //        LOGD("Mod medyan ");

        zeroCrossingWithEps(degerler, sonuc[MEDIAN], &sonuc[ORTMAX_SIFIR], &sonuc[USTMAX_SIFIR], &sonuc[ALTMAX_SIFIR]);
        //        LOGD("Zero crossing with eps zero ");
        //        LOGD("ust ort: %d, ort ort: %d, alt ort: %d \n",ustMax_Ortalama,ortMax_Ortalama,altMax_Ortalama);

        zeroCrossingWithEps(degerler, sonuc[ARITMETIKORTALAMA], &sonuc[ORTMAX_ORTALAMA], &sonuc[USTMAX_ORTALAMA], &sonuc[ALTMAX_ORTALAMA]);
        //        LOGD("Zero crossing with eps ortalama ");
        //        LOGD("ust ort: %d, ort ort: %d, alt ort: %d \n",ustMax_Ortalama,ortMax_Ortalama,altMax_Ortalama);
        //        LOGD("-------------\n");

        zeroCrossingWithEps_Freq(degerler, baslangic, sonuc[MEDIAN], &sonuc[ORTMAX_SIFIR_FREQ], &sonuc[USTMAX_SIFIR_FREQ], &sonuc[ALTMAX_SIFIR_FREQ]);
        //        LOGD("Zero crossing with eps frekans zero ");
        zeroCrossingWithEps_Freq(degerler, baslangic, sonuc[ARITMETIKORTALAMA], &sonuc[ORTMAX_ORTALAMA_FREQ], &sonuc[USTMAX_ORTALAMA_FREQ], &sonuc[ALTMAX_ORTALAMA_FREQ]);
        //        LOGD("Zero crossing with eps frekans ortalama ");
        //LOGD("\nMin: %lf, Max: %lf,",geometrikOrtalama,varyans);


//        LOGD("\nMin: %lf",sonuc[MIN]);
        (*env)->SetDoubleArrayRegion(env, sonucM, 0, OZELLIK_SAYISI, sonuc);

        if (x==0) {
            double zcth = ((sonuc[ORTMAX_ORTALAMA_FREQ] * 100)/(sonuc[ORTMAX_ORTALAMA_FREQ]+sonuc[ALTMAX_ORTALAMA_FREQ]+sonuc[USTMAX_ORTALAMA_FREQ]));
            //LOGD("std:%lf, zerocrossingdurma: %lf", sonuc[STDSAPMA], zcth);

            // return denebilir atama yerine
            if (sonuc[STDSAPMA]>=1.27) { //default 1.27
                result = 0;
            }
            else if ((sonuc[STDSAPMA]>=1.0) && (zcth<=7.22)) {//walking durmalilar icin
                result = 0;
            }
            else if ((zcth<=3.21)) {//walking
                result = 0;
            }
            else if (sonuc[STDSAPMA]<0.045) { // stationary; //bu halde walkingdeki durmalar bulunamayacak //bazı metrolar durma gorunecek
                result = 10;
            }
            //            else if (zcth>=78) { // stationary; bu halde tram,metroH'nin cogu stationary gorunur
            //                result = -2;
            //            }

        }

    }



    return result;
}








//#define SATIR_BOYUTU 10000
//#define SINIF_SAYISI 10
//
//#define WALKING_INDIS 0
//#define STATIONARY_INDIS 10
//
//#define LOG_KAYIT_BOYUT 15
//#define ST_VS_VEH 0.2

#define CHANGE_TO_STATIONARY_CONTROL 30
#define STATIONARY_AS_WALKING 5

#define STATIONARY_LIKE_VEHICULAR_THRESHOLD 20

//#define DUZELTME_ORANI 3
//#define THRESHOLD 3
//#define ARTI_EKSI 5
//#define HATA_SINIRI 5
//#define OVER_LAP 5


//#define DOSYA_ACMA_HATA 5



//????
int maxTekrarEdenArac(int *siniflar, int numOfClass)
{
    LOGD("MAX TEKRAR EDEN ARAC FONKSIYONUNDA");
    int i;
    int maxSinif = 1;
    int max = siniflar[1];
    int n = numOfClass - 1;
//    double veh_stat_orani;

    for (i = 2; i < n; i++) {
        if (siniflar[i] > max) {
            max = siniflar[i];
            maxSinif = i;
        }

    }


    //TODO: eğer max sayisi 0 olursa stat kontrole girebilir bu durum mümkün mü ona bak
    if(siniflar[n] >= STATIONARY_LIKE_VEHICULAR_THRESHOLD){
        //TODO: yeni kural eklenecek 30 sayma kuralıda çözülecek
        if (siniflar[n] > max){
            maxSinif = n;
        }
    }

//    LOGD("sayi: %d", siniflar[STATIONARY_INDIS]);
//    if (siniflar[STATIONARY_INDIS] > siniflar[maxSinif]){
//        veh_stat_orani = siniflar[maxSinif] / siniflar[STATIONARY_INDIS];
//        LOGD("ALI2");
//        if (veh_stat_orani < ST_VS_VEH){
//            maxSinif = STATIONARY_INDIS;
//        }
//    }

//    LOGD("ALI3");
    LOGD("MAX TEKRAR EDEN ARAC: %d", maxSinif);
    return maxSinif;
}


void arrERROR(void *p){
    if (!p){
        LOGD("Dizi olusturulamadi");
        exit(33);
    }
}


////extern "C"
void
Java_com_example_bdusun_vehicleclassification_MainActivity_heal(
        JNIEnv *env,
        jobject obj/* this */,
        jintArray results,
        int numOfResult,
        int numOfClass) {



    int *resultArray;
    resultArray = (*env)->GetIntArrayElements(env, results, NULL);
    int *classes;
    classes = (int *) malloc(numOfClass * sizeof(int));
    arrERROR(classes);
//    int *healResultsCopy;
//    healResultsCopy = (*env)->GetIntArrayElements(env, heal_results, NULL);
    int *healResultArray;
    healResultArray = (*env)->GetIntArrayElements(env, results, NULL);
//    healResultArray = (int *) malloc(numOfResult * sizeof(int));
//    arrERROR(healResultArray);

    int walkingIndis = 0;
    int stationaryIndis = numOfClass - 1;

    LOGD("HEAL FONKSIYONUNDA");

//    char *labels[]= {"walking","minibus","car","bus","tram","metroK","metroH","marmaray","metrobus","ferry","stationary"};
//    int siniflar[SINIF_SAYISI];
    int maxSinif;
    int bas, son;
    int indis;


    int eskiSayac=0;
//    int tahminSayisi;
    int i,j,k,sayac;

//    FILE *fRead;
//    FILE *fWrite;

//    char dosyaAdiRead[100];
//    //char dosyaAdiWrite[100];
//    char tmpSatir[SATIR_BOYUTU];

//    char labelCheck[20];

//    int *gercekDegerler;
//    int *tahminiDegerler;
//    int *duzeltilenDegerler;


    //log lama olaylari burada gerceklesecek
//    char **saat;
//    char **thDegeri;
//    int *thIndis;
//    char **wekaDegeri;
//    int *wekaIndis;
//    char **GPS1;
//    char **GPS2;

    int stationarySayac;


    // Prepare output file name
    //    degerler = (*env)->GetDoubleArrayElements(env, degerlerM, NULL);
//    const char *classifyResultPath = (*env)->GetStringUTFChars(env, jclassifyResultPath,NULL);

    // Prepare output file name
//    const char *outputFilePath = (*env)->GetStringUTFChars(env, joutputFilePath,NULL);

    //LOGD("Tahmin dosyasinin adini giriniz: ");
    //scanf("%s",dosyaAdiRead);

    //printf("Düzeltilmiş dosyasinin adini giriniz: ");
    //scanf("%s",dosyaAdiWrite);
//
//    LOGD("Healing file: %s", classifyResultPath);
//    LOGD("Heal output file: %s", outputFilePath);
//    fRead = fopen(classifyResultPath,"r");



//    tahminSayisi = 0;
//    while (fgets(tmpSatir, SATIR_BOYUTU, fRead))
//    {
//        tahminSayisi++;
//    }
    //    tahminSayisi--;?
//    LOGD("Tahmin Sayisi = %d\n",tahminSayisi);


//    fclose(fRead);

    LOGD("MATRISLER OLUSTURULUYOR");

//    gercekDegerler = (int *) calloc(tahminSayisi, sizeof(int));
//    tahminiDegerler = (int *) calloc(tahminSayisi, sizeof(int));
//    duzeltilenDegerler = (int *) calloc(tahminSayisi, sizeof(int));
//    thIndis = (int *) calloc(tahminSayisi, sizeof(int));
//    wekaIndis = (int *) calloc(tahminSayisi, sizeof(int));

//    saat = (char **) calloc(tahminSayisi, sizeof(char *));
//    arrERROR(saat);
//    for (i = 0; i < tahminSayisi; i++) {
//        saat[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
//        arrERROR(saat[i]);
//    }
//
//    thDegeri = (char **) calloc(tahminSayisi, sizeof(char *));
//    arrERROR(thDegeri);
//    for (i = 0; i < tahminSayisi; i++) {
//        thDegeri[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
//        arrERROR(thDegeri[i]);
//    }
//
//    wekaDegeri = (char **) calloc(tahminSayisi, sizeof(char *));
//    arrERROR(wekaDegeri);
//    for (i = 0; i < tahminSayisi; i++) {
//        wekaDegeri[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
//        arrERROR(wekaDegeri[i]);
//    }
//
//    GPS1 = (char **) calloc(tahminSayisi, sizeof(char *));
//    arrERROR(GPS1);
//    for (i = 0; i < tahminSayisi; i++) {
//        GPS1[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
//        arrERROR(GPS1[i]);
//    }
//
//    GPS2 = (char **) calloc(tahminSayisi, sizeof(char *));
//    arrERROR(GPS2);
//    for (i = 0; i < tahminSayisi; i++) {
//        GPS2[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
//        arrERROR(GPS2[i]);
//    }


//    LOGD("MATRISLER OLUSTURULDU.");
//    fRead = fopen(classifyResultPath, "r");


//    LOGD("DEGERLER OKUNUYOR");
//    for (i = 0; i < tahminSayisi; i++) {
//        fscanf(fRead, "%s %s %d %s %d %s %s", saat[i], thDegeri[i], &thIndis[i], wekaDegeri[i], &wekaIndis[i], GPS1[i], GPS2[i]);
//
//        if (thIndis[i] != -1){
//            tahminiDegerler[i] = thIndis[i];
//            duzeltilenDegerler[i] = thIndis[i];
//        } else{
//            tahminiDegerler[i] = wekaIndis[i];
//            duzeltilenDegerler[i] = wekaIndis[i];
//        }
//    }

//    LOGD("DEGERLER OKUNDU");


    // iyilestirmenin yapildigi kisim
    i=0;
    int saveFirst = 0;
    int isWalking = 1;

    for (k = 0; k < numOfClass; k++) { classes[k] = 0;}

    LOGD("IYILESTIRME ISLEMI BASLIYOR");

    while (i < numOfResult) {

        bas = saveFirst;

        // burada walkingten farkli olan degerleri buluyor sonra o arada kalanlardan en cok tekrar edeni duzeltiyor
        while ((i < numOfResult) && (resultArray[i] != walkingIndis) && (resultArray[i] != stationaryIndis)) {
            indis = resultArray[i];
            classes[indis]++;
            i++;
            isWalking = 0;
        }
        son = i; // dikkat son kisim tahmin sayisina esit olabilir


        //        /**DEBUG*/
        //        LOGD("Bas: %d, Son: %d, maxSinif: %d\n",(bas+1),(son+1),maxSinif);
        //        LOGD("Siniflar: \n");
        //        for (k = 0; k < SINIF_SAYISI; k++) { printf("%d, ", siniflar[k]);}
        //        LOGD("\n");
        /**DEBUG*/

        stationarySayac = 0;
        while((i < numOfResult) &&(resultArray[i] == stationaryIndis)){
            i++;
            classes[stationaryIndis]++;
            stationarySayac++;
        }



        if (isWalking){
            saveFirst = i;
            isWalking = 0;
        }else{

            //5 stationary ise walking gibi davranır
            if (stationarySayac >= STATIONARY_AS_WALKING) {
                maxSinif = maxTekrarEdenArac(classes, numOfClass); // en cok tekrar eden tasiti buluyor
                //burada duzeltme islemi gerceklesiyor
                for (j = bas; j < son; j++) {
                    healResultArray[j] = maxSinif;
                }
                //30 dk boyunca stationary ise dümdüz stat yapılıyor
                if (stationarySayac >= CHANGE_TO_STATIONARY_CONTROL) {
                    while((i < numOfResult) && (resultArray[i] != walkingIndis)){
                        healResultArray[i] = stationaryIndis;
                        i++;
                    }
                }
                saveFirst = i;
                for (k = 0; k < numOfClass; k++) { classes[k] = 0;}
                //burda duzeltme olacak DUZELTME1
                //duzeltme 2
            }else{
                if ((i < numOfResult)){
                    if (resultArray[i] == walkingIndis)
                    {
                        maxSinif = maxTekrarEdenArac(classes, numOfClass); // en cok tekrar eden tasiti buluyor
                        //burada duzeltme islemi gerceklesiyor
                        for (j = bas; j < son; j++) {
                            healResultArray[j] = maxSinif;
                        }
                        saveFirst = i;
                        for (k = 0; k < numOfClass; k++) { classes[k] = 0;}
                    }
                }else{
                    maxSinif = maxTekrarEdenArac(classes, numOfClass); // en cok tekrar eden tasiti buluyor
                    //burada duzeltme islemi gerceklesiyor
                    for (j = bas; j < numOfResult; j++) {
                        healResultArray[j] = maxSinif;
                    }
                }
            }
        }



        while((i < numOfResult) && (resultArray[i] == walkingIndis)){
            i++;
            saveFirst++;
            isWalking = 1;
            classes[walkingIndis]++;
        }
    }
    LOGD("IYILESTIRME ISLEMI TAMAMLANDI");

    //iyilestirme son



//    fWrite = fopen(outputFilePath, "w");
//
//    LOGD("SONUCLAR DOSYAYA YAZILIYOR");
//
//    fprintf(fWrite,"inst#,Threshold,RawWeka,Prediction,AfterHeal,Date,Healed?\n");
//
//    //printf("\n");
//    sayac = 0;
//    for(i=0; i<tahminSayisi; i++)
//    {
////        LOGD("i=%d, Threshold= %s, Weka= %s, tahmini= %s, duzeltilen= %s, ",(i+1),thDegeri[i],wekaDegeri[i], labels[tahminiDegerler[i]],labels[duzeltilenDegerler[i]]);
//        fprintf(fWrite, "%d,%s,%s,%s,%s,%s,",(i+1),thDegeri[i],wekaDegeri[i],labels[tahminiDegerler[i]],labels[duzeltilenDegerler[i]],saat[i]);
//        //        if(gercekDegerler[i]!=tahminiDegerler[i]) // healdan onceki hata
//        //        {
//        //            LOGD("+");
//        //            fprintf(fWrite, "+");
//        //            //sayac++;
//        //        }
//        //        printf(",");
//        //        fprintf(fWrite, ",");
//        if(duzeltilenDegerler[i] != tahminiDegerler[i]) // duzeltme - healdan sonraki duzeltme
//        {
////            LOGD("*");
//            fprintf(fWrite, "*");
//            sayac++;
//        }
//        fprintf(fWrite, "\n");
//        //        printf(",");
//        //        fprintf(fWrite, ",");
//        //        if(gercekDegerler[i] != duzeltilenDegerler[i]){ // hatali duzeltme
//        //            LOGD("er");
//        //            fprintf(fWrite, "er");
//        //            sayac++;
//        //        }
//        //        printf("\n");
//        //        fprintf(fWrite, "\n");
//    }
//    //    float basari = ( (float)(tahminSayisi-sayac) / (tahminSayisi))*100;
//    //    float eskiBasari = ((float)(tahminSayisi-eskiSayac) / tahminSayisi)*100;
//    //    LOGD("hataSayisi=%d, eskiBasariOrani=%f, yeniBasariOrani=%f\n",sayac,eskiBasari, basari);
//    //    fprintf(fWrite,"\nhataSayisi=%d, eskiBasariOrani=%f, yeniBasariOrani=%f\n",sayac,eskiBasari, basari);
//    LOGD("DOSYAYA YAZMA ISLEMI TAMAMLANDI");
//    LOGD("Degistirilen Deger Sayisi: %d", sayac);
//
//    free(gercekDegerler);
//    free(tahminiDegerler);
//    free(duzeltilenDegerler);
//    LOGD("DUZELTME ISLEMINDE KULLANILAN DIZILER FREE EDILDI");
//    for (i = 0; i < tahminSayisi; i++) {
//        free(saat[i]);
//        free(thDegeri[i]);
//        free(wekaDegeri[i]);
//        free(GPS1[i]);
//        free(GPS2[i]);
//    }
//    LOGD("CIKTI VERMEK ICIN KULLANILAN MATRISLERIN SUTUNLARI FREE EDILDI");
//    free(saat);
//    free(thDegeri);
//    free(thIndis);
//    free(wekaDegeri);
//    free(wekaIndis);
//    free(GPS1);
//    free(GPS2);
//
//    LOGD("CIKTI VERMEK ICIN KULLANILAN MATRISLERIN SATIRLARI FREE EDILDI");
//
//    fclose(fRead);
//    fclose(fWrite);

//    (*env)->SetDoubleArrayRegion(env, healResultsCopy, 0, numOfResult, healResultArray); ??? TODO:BURAYI YAPMAK GEREKEBILIR


    (*env)->SetIntArrayRegion(env, results, 0, numOfResult, healResultArray);

//    for (i = 0; i < numOfResult; ++i) {
//        LOGD("healResultarray: %d", healResultArray[i]);
//    }
    LOGD("HEAL FONKSIYONU TAMAMLANDI");
    //return 0;
}



//#define SINIF_SAYISI 10
//
//#define WALKING_INDIS 0
//#define STATIONARY_INDIS 10
//
#define LOG_KAYIT_BOYUT 15
//#define ST_VS_VEH 0.2
//
#define CHANGE_TO_STATIONARY_CONTROL 30
#define STATIONARY_AS_WALKING 5
/**
 * TODO burada dosyalar okunarak özellik çıkarılacaktır dosyaya yazılacaktır
*/








#define SATIR_BOYUTU 10000
#define SINIF_SAYISI 10

#define WALKING_INDIS 0
#define STATIONARY_INDIS 9

#define LOG_KAYIT_BOYUT 15
//#define ST_VS_VEH 0.2

#define CHANGE_TO_STATIONARY_CONTROL 30

//#define DUZELTME_ORANI 3
//#define THRESHOLD 3
//#define ARTI_EKSI 5
//#define HATA_SINIRI 5
//#define OVER_LAP 5


//#define DOSYA_ACMA_HATA 5


//????
//
//int maxTekrarEdenAracDosya(int *siniflar)
//{
//    LOGD("MAX TEKRAR EDEN ARAC FONKSIYONUNDA");
//    int i;
//    int maxSinif = 1;
//    int max = siniflar[1];
//    int n = SINIF_SAYISI - 1;
////    double veh_stat_orani;
//
//    for (i = 2; i < n; i++) {
//        if (siniflar[i] > max) {
//            max = siniflar[i];
//            maxSinif = i;
//        }
//
//    }
//
////    LOGD("sayi: %d", siniflar[STATIONARY_INDIS]);
////    if (siniflar[STATIONARY_INDIS] > siniflar[maxSinif]){
////        veh_stat_orani = siniflar[maxSinif] / siniflar[STATIONARY_INDIS];
////        LOGD("ALI2");
////        if (veh_stat_orani < ST_VS_VEH){
////            maxSinif = STATIONARY_INDIS;
////        }
////    }
//
////    LOGD("ALI3");
//    LOGD("MAX TEKRAR EDEN ARAC: %d", maxSinif);
//    return maxSinif;
//}
////
////
////void arrERROR(void *p){
////    if (!p){
////        LOGD("Dizi olusturulamadi");
////        exit(33);
////    }
////}
////
////
////extern "C"
//void
//Java_com_example_bdusun_vehicleclassification_MainActivity_healFile(
//        JNIEnv *env,
//        jobject obj/* this */,
//        jstring jclassifyResultPath,
//        jstring joutputFilePath
//        ) {
//
//    LOGD("HEAL FONKSIYONUNDA");
//
//    char *labels[]= {"walking","minibus","car","bus","tram","metroK","metroH","marmaray","metrobus","stationary"};
//    int siniflar[SINIF_SAYISI];
////    int *siniflar;
////    siniflar = (int *) malloc(numOffClasses* sizeof(int));
////    arrERROR(siniflar);
//
//    int maxSinif;
//    int bas, son;
//    int indis;
//
//
////    int walkingIndis = 0;
////    int stationaryIndis = SINIF_SAYISI - 1;
//
//    int eskiSayac=0;
//    int tahminSayisi;
//    int i,j,k,sayac;
//
//    FILE *fRead;
//    FILE *fWrite;
//
//    char dosyaAdiRead[100];
//    //char dosyaAdiWrite[100];
//    char tmpSatir[SATIR_BOYUTU];
//
//    char labelCheck[20];
//
//    int *gercekDegerler;
//    int *tahminiDegerler;
//    int *duzeltilenDegerler;
//
//
//    //log lama olaylari burada gerceklesecek
//
////    char **saat;
////    char **realDegeri;
//    char tmpChar[100];
//    int tmpInt;
//
//
////    int *thIndis;
////    char **wekaDegeri;
////    int *wekaIndis;
////    char **GPS1;
////    char **GPS2;
//
//    int stationarySayac;
//
//
//    // Prepare output file name
//    //    degerler = (*env)->GetDoubleArrayElements(env, degerlerM, NULL);
//    const char *classifyResultPath = (*env)->GetStringUTFChars(env, jclassifyResultPath,NULL);
//
//    // Prepare output file name
//    const char *outputFilePath = (*env)->GetStringUTFChars(env, joutputFilePath,NULL);
//
//    //LOGD("Tahmin dosyasinin adini giriniz: ");
//    //scanf("%s",dosyaAdiRead);
//
//    //printf("Düzeltilmiş dosyasinin adini giriniz: ");
//    //scanf("%s",dosyaAdiWrite);
//
//    LOGD("Healing file: %s", classifyResultPath);
//    LOGD("Heal output file: %s", outputFilePath);
//    fRead = fopen(classifyResultPath,"r");
//
//
//
//    tahminSayisi = 0;
//    while (fgets(tmpSatir, SATIR_BOYUTU, fRead))
//    {
//        tahminSayisi++;
//    }
//    //    tahminSayisi--;?
//    LOGD("Tahmin Sayisi = %d\n",tahminSayisi);
//
//
//    fclose(fRead);
//
//    LOGD("MATRISLER OLUSTURULUYOR");
//
//    gercekDegerler = (int *) calloc(tahminSayisi, sizeof(int));
//    arrERROR(gercekDegerler);
//    tahminiDegerler = (int *) calloc(tahminSayisi, sizeof(int));
//    arrERROR(tahminiDegerler);
//    duzeltilenDegerler = (int *) calloc(tahminSayisi, sizeof(int));
//    arrERROR(duzeltilenDegerler);
//
//
////    thIndis = (int *) calloc(tahminSayisi, sizeof(int));
////    wekaIndis = (int *) calloc(tahminSayisi, sizeof(int));
////
////    saat = (char **) calloc(tahminSayisi, sizeof(char *));
////    arrERROR(saat);
////    for (i = 0; i < tahminSayisi; i++) {
////        saat[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
////        arrERROR(saat[i]);
////    }
////
////    thDegeri = (char **) calloc(tahminSayisi, sizeof(char *));
////    arrERROR(thDegeri);
////    for (i = 0; i < tahminSayisi; i++) {
////        thDegeri[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
////        arrERROR(thDegeri[i]);
////    }
////
////    wekaDegeri = (char **) calloc(tahminSayisi, sizeof(char *));
////    arrERROR(wekaDegeri);
////    for (i = 0; i < tahminSayisi; i++) {
////        wekaDegeri[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
////        arrERROR(wekaDegeri[i]);
////    }
////
////    GPS1 = (char **) calloc(tahminSayisi, sizeof(char *));
////    arrERROR(GPS1);
////    for (i = 0; i < tahminSayisi; i++) {
////        GPS1[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
////        arrERROR(GPS1[i]);
////    }
////
////    GPS2 = (char **) calloc(tahminSayisi, sizeof(char *));
////    arrERROR(GPS2);
////    for (i = 0; i < tahminSayisi; i++) {
////        GPS2[i] = (char *) calloc(LOG_KAYIT_BOYUT, sizeof(char));
////        arrERROR(GPS2[i]);
////    }
////
//
////    LOGD("MATRISLER OLUSTURULDU.");
//    fRead = fopen(classifyResultPath, "r");
//
//
//    LOGD("DEGERLER OKUNUYOR");
//    for (i = 0; i < tahminSayisi; i++) {
//        fscanf(fRead, "%d %s %d %s %d %d", &tmpInt, tmpChar, &gercekDegerler[i], tmpChar, &tahminiDegerler[i], &tmpInt);
//
//        duzeltilenDegerler[i] = tahminiDegerler[i];
////        if (thIndis[i] != -1){
////            tahminiDegerler[i] = thIndis[i];
////            duzeltilenDegerler[i] = thIndis[i];
////        } else{
////            tahminiDegerler[i] = wekaIndis[i];
////            duzeltilenDegerler[i] = wekaIndis[i];
////        }
//    }
//
//    LOGD("DEGERLER OKUNDU");
//
//
//    // iyilestirmenin yapildigi kisim
//    i=0;
//    int saveFirst = 0;
//    int isWalking = 1;
//
//    for (k = 0; k < SINIF_SAYISI; k++) { siniflar[k] = 0;}
//
//    LOGD("IYILESTIRME ISLEMI BASLIYOR");
//
//    while (i < tahminSayisi) {
//
//        bas = saveFirst;
//
//        // burada walkingten farkli olan degerleri buluyor sonra o arada kalanlardan en cok tekrar edeni duzeltiyor
//        while ((i < tahminSayisi) && (tahminiDegerler[i] != WALKING_INDIS) && (tahminiDegerler[i] != STATIONARY_INDIS)) {
//            indis = tahminiDegerler[i];
//            siniflar[indis]++;
//            i++;
//            isWalking = 0;
//        }
//        son = i; // dikkat son kisim tahmin sayisina esit olabilir
//
//
//        //        /**DEBUG*/
//        //        LOGD("Bas: %d, Son: %d, maxSinif: %d\n",(bas+1),(son+1),maxSinif);
//        //        LOGD("Siniflar: \n");
//        //        for (k = 0; k < SINIF_SAYISI; k++) { printf("%d, ", siniflar[k]);}
//        //        LOGD("\n");
//        /**DEBUG*/
//
//        stationarySayac = 0;
//        while((i < tahminSayisi) &&(tahminiDegerler[i] == STATIONARY_INDIS)){
//            i++;
//            siniflar[STATIONARY_INDIS]++;
//            stationarySayac++;
//        }
//
//
//
//        if (isWalking){
//            saveFirst = i;
//            isWalking = 0;
//        }else{
//            if (stationarySayac >= STATIONARY_AS_WALKING) {
//                maxSinif = maxTekrarEdenAracDosya(siniflar); // en cok tekrar eden tasiti buluyor
//                //burada duzeltme islemi gerceklesiyor
//                for (j = bas; j < son; j++) {
//                    duzeltilenDegerler[j] = maxSinif;
//                }
//
//                if (stationarySayac >= CHANGE_TO_STATIONARY_CONTROL) {
//                    while((i < tahminSayisi) && (tahminiDegerler[i] != WALKING_INDIS)){
//                        duzeltilenDegerler[i] = STATIONARY_INDIS;
//                        i++;
//                    }
//
//                }
//                saveFirst = i;
//                for (k = 0; k < SINIF_SAYISI; k++) { siniflar[k] = 0;}
//                //burda duzeltme olacak DUZELTME1
//                //duzeltme 2
//            }else{
//                if ((i < tahminSayisi)){
//                    if (tahminiDegerler[i] == WALKING_INDIS)
//                    {
//                        maxSinif = maxTekrarEdenAracDosya(siniflar); // en cok tekrar eden tasiti buluyor
//                        //burada duzeltme islemi gerceklesiyor
//                        for (j = bas; j < son; j++) {
//                            duzeltilenDegerler[j] = maxSinif;
//                        }
//                        saveFirst = i;
//                        for (k = 0; k < SINIF_SAYISI; k++) { siniflar[k] = 0;}
//                    }
//                }else{
//                    maxSinif = maxTekrarEdenAracDosya(siniflar); // en cok tekrar eden tasiti buluyor
//                    //burada duzeltme islemi gerceklesiyor
//                    for (j = bas; j < tahminSayisi; j++) {
//                        duzeltilenDegerler[j] = maxSinif;
//                    }
//                }
//            }
//        }
//
//
//
//        while((i < tahminSayisi) && (tahminiDegerler[i] == WALKING_INDIS)){
//            i++;
//            saveFirst++;
//            isWalking = 1;
//            siniflar[WALKING_INDIS]++;
//        }
//    }
//    LOGD("IYILESTIRME ISLEMI TAMAMLANDI");
//
//    //iyilestirme son
//
//
//
//    fWrite = fopen(outputFilePath, "w");
//
//    LOGD("SONUCLAR DOSYAYA YAZILIYOR");
//
//    fprintf(fWrite,"inst#, Real Name, Real Id, Prediction Name, Prediction Id, Healed Name, Healed Id\n");
//
//    //printf("\n");
//    sayac = 0;
//    int sayacTahmini = 0;
//    int sayacDuzeltilen = 0;
//    for(i=0; i<tahminSayisi; i++)
//    {
////        LOGD("i=%d, Threshold= %s, Weka= %s, tahmini= %s, duzeltilen= %s, ",(i+1),thDegeri[i],wekaDegeri[i], labels[tahminiDegerler[i]],labels[duzeltilenDegerler[i]]);
//        fprintf(fWrite, "%d,%s,%d,%s,%d,%s, %d,",(i+1), labels[gercekDegerler[i]],gercekDegerler[i],labels[tahminiDegerler[i]], tahminiDegerler[i],labels[duzeltilenDegerler[i]],duzeltilenDegerler[i]);
//        //        if(gercekDegerler[i]!=tahminiDegerler[i]) // healdan onceki hata
//        //        {
//        //            LOGD("+");
//        //            fprintf(fWrite, "+");
//        //            //sayac++;
//        //        }
//        //        printf(",");
//        //        fprintf(fWrite, ",");
//        if(duzeltilenDegerler[i] != tahminiDegerler[i]) // duzeltme - healdan sonraki duzeltme
//        {
////            LOGD("*");
//            fprintf(fWrite, "*,");
//            sayac++;
//        }
//
//        if (gercekDegerler[i] == tahminiDegerler[i]){
//            sayacTahmini++;
//        }
//
//        if (gercekDegerler[i] == duzeltilenDegerler[i]){
//            sayacDuzeltilen++;
//        }
//
//        fprintf(fWrite, "\n");
//        //        printf(",");
//        //        fprintf(fWrite, ",");
//        //        if(gercekDegerler[i] != duzeltilenDegerler[i]){ // hatali duzeltme
//        //            LOGD("er");
//        //            fprintf(fWrite, "er");
//        //            sayac++;
//        //        }
//        //        printf("\n");
//        //        fprintf(fWrite, "\n");
//    }
//    //    float basari = ( (float)(tahminSayisi-sayac) / (tahminSayisi))*100;
//    //    float eskiBasari = ((float)(tahminSayisi-eskiSayac) / tahminSayisi)*100;
//    //    LOGD("hataSayisi=%d, eskiBasariOrani=%f, yeniBasariOrani=%f\n",sayac,eskiBasari, basari);
//    //    fprintf(fWrite,"\nhataSayisi=%d, eskiBasariOrani=%f, yeniBasariOrani=%f\n",sayac,eskiBasari, basari);
//    LOGD("DOSYAYA YAZMA ISLEMI TAMAMLANDI");
//    LOGD("Degistirilen Deger Sayisi: %d", sayac);
//
//    fprintf(fWrite, "Duzeltilen deger sayisi: %d,", sayac);
//    fprintf(fWrite, "Tahmini Basari Orani: %lf,", (double) (sayacTahmini/tahminSayisi));
//    fprintf(fWrite, "İyileştirilen Basari Orani: %lf,", (double) (sayacDuzeltilen/tahminSayisi));
//
//    free(gercekDegerler);
//    free(tahminiDegerler);
//    free(duzeltilenDegerler);
//    LOGD("DUZELTME ISLEMINDE KULLANILAN DIZILER FREE EDILDI");
////    for (i = 0; i < tahminSayisi; i++) {
////        free(saat[i]);
////        free(thDegeri[i]);
////        free(wekaDegeri[i]);
////        free(GPS1[i]);
////        free(GPS2[i]);
////    }
////    LOGD("CIKTI VERMEK ICIN KULLANILAN MATRISLERIN SUTUNLARI FREE EDILDI");
////    free(saat);
////    free(thDegeri);
////    free(thIndis);
////    free(wekaDegeri);
////    free(wekaIndis);
////    free(GPS1);
////    free(GPS2);
//
////    LOGD("CIKTI VERMEK ICIN KULLANILAN MATRISLERIN SATIRLARI FREE EDILDI");
//
//    fclose(fRead);
//    fclose(fWrite);
//    LOGD("HEAL FONKSIYONU TAMAMLANDI");
//    //return 0;
//}
//////{
//////    double nSayi = (sayi-min)/(max-min);
//////
//////    return nSayi;
//////}
//////
//////void normalize(double *tmp, OZELLIK_MIN  fMin, OZELLIK_MAX fMax, FILE **fWrite)
//////{
//////    double normalizeDeger;
//////    int i;
//////
//////    for(i = 0; i < OZELLIK_SAYISI; i++)
//////    {
//////        normalizeDeger = normalizeYap(tmp[i],fMin.ozellik[i],fMax.ozellik[i]);
//////        // printf("%lf,",normalizeDeger);
//////        // printf("\n%lf,%lf,%lf,%lf,",tmp[i],fMin.ozellik[i],fMax.ozellik[i],normalizeDeger);
//////        fprintf(*fWrite, "%lf,", normalizeDeger);
//////    }
//////
//////}
//////
////
////
//
//
//void islemDosya(double **degerler,int baslangic, int argSayisi, char ayrac, FILE **fWrite)
//{
//
//    int x;
//
//    double min;
//    double max;
//    double ranj;
//    double minInc;
//    double maxInc;
//    double minRed;
//    double maxRed;
//    double median;
//    double mod;
//    double aritmetikOrtalama;
//    double harmonikOrtalama;
//    double kuadratikOrtalama;
//    double stdSapma;
//    double varyans;
//    double anlikAritmetikOrt;
//    double anlikKuadratikOrt;
//    double varyasyonKatSayi;
//
//    double ustMax_Sifir = 0;
//    double ortMax_Sifir = 0;
//    double altMax_Sifir = 0;
//
//    double ustMax_Ortalama = 0;
//    double ortMax_Ortalama = 0;
//    double altMax_Ortalama = 0;
//
//    double ustMax_Sifir_Freq;
//    double ortMax_Sifir_Freq;
//    double altMax_Sifir_Freq;
//
//    double ustMax_Ortalama_Freq;
//    double ortMax_Ortalama_Freq;
//    double altMax_Ortalama_Freq;
//
//
//    for(x=0; x<argSayisi; x++)
//    {
//        ustMax_Sifir = 0;
//        ortMax_Sifir = 0;
//        altMax_Sifir = 0;
//
//        ustMax_Ortalama = 0;
//        ortMax_Ortalama = 0;
//        altMax_Ortalama = 0;
//
//        min = minBul(degerler[x]);
//        max = maxBul(degerler[x]);
//        ranj = ranjBul(min,max);
//        minInc = minIncBul(degerler[x],baslangic);
//        maxInc = maxIncBul(degerler[x],baslangic);
//        minRed = minRedBul(degerler[x],baslangic);
//        maxRed = maxRedBul(degerler[x],baslangic);
//
//        aritmetikOrtalama = ortalamaAritmetikBul(degerler[x]);
//        harmonikOrtalama = ortalamaHarmonikBul(degerler[x]);
//        kuadratikOrtalama = ortalamaKuadratikBul(degerler[x]);
//        varyans = varyansBul(degerler[x],aritmetikOrtalama);
//        stdSapma = standartSapma(varyans);
//        anlikOrtalamalar(degerler[x],baslangic,&anlikAritmetikOrt,&anlikKuadratikOrt);
//        varyasyonKatSayi = varyasyonKatSayisi(stdSapma,aritmetikOrtalama,min,max);
//        modMedianBul(degerler[x],&median,&mod);
//
//        zeroCrossingWithEps(degerler[x], median, &ortMax_Sifir, &ustMax_Sifir, &altMax_Sifir);
//        //        printf("ust ort: %d, ort ort: %d, alt ort: %d \n",ustMax_Ortalama,ortMax_Ortalama,altMax_Ortalama);
//
//        zeroCrossingWithEps(degerler[x], aritmetikOrtalama, &ortMax_Ortalama, &ustMax_Ortalama, &altMax_Ortalama);
//
//        //        printf("ust ort: %d, ort ort: %d, alt ort: %d \n",ustMax_Ortalama,ortMax_Ortalama,altMax_Ortalama);
//        //        printf("-------------\n");
//        zeroCrossingWithEps_Freq(degerler[x], baslangic, median, &ortMax_Sifir_Freq, &ustMax_Sifir_Freq, &altMax_Sifir_Freq);
//        zeroCrossingWithEps_Freq(degerler[x], baslangic, aritmetikOrtalama, &ortMax_Ortalama_Freq, &ustMax_Ortalama_Freq, &altMax_Ortalama_Freq);
//        //printf("\nMin: %lf, Max: %lf,",geometrikOrtalama,varyans);
//
////                fprintf(*fWrite,"%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,",minRed,maxRed,minInc,maxInc,min,max,ranj,aritmetikOrtalama,harmonikOrtalama,kuadratikOrtalama,mod,median,stdSapma,varyans,anlikAritmetikOrt,anlikKuadratikOrt,varyasyonKatSayi,ustMax_Sifir,ortMax_Sifir,altMax_Sifir, ustMax_Ortalama,ortMax_Ortalama,altMax_Ortalama,ustMax_Sifir_Freq,ortMax_Sifir_Freq,altMax_Sifir_Freq,ustMax_Ortalama_Freq,ortMax_Ortalama_Freq,altMax_Ortalama_Freq);
//        fprintf(*fWrite,"%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,%.15E,",minRed,maxRed,minInc,maxInc,min,max,ranj,aritmetikOrtalama,harmonikOrtalama,kuadratikOrtalama,mod,median,stdSapma,varyans,anlikAritmetikOrt,anlikKuadratikOrt,varyasyonKatSayi,ustMax_Sifir,ortMax_Sifir,altMax_Sifir, ustMax_Ortalama,ortMax_Ortalama,altMax_Ortalama,ustMax_Sifir_Freq,ortMax_Sifir_Freq,altMax_Sifir_Freq,ustMax_Ortalama_Freq,ortMax_Ortalama_Freq,altMax_Ortalama_Freq);
//    }
//
//}
//
//
//
//char denemeDosya[1000];
//
//int isCSV(char *dosyaAdi)
//{
//    int i = (int) strlen(dosyaAdi);
//    i--;
//    if ((dosyaAdi[i]=='v')&&(dosyaAdi[--i]=='s')&&(dosyaAdi[--i]=='c')&&(dosyaAdi[--i]=='.'))
//    {
//        return 1;
//    }
//    else
//    {
//        return 0;
//    }
//}
///**
// *dosya isimleri verilirken
// *gönderilen label tipine göre _ kontrolu yapılır.
// *toplanıp etiketlenmis olan yeni verilerde sadece LabelType 2 icin dogru sonuc vermektedir.
// */
//void labelBul(char *dosyaAdi, char *label, int labelType)
//{
//    int i=0,j=0;
//    if(labelType == 1)
//    {
//        while(dosyaAdi[i] != '_')
//        {
//            label[i] = dosyaAdi[i];
//            i++;
//        }
//        label[i] = '\0';
//
//    }
//    else {
//        while(dosyaAdi[i] != '_')
//        {
//            i++;
//        }
//        i++;
//        while(dosyaAdi[i] != '_')
//        {
//            label[j] = dosyaAdi[i];
//            i++;
//            j++;
//        }
//        if (labelType == 0)
//        {
//            label[j] = dosyaAdi[i]; // _'da kalmamasi icin
//            i++;
//            j++;
//            while(dosyaAdi[i] != '_')
//            {
//                label[j] = dosyaAdi[i];
//                i++;
//                j++;
//            }
//        }
//        label[j] = '\0';
//    }
//}
//
////TODO bunun başına rütüel olan şeyler yazılacak
//void
//Java_com_example_bdusun_vehicleclassification_MainActivity_arffExtract(
//        JNIEnv *env,
//        jobject obj/* this */,
//        jstring jclassifyResultPath,
//        jstring joutputFilePath,
//        jstring joutputFileName
//        )
//{
//    int i,j,k,x,y;
////    int atlamaMik;
//
//    int degerlerMin_MaxBoyut; //cikarilan ozelliklerin min max degerlerini sakladigimiz structure dizisinin boyutu
//    int indis;
//    double oklidDegeri;
//
//
//    const char *classifyResultPath = (*env)->GetStringUTFChars(env, jclassifyResultPath, NULL);
////
////    // Prepare output file name
//    const char *outputFilePath = (*env)->GetStringUTFChars(env, joutputFilePath, NULL);
//    const char *outputFileName = (*env)->GetStringUTFChars(env, joutputFileName, NULL);
//
//
//    int r ;
//    int argSayisi = 12; // islenecek olan sensor verilerinin column sayisi.
//
//    char dosyaAdiOkuma[100];
//    char dosyaAdiYazma[500] ;
//
//    dosyaAdiYazma[0] = 0;
//
//
//    char tmpSatir[LINESIZE];
//    char ayrac;
//    int kelimeUzunlugu;
//
//    double **degerler; // kullandigimiz pencere
//
//
//    int degerSayisi;
//
//
//    //double donusumu sirasinda kullandigimiz degiskenler
//    double tmpDeger = 0.0;
//    double dDonustur = 1.0;
//    double isaret = 1.0;
//
//
//    int sure;
//    int frekans;
//    int atlamaOrani;
//    int labelType = 0;
//
//    //cikardigimiz ozelllikleri arff icerisinde belirtebilmemiz icin var
//    char *ozellikler[] = {"MINRED","MAXRED","MININC","MAXINC","MINVAL","MAXVAL", "RANGE",
//                          "ARTMEAN","HARMEAN", "QUADMEAN", "MOD", "MEDIAN", "STD","VAR", "IEARTMEAN",
//                          "IEQUADMEAN", "COV", "ZC_ust_sifir","ZC_ort_sifir","ZC_alt_sifir", "ZC_ust_avg","ZC_ort_avg","ZC_alt_avg","ZC_ust_sifir_freq","ZC_ort_sifir_freq","ZC_alt_sifir_freq", "ZC_ust_avg_freq","ZC_ort_avg_freq","ZC_alt_avg_freq"
//    };
//    // label tiplerine gore ayrı dizilerde labellar var bu labellar arff dosyasinin içerisinde belirtilmek zorunda
//    // char *labels[] = {"walking","car","bus","tram","metro1","metro2","marmaray","metrobus","bicycle","ferry","n"};
//    char *labels[] = {"walking_walking","minibus_sitting","minibus_standing","car_car","bus_sitting","tram_sitting","metroH_sitting","metroK_sitting","marmaray_sitting","metrobus_sitting","ferry_sitting","bus_standing","tram_standing","metroH_standing","metroK_standing","marmaray_standing","metrobus_standing","ferry_standing"};
//
//    char *labels1[]= {"walking","minibus","car","bus","tram","metro","metrobus","ferry"};
//
//    char *labels2[]= {"walking","minibus","car","bus","tram","metroK","metroH","marmaray","metrobus","stationary"};
//
//    char label[100];
//    char alan[100];
//    int kacinciKelime;
//    int yazildi=0;
//
//    //elde ettigimiz ozellik dosyasi
////    printf("Ozellik dosyasinin adini giriniz: ");
////    scanf("%s",dosyaAdiYazma);
//    strcat(dosyaAdiYazma, outputFilePath);
//    strcat(dosyaAdiYazma, outputFileName);
////
//    FILE *fRead;
//    FILE *fWrite;
//    FILE *fWrite1;
//
//
//
////    printf("\nAzaltilmis labellar?(2 - evet, 1 - evet, 0 - hayir) ");
//    //    scanf("%d",&labelType);
//    labelType = 2;
//
//    fWrite = fopen(dosyaAdiYazma,"w");
//    arrERROR(fWrite);
////
//    fprintf(fWrite,"@RELATION vehicle_classification\n\n");
//
//
//    // klasor icerisindeki butun dosyalari okuyor csv formatinda olanlar uzerinde islem yapiyor
//
//    DIR *dp;
//    struct dirent *ep;
////
//    dp = opendir (classifyResultPath);
//    if (dp == NULL)
//    {
//        LOGD("exit");
//        exit(1);
//    }
//
//    LOGD("Classify Result Path: %s", classifyResultPath);
//    while ((ep = readdir (dp)))
//    {
//
//        LOGD("while icerisinde");
//
//        strcpy(dosyaAdiOkuma,ep->d_name);
//        strcpy(denemeDosya,ep->d_name);
//        LOGD("Dosya Adi Okuma: %s", dosyaAdiOkuma);
//        if (isCSV(dosyaAdiOkuma))
//        {
//
//            LOGD("isCSV BASI");
//            strcpy(dosyaAdiOkuma, classifyResultPath);
//            strcat(dosyaAdiOkuma, denemeDosya);
//            labelBul(dosyaAdiOkuma,label,labelType);
//
//            fRead = fopen(dosyaAdiOkuma,"r");
//
//            LOGD("dosya adi open: %s", dosyaAdiOkuma);
//            fgets(tmpSatir,LINESIZE,fRead); //????
//
//
//
//
//            //fprintf(fWrite,"*******\n");
//            i = 0;
//
//            if(!yazildi)
//            {
//
//                LOGD("YAZILDI BASI");
//
//
//                while(tmpSatir[i])
//                {
//                    //kac column icin ozellik cikarilacagina bakiliyor
//                    if(tmpSatir[i] == ',' || tmpSatir[i] == ';')
//                    {
//
//                        ayrac = tmpSatir[i];
//                    }
//                    i++;
//                }
//
//                j= 0;
//
//
//                i=0;
//                kacinciKelime=0;
//                k=0;
//                kelimeUzunlugu=0;
//
//
//                for(k=0; k<OZELLIK_SAYISI; k++)
//                {
//                    fprintf(fWrite,"@ATTRIBUTE \"ACC_%s\" NUMERIC\n",ozellikler[k]);
//                }
//
//                for(k=0; k<OZELLIK_SAYISI; k++)
//                {
//                    fprintf(fWrite,"@ATTRIBUTE \"GYRO_%s\" NUMERIC\n",ozellikler[k]);
//                }
//
//                for(k=0; k<OZELLIK_SAYISI; k++)
//                {
//                    fprintf(fWrite,"@ATTRIBUTE \"MAG_%s\" NUMERIC\n",ozellikler[k]);
//                }
//
//                k=0;
//
//                //arff icin attribute isimlerini olusturuyor
//                while(kacinciKelime<(argSayisi-3))
//                {
//                    if(tmpSatir[i] != ayrac)
//                    {
//                        alan[kelimeUzunlugu] = tmpSatir[i];
//                        printf("%c",alan[kelimeUzunlugu]);
//                        kelimeUzunlugu++;
//                    }
//                    else
//                    {
//                        alan[kelimeUzunlugu] = '\0';
//                        printf("\n");
//                        for(k=0; k<OZELLIK_SAYISI; k++)
//                        {
//                            fprintf(fWrite,"@ATTRIBUTE \"%s_%s\" NUMERIC\n",alan,ozellikler[k]);
//                        }
//                        kelimeUzunlugu=0;
//                        kacinciKelime++;
//                    }
//                    i++;
//                }
//
//                //oklid degerleri icin eklenmis olan attribute lar el ile hazirlanmistir.
//
//                //..
//
//                //etiketleri belirten parca
//                fprintf(fWrite,"@ATTRIBUTE label {");
//
//                if(labelType == 0)
//                {
//                    for(i=0; i<LABELSAYISI-1; i++)
//                    {
//                        fprintf(fWrite,"%s,",labels[i]);
//                    }
//                    fprintf(fWrite,"%s",labels[i]);
//                }
//                else if (labelType == 1)
//                {
//                    for(i=0; i<LABELSAYISI1-1; i++)
//                    {
//                        fprintf(fWrite,"%s,",labels1[i]);
//                    }
//                    fprintf(fWrite,"%s",labels1[i]);
//                }
//                else if (labelType == 2){
//                    for(i=0; i<LABELSAYISI2-1; i++)
//                    {
//                        fprintf(fWrite,"%s,",labels2[i]);
//                    }
//                    fprintf(fWrite,"%s",labels2[i]);
//                }
//
//                fprintf(fWrite,"}\n\n@DATA\n");
//                //..
//
//
//
//                //saniyede alinan veri sayisi
////                printf("\nFrekans Nedir? (hz) ");
//                //                scanf("%d",&frekans);
////                frekans = 100;
////                printf("\nNe Kadarlik Zaman Araliginin Ozelligini Cikarmak İstersiniz? (milisaniye cinsinden) ");
////                //                scanf("%d",&sure);
////                sure = 60000;
////
////                //over lap islemini sagliyor fakat atlama oranini gosteriyor 100-overlap
////                printf("\nAtlama oranini yuzde olarak giriniz(Tam Sayi) ");
////                //                scanf("%d",&atlamaOrani);
//                atlamaOrani = 60;
////                //atlamaOrani = 0;
////
////                //pencere boyutu hesabi
////
//////                kontrolAdet = (sure*frekans)/1000;
////                kontrolAdet = 6000;
////                atlamaMik = (kontrolAdet*atlamaOrani)/100;
//
//                //atlamaMik=0;
//
//                //   OZELLIK_SAYISI = (degerSayisi-kontrolAdet)/atlamaMik + 1;
//
//                //karisik oldugu icin arguman sayisi ve oklid ile elde edilen degerlerin sayisinin toplami bulunacak olan min max degerlerinin boyutunu vermektedir
//
////
////                printf("Arg Sayisi= %d\n",argSayisi);
////                printf("Kontrol Adet = %d\n",kontrolAdet);
////                printf("Atlama Miktari = %d\n",atlamaMik);
//
//                degerler = (double**) malloc(argSayisi * sizeof(double*));
//                arrERROR(degerler);
//                for(i=0; i<argSayisi; i++)
//                {
//                    degerler[i] = (double*) malloc(kontrolAdet * sizeof(double));
//                    arrERROR(degerler[i]);
//                }
//
//                yazildi=1;
//            }
//
//            degerSayisi = 0; //pencere boyutunun kontrolu saglayan degisken
//            r = 0;//dongusel olan pencerenin baslangic degeri.
//
////            for (i = 0; i < 6000; i++) {
////                fgets(tmpSatir,LINESIZE,fRead);
////            }
//
//            while(fgets(tmpSatir,LINESIZE,fRead))
//            {
//                //dongusel matrisimiz doldugunda ozelliklerin cikartilip arff dosyasina yazilmasi saglaniyor.
//                if(degerSayisi == kontrolAdet)
//                {
//                    //islemlerin devam edebilmesi icin deger sayisi azaltiliyor
//                    //0 lanmama sebebi overlap etmesidir.
//
////                    degerSayisi = kontrolAdet - atlamaMik;
//                    degerSayisi = 0; //burada overlapi kaldırıyoruz
////                    int xyz;
////                    for (xyz = 0; xyz < kontrolAdet; xyz++) {
////                        printf("x = %d, deger = %lf\n", xyz, degerler[0][xyz]);
////                    }
//
//                    islemDosya(degerler,r,argSayisi,ayrac,&fWrite);
//
//                    fprintf(fWrite,"%s",label);
//                    putc('\n',fWrite);
//                }
//
//                j=0;
//                //satir satir degerleri buluyor.
//                //her satirda arguman sayisi kadar column vardir yani o kadar deger vardir.
//                for(i=3; i<argSayisi; i++)
//                {
//
//                    //column lardaki double degerler char char degerlendirilerek olusturuluyor
//                    isaret = 1.0;
//                    tmpDeger = 0.0;
//                    dDonustur = 1.0;
//
//                    //tam sayi kismi
//                    if(tmpSatir[j]== '-')
//                    {
//                        isaret = -1.0;
//                        j++;
//                    }
//
//                    while(tmpSatir[j] != ayrac && tmpSatir[j] != '.')
//                    {
//
//                        tmpDeger *= 10;
//                        tmpDeger += (tmpSatir[j] - '0');
//                        j++;
//                    }
//                    if(tmpSatir[j] != ayrac)
//                        j++;
//                    //..
//
//                    //ondalik kisim
//                    while(tmpSatir[j] != ayrac)
//                    {
//                        dDonustur *= 10;
//                        tmpDeger = tmpDeger + (tmpSatir[j] - '0')/dDonustur;
//                        j++;
//                    }
//                    //..
//
//                    //degerleri ekledigimiz kisim
//                    degerler[i][r] = tmpDeger*isaret;
//
//                    j++;
//
//                }
//
//                for(i = 0; i < 3; i++){
//                    indis = (i + 1) * 3;
//
//                    oklidDegeri= pow(degerler[indis][r], 2)
//                                 + pow(degerler[indis + 1][r], 2)
//                                 + pow(degerler[indis + 2][r], 2);
//                    degerler[i][r] = sqrt(oklidDegeri);
////                    printf("x = %d, deger = %lf\n", r, degerler[i][r]);
//                }
//                // printf("\n");
//                //r  dongusel olarak artmaktadir.
//                //degerler i arguman sayisi kadar, r ise pencere boyutu kadardir.
//                r++;
//                r %= kontrolAdet;
//                degerSayisi++;
//            }
//
//
//            LOGD("CLOSE READ");
//            fclose(fRead);
//        }
//    }
//
//
//    LOGD("free part");
//    if (degerler){
//        for(i=0; i<argSayisi; i++)
//        {
//            free(degerler[i]);
//
//        }
////
//
//        free(degerler);
//    }
//
//
//
//
//
//    LOGD("CLOSE Write");
//    fclose(fWrite);
//
//    LOGD("CLOSE Write FINISH");
//
//}

//void dosyaErr(FILE *f)
//{
//    if(!f)
//    {
//        printf("Dosya Bulunamadi");
//        exit(0);
//    }
//}

//void doubleMErr(double **d)
//{
//    if(!d)
//    {
//        printf("Matris Olusturulamadi");
//        exit(0);
//    }
//}

//void doubleDErr(double *d)
//{
//    if(!d)
//    {
//        printf("Alan Olusturulamadi");
//        exit(0);
//    }
//}
