//package com.example.bdusun.vehicleclassification;
//
///**
// * Created by bdusun on 30/01/17.
// */
//
//import android.os.AsyncTask;
//import android.util.Log;
//
//import java.io.BufferedWriter;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.Writer;
//
//import weka.classifiers.Classifier;
//import weka.classifiers.Evaluation;
//import weka.classifiers.trees.RandomForest;
//import weka.core.Instances;
//import weka.core.converters.ConverterUtils;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
//
//
//import android.os.AsyncTask;
//import android.util.Log;
//import android.widget.Toast;
//
//import java.io.BufferedWriter;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.Writer;
//
//import weka.classifiers.Classifier;
//import weka.classifiers.Evaluation;
//import weka.classifiers.meta.FilteredClassifier;
//import weka.classifiers.trees.RandomForest;
//import weka.core.Instances;
//import weka.core.converters.ConverterUtils;
//import weka.filters.unsupervised.attribute.Remove;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.HEAL_RESULT_FILENAME;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.JOURNEY_DATA_FOLDER;
//import static com.example.bdusun.vehicleclassification.MainActivityOld.heal;
//
///**
// * Created on 24/01/17.
// */
//
//public class TaskHeal extends AsyncTask<String,String,String> {
//
//
//    private String resp;
//    private long beginTime = System.currentTimeMillis();
//
//
//
//    @Override
//    protected String doInBackground(String... params) {
//        //publishProgress("Processing file..."); // Calls onProgressUpdate()
//
////        String classificationResultPath = params[0];
////        String outputPath = params[1];
//
//        String classificationResultPath = JOURNEY_DATA_FOLDER + "20170304.txt";
//        String outputPath = JOURNEY_DATA_FOLDER + "20170304.heal";
//
//
//        //classificationResultPath += CLASSIFICATION_RESULT_FILENAME;
//        //outputPath += HEAL_RESULT_FILENAME; // summary.txt prediction.csv
//        //predictionPath += "prediction.csv";
//
//        try {
//            // Do your long operations here and return the result
//
//            heal(classificationResultPath,outputPath);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            resp = e.getMessage();
//        }
//        return resp;
//
//    }
//
//    @Override
//    protected void onPostExecute(String result) {
//        // execution of result of Long time consuming operation
//        //finalResult.setText(result);
//        long endTime = System.currentTimeMillis();
//        long dif = endTime - beginTime;
//
//        Log.d("VEHCLA-HEAL", "Heal completed in " + dif + " ms");
//        //Toast.makeText(getBaseContext(), "Classification completed in " + dif + " ms" ,Toast.LENGTH_SHORT).show();
//
//    }
//
//    @Override
//    protected void onPreExecute() {
//        // Things to be done before execution of long running operation. For
//        // example showing ProgessDialog
//    }
//
//    @Override
//    protected void onProgressUpdate(String... text) {
//        //finalResult.setText(text[0]);
//        // Things to be done while execution of long running operation is in
//        // progress. For example updating ProgessDialog
//    }
//
//}

package com.example.bdusun.vehicleclassification;

/**
 * Created by bdusun on 30/01/17.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;


import android.widget.Toast;

import static com.example.bdusun.vehicleclassification.Constants.FILE.JOURNEY_DATA_FOLDER;

/**
 * Created on 24/01/17.
 */

public class TaskHeal extends AsyncTask<String,String,String> {


    private String resp;
    private long beginTime = System.currentTimeMillis();
    private Context mContext;

    public TaskHeal(Context context){
        mContext = context;
    }


    @Override
    protected String doInBackground(String... params) {
        //publishProgress("Processing file..."); // Calls onProgressUpdate()

//        String classificationResultPath = params[0];
//        String outputPath = params[1];
        File[] listOfFiles;
        File folder;
        String fileName;

        String classificationResultPath; // = JOURNEY_DATA_FOLDER + "20170304.txt";
        String outputPath; // = JOURNEY_DATA_FOLDER + "20170304.heal";


        folder = new File(JOURNEY_DATA_FOLDER); // bu kisim androidde degisecek
        listOfFiles = folder.listFiles();

        for (File file : listOfFiles){
            fileName = file.getName();
            if (this.isTXT(fileName)){
                classificationResultPath = JOURNEY_DATA_FOLDER + fileName;
                fileName = fileName.replace(".txt",".rtf");
                outputPath = JOURNEY_DATA_FOLDER + fileName;
                try {
                    // Do your long operations here and return the result
//                    heal(classificationResultPath,outputPath);TODO: burası ayarlanacak

                } catch (Exception e) {
                    e.printStackTrace();
                    resp = e.getMessage();
                }

            }
        }

        //classificationResultPath += CLASSIFICATION_RESULT_FILENAME;
        //outputPath += HEAL_RESULT_FILENAME; // summary.txt prediction.csv
        //predictionPath += "prediction.csv";

        return resp;

    }




    @Override
    protected void onPostExecute(String result) {
        // execution of result of Long time consuming operation
        //finalResult.setText(result);
        long endTime = System.currentTimeMillis();
        long dif = endTime - beginTime;

        Toast.makeText(mContext, "Healing of all files completed in " + dif + " ms", Toast.LENGTH_SHORT)
                .show();

        Log.d("VEHCLA-HEAL", "Heal completed in " + dif + " ms");
        //Toast.makeText(getBaseContext(), "Classification completed in " + dif + " ms" ,Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPreExecute() {
        // Things to be done before execution of long running operation. For
        // example showing ProgessDialog
    }

    @Override
    protected void onProgressUpdate(String... text) {
        //finalResult.setText(text[0]);
        // Things to be done while execution of long running operation is in
        // progress. For example updating ProgessDialog
    }

    private boolean isTXT(String fileName){
        return fileName.endsWith(".txt");
    }

}
