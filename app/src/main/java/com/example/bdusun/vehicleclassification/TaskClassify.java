package com.example.bdusun.vehicleclassification;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Scanner;

import weka.classifiers.Classifier;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
//import weka.core.expressionlanguage.parser.Scanner;

import static com.example.bdusun.vehicleclassification.Constants.FILE.ARFF_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.REQUIRED_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
import static com.example.bdusun.vehicleclassification.MainActivity.islem;

/**
 * Created on 24/01/17.
 */

public class TaskClassify extends AsyncTask<String,String,String> {

    private String resultNames[] = {
            "Walking",
            "Minibus",
            "Car",
            "Bus",
            "Tram",
            "Hafif Raylı Metro",
            "Metro",
            "Marmaray",
            "Metrobus",
//            "Ferry",
            "Stationary"
    };

    private String resp;
    private long beginTime = System.currentTimeMillis();
    private int baslangic;
    private double degerler[][];
    private static int argSayisi = 12;
    private static int ozellikSayisi = 29;
    private String label;
    private static int count = 0;

    private String trainPath = REQUIRED_FOLDER + "all.model";;

    private String resultPath = CLASSIFICATION_RESULT_FOLDER;


    public TaskClassify(String filename) {
        try {
            scanner = new Scanner(new File(ARFF_FOLDER+filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        filename = filename.replace(".arff", ".txt");
        resultPath += filename;
    }

    private Scanner scanner;
    private File file;
    private String gercekDeger;

    @Override
    protected String doInBackground(String... params) {
        //publishProgress("Processing file..."); // Calls onProgressUpdate()

        try {

            int i,j;

            scanner.useDelimiter(",");

            for (i=0; i<353; i++) {
                scanner.nextLine();//burada sanıyorum ki argSayisi*ozellikSayisi kadar bir okuma gerekli hesaplarız
            }


            //System.out.println(filename + " is being read...");

            double[][] ozellikler = new double[argSayisi][ozellikSayisi] ;

            for (i = 0; i <argSayisi ; i++) {
                for (j = 0; j < ozellikSayisi; j++) {
                    ozellikler[i][j] = scanner.nextDouble();
                }
            }

            int result = -1;
            label = scanner.nextLine();//nextString()
            double zcth = ((ozellikler[0][27] * 100)/(ozellikler[0][27]+ozellikler[0][28]+ozellikler[0][25]));
            //LOGD("std:%lf, zerocrossingdurma: %lf", ozellikler[0][STDSAPMA], zcth);

            // return denebilir atama yerine
            if (ozellikler[0][12]>=1.27) { //default 1.27
                result = 0;
            }
            else if ((ozellikler[0][12]>=1.0) && (zcth<=7.22)) {//walking durmalilar icin
                result = 0;
            }
            else if ((zcth<=3.21)) {//walking
                result = 0;
            }
            else if (ozellikler[0][12]<0.1) { // stationary; //bu halde walkingdeki durmalar bulunamayacak //bazı metrolar durma gorunecek
                result = 9;
            }


            // Do your long operations here and return the result

            Classifier cls = (Classifier) weka.core.SerializationHelper.read(trainPath);

            // Read all the instances in the file (ARFF, CSV, XRFF, ...)
            //ConverterUtils.DataSource testSource = new ConverterUtils.DataSource(testPath);
            //Instances test = testSource.getDataSet();

            //test.setClassIndex(test.numAttributes() - 1);



            ConverterUtils.DataSource source = new ConverterUtils.DataSource(REQUIRED_FOLDER+"source.arff");
            Instances instances = source.getDataSet();
            instances.setClassIndex(argSayisi*ozellikSayisi);

            Instance instance = new DenseInstance(argSayisi*ozellikSayisi+1);
            instance.setDataset(instances);

            double instanceValues[][] = new double[argSayisi][ozellikSayisi];

            islem(degerler, baslangic, argSayisi, instanceValues);

            int sayac;

            sayac = 0;
            for(i=0; i<argSayisi; i++) {
                for(j=0; j<ozellikSayisi; j++){
                    instance.setValue(sayac, instanceValues[i][j]);
                    sayac++;
                }
            }

            //instance.setClassMissing();
            instance.setValue(sayac, label);
            double real = instance.classValue(); //Bunlar değişecek
            double pred = cls.classifyInstance(instance); //double pred = rf.classifyInstance(test.instance(i));


            if (result == -1){
                result = (int) pred;
            }


            int act = 0;

            Writer writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(resultPath,true), "utf-8"));


                //if (pred==real) { act = act + 1;}
                String error = (((int) pred)==((int) instance.classValue()) ? "" : "+" );
                //Log.d("VEHCLA", test.instance(i).value(1)+","
                writer.write((i+1)
                        + " "
                        + instance.classAttribute().value((int) real)
                        + " "
                        + (int) real
                        + " "
                        + instance.classAttribute().value(result)
                        + " "
                        + (int) result
                        + " "
                        + error + " " + "\n");
//                Log.d("VEHCLA", (i+1)
//                        + " "
//                        + instance.classAttribute().value((int) instance.classValue())
//                        + " "
//                        + (int) instance.classValue() + " "
//                        + " "
//                        + instance.classAttribute().value((int) pred) + " "
//                        + (int) pred + " "
//                        + error + " " + "\n");

//                double real = instance.classValue(); //Bunlar değişecek
//                double pred = cls.classifyInstance(instance); //double pred = rf.classifyInstance(test.instance(i));
//                //if (pred==real) { act = act + 1;}
//                String error = (((int) pred)==((int) instance.classValue()) ? "" : "+" );
//                //Log.d("VEHCLA", test.instance(i).value(1)+","
//                writer.write((i+1) +","
//                        + (int) instance.classValue() + ":"
//                        + instance.classAttribute().value((int) instance.classValue()) + ","
//                        + (int) pred + ":"
//                        + instance.classAttribute().value((int) pred) + ","
//                        + error + "," + "\n");
//                Log.d("VEHCLA", (i+1) +","
//                        + (int) instance.classValue() + ":"
//                        + instance.classAttribute().value((int) instance.classValue()) + ","
//                        + (int) pred + ":"
//                        + instance.value((int) pred) + ","
//                        + error + ",");

                // writer.write(eval.toMatrixString("\nResults\n======\n"));
                //Log.d("VEHCLA-CLA", eval.toSummaryString("\n===Summary===\n", false));
                //writer.write(eval.toSummaryString("\n===Summary===\n", true));
                //writer.write("\n");
                //Log.d("VEHCLA-CLA", eval.toMatrixString("\n===Confusion Matrix===\n"));
                //writer.write(eval.toMatrixString("\n===Confusion Matrix===\n"));
                //writer.write("\n");
//                Log.d("VEHCLA-CLA", "\n----Class Indexes----\n");
//                writer.write("\n----Class Indexes----\n");
//                Log.d("VEHCLA-CLA", "\nclass#,className\n");
//                writer.write("\nclass#,className\n");
//                for (i = 0; i < test.numClasses(); i++) {
//                    double pred = cls.classifyInstance(test.instance(i)); //double pred = rf.classifyInstance(test.instance(i));
//                    //Log.d("VEHCLA", test.instance(i).value(1)+","
//                    writer.write(i + "," + test.classAttribute().value(i)+"\n");
//                }
//                Log.d("VEHCLA-CLA", "\n===Predictions===\n\n");
//                writer.write("\n===Predictions===\n\n");
                //int act = 0;
                /*for (i = 0; i < test.numInstances(); i++) {
                    double real = test.instance(i).classValue();
                    double pred = cls.classifyInstance(test.instance(i)); //double pred = rf.classifyInstance(test.instance(i));
                    if (pred==real) { act = act + 1;}
                    String error = (((int) pred)==((int) test.instance(i).classValue()) ? "" : "+" );
                    //Log.d("VEHCLA", test.instance(i).value(1)+","
                    writer.write((i+1) +","
                            + (int) test.instance(i).classValue() + ":"
                            + test.classAttribute().value((int) test.instance(i).classValue()) + ","
                            + (int) pred + ":"
                            + test.classAttribute().value((int) pred) + ","
                            + error + "," + "\n");
                    Log.d("VEHCLA", (i+1) +","
                            + (int) test.instance(i).classValue() + ":"
                            + test.classAttribute().value((int) test.instance(i).classValue()) + ","
                            + (int) pred + ":"
                            + test.classAttribute().value((int) pred) + ","
                            + error + ",");
                }*/
                //writer.write(eval.toClassDetailsString("\n===Detailed Accuracy by Class===\n"));
                //Log.d("VEHCLA-CLA", eval.toClassDetailsString("\n===Detailed Accuracy by Class===\n"));

                //double pct = (double) act / (double) test.numInstances();
                //Log.d("VEHCLA-CLA","correct/numOfInst: "+pct);

            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    writer.close();
                } catch (Exception ex) {/*ignore*/}
            }

        } catch (Exception e) {
            e.printStackTrace();
            resp = e.getMessage();
        }
        return resp;

    }

    @Override
    protected void onPostExecute(String result) {
        // execution of result of Long time consuming operation
        //finalResult.setText(result);
        long endTime = System.currentTimeMillis();
        long dif = endTime - beginTime;

        Log.d("VEHCLA-CLA","Classification completed in " + dif + " ms");
        //Toast.makeText(getBaseContext(), "Classification completed in " + dif + " ms" ,Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPreExecute() {
        // Things to be done before execution of long running operation. For
        // example showing ProgessDialog
    }

    @Override
    protected void onProgressUpdate(String... text) {
        //finalResult.setText(text[0]);
        // Things to be done while execution of long running operation is in
        // progress. For example updating ProgessDialog
    }

}
