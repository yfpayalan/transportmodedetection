package com.example.bdusun.vehicleclassification;

import android.app.ActivityManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.example.bdusun.vehicleclassification.Constants.FILE.ARFF_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.JOURNEY_DATA_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_EXTRACT_FOLDER;
//import static com.example.bdusun.vehicleclassification.MainActivity.arffExtract;
import static com.example.bdusun.vehicleclassification.MainActivity.heal;


public class TravelOperationsActivity extends AppCompatActivity {

    public final static String TAG = "TravelOperationsActivity";

    private boolean isWorkingFab;


    private ClassificationDBHelper clsResDBHelper;
    private SQLiteDatabase dbRead;
    private SQLiteDatabase dbWrite;

    private boolean combinedThresholdCountValue;
    private int walkingAndStationaryCountThreshold;
    private int stationaryCountThreshold;
    private int walkingCountThreshold;
    private float walkingStddevThreshold;
    private float stationaryStddevThreshold;
//    private ServiceClassifyAndRecord mService;

    private TabHost classificationResults_tabHost;
    private TabHost.TabSpec tab1;
    private TabHost.TabSpec tab2;
    private TabHost.TabSpec tab3;

    private Toolbar toolbar;

    private FloatingActionButton fab;

    private String healDate = null;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_operations);


        clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        dbRead = clsResDBHelper.getReadableDatabase();
        clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        dbWrite = clsResDBHelper.getWritableDatabase();
        this.initializeLayout();

        this.isWorkingFab = false;




        Log.d(TAG, String.valueOf(classificationResults_tabHost.getCurrentTab()));
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

//
//        Fragment healFragment = new HealFragment();
//
//        ArrayList<Fragment> fragments = new ArrayList<Fragment>();
//        fragments.add(healFragment);
//

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//
//
//        getThresholdsFromPrefs();
//
//        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
//        int recordMode = Integer.valueOf(sharedPref.getString(getString(R.string.pref_key_recording_mode),
//                getString(R.string.pref_recording_mode_default)));
//
//        //Toast.makeText(this,recordModeTitle,Toast.LENGTH_LONG).show();
//
//        String recordModeTitleArray[] = getResources().getStringArray(R.array.pref_recording_mode_list_titles);
//        final String recordModeTitle = recordModeTitleArray[recordMode];
//
//        boolean isCsvEnabled = false;
//        boolean isClassificationEnabled = true;
//
//        switch (recordMode) {
//            case 0: //Classification only
//                isCsvEnabled = false;
//                isClassificationEnabled = true;
//                break;
//            case 1: //csv only
//                isCsvEnabled = true;
//                isClassificationEnabled = false;
//                break;
//            case 2: //both
//                isCsvEnabled = true;
//                isClassificationEnabled = true;
//                break;
//        }
//
//        final boolean isCsvEnabledFinal = isCsvEnabled;
//        final boolean isClassificationEnabledFinal = isClassificationEnabled;
//
//        if(isMyServiceRunning(ServiceClassifyAndRecord.class)){
//            fab.setImageResource(R.drawable.ic_stop_white_24dp);
//        }
//        else {
//            fab.setImageResource(R.drawable.ic_play_arrow_white_24dp);
//        }
//
//        fab.setOnClickListener(new View.OnClickListener() {
//
//            FloatingActionButton fabIn = (FloatingActionButton) TravelOperationsActivity.this.findViewById(R.id.fab);
//
//            @Override
//            public void onClick(View view) {
////                Snackbar.make(view, recordModeTitle, Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
//
//                Intent intent = new Intent(TravelOperationsActivity.this, ServiceClassifyAndRecord.class);
//
//                if(isMyServiceRunning(ServiceClassifyAndRecord.class)){
//                    intent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
//                    fabIn.setImageResource(R.drawable.ic_play_arrow_white_24dp);
//                    Snackbar.make(view, "Stopping " + recordModeTitle, Snackbar.LENGTH_LONG).show();
////                    mService.stop();
//                }
//                else {
////                    mService = new ServiceClassifyAndRecord(TravelOperationsActivity.this);
//                    intent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
//                    fabIn.setImageResource(R.drawable.ic_stop_white_24dp);
//                    intent.putExtra(getString(R.string.param_csv_enabled),isCsvEnabledFinal);
//                    intent.putExtra(getString(R.string.param_classification_enabled),isClassificationEnabledFinal);
//                    Snackbar.make(view, "Starting " + recordModeTitle, Snackbar.LENGTH_LONG).show();
////                    mService.start(intent);
//                    ServiceClassifyAndRecord.setStationaryThresholdStddev(stationaryStddevThreshold);
//                    ServiceClassifyAndRecord.setWalkingThresholdStddev(walkingStddevThreshold);
//                    RunnableIslem.setStationaryCountThreshold(stationaryCountThreshold);
//                    RunnableIslem.setWalkingAndStationaryCountThreshold(walkingAndStationaryCountThreshold);
//                    RunnableIslem.setCombinedThresholdCountValue(combinedThresholdCountValue);
//                    RunnableIslem.setWalkingCountThreshold(walkingCountThreshold);
//                }
//                startService(intent);
//                Log.d(TAG, "starting service from TravelOperations");
//
//            }
//        });


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                int currentTab = classificationResults_tabHost.getCurrentTab();
                switch (currentTab) {
                    case 0:
//                        extract();
                        Log.d(TAG, "Extract sonrası");
                        break;
                    case 1:
//                        classify();
                        break;
                    case 2:
//                        healJava();
                        break;
                    case 3:
//                        if (!isWorkingFab){
//
//                        }


                        Thread thread = new Thread(){
                            public void run(){
                                System.out.println("Thread Running");
                                addAllFilesSQL();
                            }
                        };
                        thread.start();
                        try {
                            thread.join();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;

                }
            }
        });


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        healDate = null;
        intent = getIntent();
        if (intent != null) {
            healDate = intent.getStringExtra("date"); // get date
            Log.d(TAG, "healdate: " + healDate);
            if(this.healDate != null) {
                healGivingDateToSQL(healDate);
                Log.d(TAG, "healed");
            }
        }
        else {
            Log.d(TAG, "null");
        }

        ApplicationDrawer applicationDrawer = new ApplicationDrawer(this);

    }

    private void addAllFilesSQL(){
//        setIsWorkingFab(true, "insert");
//        this.fab.setClickable(false);
//        Toast.makeText(getBaseContext(),"Starting instert to sql", Toast.LENGTH_SHORT).show();
        List<String> filenames = getFilenames(JOURNEY_DATA_FOLDER, ".txt");
        int i;
        int numOfFiles = filenames.size();
        Log.d(TAG, "numOfFiles: " + numOfFiles);
        for(i=0; i<numOfFiles;i++){
            readAFileAndInsertSQL(filenames.get(i));
        }
//        this.setIsWorkingFab(false, "insert");
//        this.fab.setClickable(true);
//        Toast.makeText(getBaseContext(),"Ended instert to sql", Toast.LENGTH_SHORT).show();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new MapsActivity.DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private String getDateFromFileName(String fileName){
        String date = "";
        date += fileName.substring(0, 4) + "-";
        date += fileName.substring(4, 6) + "-";
        date += fileName.substring(6, 8) + " ";

        return date;
    }


//    public void setIsWorkingFab(Boolean isWork, String workName){
//        if (isWork){
//            this.fab.setImageResource(R.drawable.ic_stop_black_24dp);
////            Snackbar.make(null, "Starting " + workName, Snackbar.LENGTH_LONG).show();
//        }else {
//            this.fab.setImageResource(R.drawable.ic_play_arrow_black_24dp);
////            Snackbar.make(null, "Stopping " + workName, Snackbar.LENGTH_LONG).show();
//        }
//
//        this.fab.setClickable(!isWork);
//        this.isWorkingFab = isWork;;
//    }
    private void readAFileAndInsertSQL(String fileName){
        try {
            Log.d(TAG, "readAFileAndInsertSQL: " + fileName);
            Scanner scanner = new Scanner(new File(JOURNEY_DATA_FOLDER + fileName));
            scanner.useDelimiter(" ");
            String date = this.getDateFromFileName(fileName);
            String datetime = date;

            int result;
            double lat = -100000;
            double lon = -100000;

            String tmpS;
            String tmpS2;
            int tmpI;
            while(scanner.hasNextLine()){
                tmpS = scanner.next();
                datetime += tmpS;
                tmpS = scanner.next();
                tmpI = scanner.nextInt();

                tmpS = scanner.next();
                if(tmpI == -1){
                    result = scanner.nextInt();
                }else{
                    result = tmpI;
                    tmpI = scanner.nextInt();
                }
//                tmpI = scanner.nextInt();
                //---
                tmpS = scanner.next();
                tmpI = scanner.nextInt();
//                result = tmpI;
                //--
                tmpS = scanner.next();
                tmpS2 = scanner.nextLine();

                if(!(tmpS.equals("null")) && !(tmpS2.equals("null"))){
                    lat = Double.parseDouble(tmpS);
                    lon = Double.parseDouble(tmpS2);
                }

//

//
//                System.out.println();

                datetime += ":00:000";
                addToSQL(result, findCombineResult(result), lat, lon, datetime);
                datetime = date;

            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private long addToSQL(int result, int combineResult, double lat, double lon, String datetime){
//        Log.d(TAG, "addAllFilesSQL/readAFileAndInsertSQL/addToSQL start..");
        ClassificationDBHelper clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        SQLiteDatabase db = clsResDBHelper.getWritableDatabase();



        ContentValues values = new ContentValues();
        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT, result);
        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT, combineResult);
//        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT, healResult);

        if(lat != -100000){
            System.out.print(lat + " " + lon);
            values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_LATITUDE, lat);
            values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_LONGITUDE, lon);
        }

        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME, datetime);
        long newRowId = db.insert(FeedClassificationContract.FeedResult.TABLE_NAME, null, values);
        db.close();

//        Log.d(TAG, "addAllFilesSQL/readAFileAndInsertSQL/addToSQL end.");
        return newRowId;
    }

    public int findCombineResult(int result){

        int combineResult = result;

        if (result == 2){
            combineResult = 1;
        }else if ((result == 1) || (result == 3) || (result == 8)){
            combineResult = 2;
        }else if (result == 4){
            combineResult = 3;
        }else if ((result == 5) || (result == 6)){
            combineResult = 4;
        }else if (result == 7){
            combineResult = 5;
        }else if (result == 9){
            combineResult = 6;
        }else if (result == 10){
            combineResult = 7;
        }
        return combineResult;
    }

//    private boolean isMyServiceRunning(Class<?> serviceClass) {
//        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if (serviceClass.getName().equals(service.service.getClassName())) {
//                return true;
//            }
//        }
//        return false;
//    }

    private List<String> getCheckedFiles(ListView listView) {
        SparseBooleanArray clickedItemPositions = listView.getCheckedItemPositions();

        List<String> files = new ArrayList<String>();

        for (int index = 0; index < clickedItemPositions.size(); index++) {
            // Get the checked status of the current item
            boolean checked = clickedItemPositions.valueAt(index);

            if (checked) {
                // If the current item is checked
                int key = clickedItemPositions.keyAt(index);
                String item = (String) listView.getItemAtPosition(key);
                files.add(item);
            }
        }
        return files;
    }

//    private void extract(){
//        //SENSOR_DATA_EXTRACT_FOLDER
//        //ARFF_FOLDER
//        //classify path
//        //output path
//        //output name
////        Runnable run = new Runnable() {
////            @Override
////            public void run() {
////                arffExtract(SENSOR_DATA_EXTRACT_FOLDER, ARFF_FOLDER, "output.txt");
////            }
////        };
////        run.run();
////        Thread t = new Thread(run);
////        t.start();
////        arffExtract(SENSOR_DATA_EXTRACT_FOLDER, ARFF_FOLDER, "output.arff");
//        final String fname = "output.arff";
//
//        Thread threadExtract = new Thread(){
//            public void run(){
//                System.out.println("Thread Running");
//                arffExtract(SENSOR_DATA_EXTRACT_FOLDER, ARFF_FOLDER, fname);
//            }
//        };
//
//        threadExtract.start();
//    }

    private void classify(){
        List<String> filenames = getFilenames(ARFF_FOLDER, ".arff");
        int i;
        TaskClassify tc;
        for(i=0; i<filenames.size();i++){
            tc = new TaskClassify(filenames.get(i));
            tc.execute();
        }
    }

    private void healJava(){
        List<String> filenames = getFilenames(ARFF_FOLDER, ".rtf");

    }

    private int numOfData;
    private int ids[];
    private int results[];
    private int combineResults[];

    public void healButtonAction(View view){
        //String beginDate = "";
        //beginDate bulunacak
        showDatePickerDialog(view);
    }

    private void healGivingDateToSQL(String beginTime){
        numOfData = this.getToBeHealedCount(beginTime);
        ids = new int[numOfData];
        results = new int[numOfData];
        combineResults = new int[numOfData];
        this.generateDataToBeHealed(beginTime, numOfData, ids, results, combineResults);

        Thread threadResult = new Thread(){
            public void run(){
                System.out.println("Thread Running");
                heal(results, numOfData, 12);
                Log.d(TAG, "updateSQL start");
                updateToSQL(numOfData, FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT, results);
                Log.d(TAG, "updateSQL end");
            }
        };

        threadResult.start();
        Thread threadCombined = new Thread(){
            public void run(){
                System.out.println("Thread Running");
                heal(combineResults, numOfData, 9);
                Log.d(TAG, "updateSQL start");
                updateToSQL(numOfData, FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT, combineResults);
                Log.d(TAG, "updateSQL end");
            }
        };

        threadCombined.start();


        try {
            threadResult.join();
            threadCombined.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void updateToSQL(int numOfResults, String columnName, int results[]){
        Log.d(TAG, "update to SQL 1");
        Log.d(TAG, "numOfResults: " + numOfResults);
//        Log.d("RunnableIslem", "addToSql 2");
        int i;
        int updateId;

        for (i = 0; i < numOfResults; i++){
//            Log.d(TAG, "columnNAme: " + columnName);
            ContentValues values = new ContentValues();
            values.put(columnName, results[i]);
//            values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT, combineResults[i]);
//            update(MYDATABASE_TABLE, values, KEY_ID+"="+id, null);
            updateId = dbWrite.update(FeedClassificationContract.FeedResult.TABLE_NAME, values, FeedClassificationContract.FeedResult._ID + " = " + ids[i], null);
            Log.d(TAG,columnName + " " + ids[i] + " results: " + results[i] + " updateId: " + updateId);
        }

        Log.d(TAG, "update to SQL end2");

    }



    public void generateDataToBeHealed(String beginDate, int numOfResults, int ids[], int results[], int combineResults[]){

        int i = 0;

//        this.results = new int[numOfResults];
//        this.combineResults = new int[numOfResults];
//        this.ids = new int[numOfResults];


        String[] projection = {
                FeedClassificationContract.FeedResult._ID,
                FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT,
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT,
                FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME
//                FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT
        };

//        Select * from users where created_at between '2017-05-05 10:45:46' and '2017-05-05 11:46:19'
        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " LIKE ?";
        String selecttionArgs[] = {
                ""+beginDate+"%"
        };
//        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + "  BETWEEN ? AND ?";
//        String selecttionArgs[] = {
//                beginDate
//        };

        String sortOrder = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME;// + DESC ve artan gibi bir şey eklenebilir

        Cursor cursor = dbRead.query(
                FeedClassificationContract.FeedResult.TABLE_NAME,
                projection,
                selection,
                selecttionArgs,
                null,
                null,
                sortOrder
        );

        while ((i < numOfResults) && (cursor.moveToNext())){
            ids[i] = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult._ID));
            results[i] = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT));
            combineResults[i] = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT));

//            Log.d(TAG, "id: " + ids[i] + "result: " + results[i] + "combineResults: " + combineResults[i]);
            i++;
//            Log.d(TAG,"DATE TIME: " + cursor.getString(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME)));
        }
        cursor.close();
    }
    public int getToBeHealedCount(String beginDate){
        String[] projection = {
                "COUNT(*)"
//                FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT
        };

        int count = 0;

        Log.d(TAG, "beginDate: " + beginDate);

//        Select * from users where created_at between '2017-05-05 10:45:46' and '2017-05-05 11:46:19'
        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " LIKE ?";
        String selecttionArgs[] = {
                ""+beginDate+"%"
        };

//        String sortOrder = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME;// + DESC ve artan gibi bir şey eklenebilir

        Cursor cursor = dbRead.query(
                FeedClassificationContract.FeedResult.TABLE_NAME,
                projection,
                selection,
                selecttionArgs,
                null,
                null,
                null
        );

        if (cursor.moveToNext()){
            count = cursor.getInt(cursor.getColumnIndex("COUNT(*)"));
            Log.d(TAG, "Count: " + count);
        }

        cursor.close();
        return count;
    }

    public void updateToSQL(int numOfResults, String columnName, int results[], int ids[]){
        Log.d(TAG, "update to SQL 1");
        Log.d(TAG, "numOfResults: " + numOfResults);
//        Log.d("RunnableIslem", "addToSql 2");
        int i;
        int updateId;

        for (i = 0; i < numOfResults; i++){
//            Log.d(TAG, "columnNAme: " + columnName);
            ContentValues values = new ContentValues();
            values.put(columnName, results[i]);
//            values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT, combineResults[i]);
//            update(MYDATABASE_TABLE, values, KEY_ID+"="+id, null);
            updateId = dbWrite.update(FeedClassificationContract.FeedResult.TABLE_NAME, values, FeedClassificationContract.FeedResult._ID + " = " + ids[i], null);
            Log.d(TAG,columnName + " " + ids[i] + " results: " + results[i] + " updateId: " + updateId);
        }

        Log.d(TAG, "update to SQL end2");

    }

//    public void getThresholdsFromPrefs(){
//        SharedPreferences prefs = getSharedPreferences(
//                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
//
//        boolean defaultCombinedThresholdCountValue = Boolean.parseBoolean(getString(R.string.pref_threshold_combined_default));
//        combinedThresholdCountValue = prefs.getBoolean(getString(R.string.pref_key_threshold_combined),defaultCombinedThresholdCountValue);
//
//        int defaultWalkingAndStationaryCountThreshold = Integer.parseInt(getString(R.string.pref_threshold_walkingandstationary_default));
//        walkingAndStationaryCountThreshold = prefs.getInt(getString(R.string.pref_key_threshold_walkingandstationary_count), defaultWalkingAndStationaryCountThreshold);
//
//        int defaultStationaryCountThreshold = Integer.parseInt(getString(R.string.pref_threshold_stationary_default));
//        stationaryCountThreshold = prefs.getInt(getString(R.string.pref_key_threshold_stationary_count),defaultStationaryCountThreshold);
//
//        int defaultWalkingCountThreshold = Integer.parseInt(getString(R.string.pref_threshold_walking_default));
//        walkingCountThreshold = prefs.getInt(getString(R.string.pref_key_threshold_walking_count),defaultWalkingCountThreshold);
//
//        float defaultWalkingStddevThreshold = Float.parseFloat(getString(R.string.pref_threshold_walking_stddev_default));
//        walkingStddevThreshold = prefs.getFloat(getString(R.string.pref_key_threshold_walking_stddev),defaultWalkingStddevThreshold);
//
//        float defaultStationaryStddevThreshold = Float.parseFloat(getString(R.string.pref_threshold_stationary_stddev_default));
//        stationaryStddevThreshold = prefs.getFloat(getString(R.string.pref_key_threshold_stationary_stddev),defaultStationaryStddevThreshold);
//
//    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_travel_operations, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        Intent intent = new Intent();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            intent.setClass(this, SettingsActivity.class);
//            startActivity(intent);
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private List<String> getFilenames(String directory, String extension) {

        List<String> files = new ArrayList<String>();
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles!=null){
            for (int i = 0; i < listOfFiles.length; i++) {
                String filename = listOfFiles[i].getName();
                if (filename.endsWith(extension)) {
                    //System.out.println("File " + listOfFiles[i].getName());
                    files.add(filename);
                }
            }
            return files;
        }
        else
            return null;
    }

    private ListView listViewExtract;
    private ListView listViewClassify;
    private ListView listViewHeal;

    private Button healButton;

    private void initializeLayout() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.classificationResults_tabHost = (TabHost) findViewById(R.id.travelOpp_TabHost);

        this.classificationResults_tabHost.setup();

//        Button b = (Button) findViewById(R.id.button_classification);

        //Tab 1
        TabHost.TabSpec spec = this.classificationResults_tabHost.newTabSpec("Extract");
        spec.setContent(R.id.feature_ext_tab1);
        spec.setIndicator("Extract");
        this.classificationResults_tabHost.addTab(spec);

        listViewExtract = (ListView) findViewById(R.id.list_view_extract);

        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                getFilenames(SENSOR_DATA_EXTRACT_FOLDER, ".txt"));

//        if(arrayAdapter!=null){
//            listViewExtract.setAdapter(arrayAdapter);
//        }
//        listViewExtract.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        //Tab 2
        spec = this.classificationResults_tabHost.newTabSpec("Classify");
        spec.setContent(R.id.classification_tab2);
        spec.setIndicator("Classify");
        this.classificationResults_tabHost.addTab(spec);

//        listViewClassify = (ListView) findViewById(R.id.list_view_classify);

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                getFilenames(SENSOR_DATA_EXTRACT_FOLDER, ".rtf"));

//        if(arrayAdapter2!=null){
//            listViewClassify.setAdapter(arrayAdapter2);
//        }


        //Tab 3
        spec = this.classificationResults_tabHost.newTabSpec("Heal");
        spec.setContent(R.id.heal_tab3);
        spec.setIndicator("Heal");
        this.classificationResults_tabHost.addTab(spec);

        healButton = (Button) findViewById(R.id.button_datepicker_heal);
        healButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                healButtonAction(view);
            }
        });

        //Tab 4
        spec = this.classificationResults_tabHost.newTabSpec("Insert");
        spec.setContent(R.id.insert_tab4);
        spec.setIndicator("Insert");
        this.classificationResults_tabHost.addTab(spec);

//        listViewHeal = (ListView) findViewById(R.id.list_view_heal);


        ArrayAdapter<String> arrayAdapter3 = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                getFilenames(SENSOR_DATA_EXTRACT_FOLDER, ".rtf"));
//
//        if(arrayAdapter3!=null){
//            listViewHeal.setAdapter(arrayAdapter3);
//        }
    }



}
