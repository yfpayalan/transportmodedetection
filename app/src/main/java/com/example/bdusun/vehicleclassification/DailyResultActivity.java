package com.example.bdusun.vehicleclassification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DailyResultActivity extends AppCompatActivity {

    private Button button;
    private ListView listViewResults;
    private ListView listViewHealed;
    private Intent intent;
    private String date;
    private String resultColumn;

    private ClassificationDBHelper clsResDBHelper;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_result);

        this.clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        this.db = clsResDBHelper.getReadableDatabase();

        button = (Button) findViewById(R.id.button_datepicker_dailyresult);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(view);
            }
        });

        listViewResults = (ListView) findViewById(R.id.listview_results);
        listViewHealed = (ListView) findViewById(R.id.listview_results_healed);

        intent = getIntent();
        date = intent.getStringExtra("date"); // get date

        //TODO get daily results from DB

        List<DailyResult> resultList = new ArrayList<>();
        List<DailyResult> healedList = new ArrayList<>();
        generateResults(date, resultList, healedList);

        DailyListViewArrayAdapter dailyListViewArrayAdapter = new DailyListViewArrayAdapter(this, resultList);

        listViewResults.setAdapter(dailyListViewArrayAdapter);

        DailyListViewArrayAdapter healedListViewArrayAdapter = new DailyListViewArrayAdapter(this, healedList);

        listViewHealed.setAdapter(healedListViewArrayAdapter);


    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new MapsActivity.DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private class DailyResult {
        int id;
        int result;
        //int healedResult;
        int drawableId;
        String transportModeType;

        public DailyResult(int id, String transportModeType, int result, int drawableId) {
            this.id = id;
            this.result = result;
            //this.healedResult = healedResult;
            this.drawableId = drawableId;
            this.transportModeType = transportModeType;
        }
    }

    private class DailyListViewArrayAdapter extends BaseAdapter {

        private LayoutInflater bInflater;
        private List<DailyResult> bResultList;
        private Activity bActivity;

        public DailyListViewArrayAdapter(Activity activity, List<DailyResult> results) {
            bActivity = activity;
            bInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            bResultList = results;
        }

        @Override
        public int getCount() {
            //return 0;
            return bResultList.size();
        }

        @Override
        public Object getItem(int i) {
            return bResultList.get(i);
        }

        @Override // ?
        public long getItemId(int i) {
            return bResultList.get(i).id;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            View rowView;

            rowView = bInflater.inflate(R.layout.row_layout_listviewdaily, null);
            TextView textViewLabel = (TextView) rowView.findViewById(R.id.textview_daily_label);
            TextView textViewResult = (TextView) rowView.findViewById(R.id.textview_daily_duration);
            //TextView textViewHealedResult = (TextView) rowView.findViewById(R.id.textview_daily_result_healed);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon_dailyresult);

            final DailyResult dailyResult = bResultList.get(position);

            textViewLabel.setText(dailyResult.transportModeType);
            textViewResult.setText(dailyResult.result + " min");
            //textViewHealedResult.setText(dailyResult.healedResult+" min");
            imageView.setImageResource(dailyResult.drawableId);

            return rowView;

        }
    }

    public void generateResults(String beginDate, List<DailyResult> results, List<DailyResult> healedResults) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS
        int resultImages[] = {
                R.drawable.walking,
//            R.drawable.minibus,
                R.drawable.car,
                R.drawable.bus,
                R.drawable.tram,
//            R.drawable.subway,
                R.drawable.metro,
                R.drawable.marmaray,
//            R.drawable.metrobus,
                R.drawable.ferry,
                R.drawable.airplane,
                R.drawable.stationary
        };
        String resultStrings[] = {
                "Walking",
                "Car",
                "Bus",
                "Tram",
                "Metro",
                "Marmaray",
                "Ferry",
                "Airplane",
                "Stationary"
        };


        String[] projection = {
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT,
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT,
                FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME
//                FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT
        };

//        Select * from users where created_at between '2017-05-05 10:45:46' and '2017-05-05 11:46:19'
        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " LIKE ?";
        String selecttionArgs[] = {
                ""+date+"%"
        };
//        Log.d("dailyresultactivty,date:",date);

        String sortOrder = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME;// + DESC ve artan gibi bir şey eklenebilir

        Cursor cursor = db.query(
                FeedClassificationContract.FeedResult.TABLE_NAME,
                projection,
                selection,
                selecttionArgs,
                null,
                null,
                sortOrder
        );

        int action=-1;
        int prevAction=-2;

        int healAction=-1;
        int prevHealAction=-2;

        int id = 0;
        int actionCount = 1;
        int healCount = 1;

        if(cursor.moveToNext()){
            action = prevAction = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT));
            healAction = prevHealAction = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT));
        }

        while (cursor.moveToNext()) {
            action = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT));
            if(action!=prevAction) {
                DailyResult dailyResult = new DailyResult(id, resultStrings[prevAction], actionCount, resultImages[prevAction]);
                results.add(dailyResult);
                id++;
                actionCount=1;
            } else {
                actionCount++;
            }
            prevAction = action;

            healAction = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT));
            if(healAction!=prevHealAction) {
                DailyResult dailyResult = new DailyResult(id, resultStrings[prevHealAction], healCount, resultImages[prevHealAction]);
                healedResults.add(dailyResult);
                id++;
                healCount=1;
            } else {
                healCount++;
            }
            prevHealAction = healAction;

//            Log.d ("DailyResult", "result: " + action);
//            Log.d("DailyResult", "healedResult:" + healAction);
//            Log.d(TAG,"DATE TIME: " + cursor.getString(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME)));
        }
        if(action == prevAction) {
            DailyResult dailyResult = new DailyResult(id, resultStrings[prevAction], actionCount, resultImages[action]);
            results.add(dailyResult);
        }
        if(healAction == prevHealAction) {
            DailyResult dailyResult = new DailyResult(id, resultStrings[prevHealAction], healCount, resultImages[healAction]);
            healedResults.add(dailyResult);
        }

        cursor.close();


//        Cursor cursor = db.rawQuery("SELECT "
//                + "COUNT(" + FeedClassificationContract.FeedResult._ID  + ")" + ","
//                + request + ","
//                + resultColumn
//                + " FROM " + FeedClassificationContract.FeedResult.TABLE_NAME
//                + " WHERE " + FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " LIKE " + "'" + date + "%'"
//                + " GROUP BY " + request
//                + " ORDER BY " + request, null);
//
//        while (cursor.moveToNext()){
//
//            int combineresult = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT));
//            int countcombineresult = cursor.getInt(cursor.getColumnIndex("COUNT(" + FeedClassificationContract.FeedResult._ID+")"));
//
//            combineresults[combineresult] = countcombineresult;
//
//            Log.d("DailyResult", date + " : " + "result=" + combineresult + ", count=" + countcombineresult);
//        }
//        //cursor.close();
//
//        cursor= db.rawQuery("SELECT "
//                + "COUNT(" + FeedClassificationContract.FeedResult._ID  + ")" + ","
//                + FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT + ","
//                + resultColumn
//                + " FROM " + FeedClassificationContract.FeedResult.TABLE_NAME
//                + " WHERE " + FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " LIKE " + "'" + date + "%'"
//                + " GROUP BY " + FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT
//                + " ORDER BY " + FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT, null);
//        while (cursor.moveToNext()){
//
//            int healedcombineresult = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT));
//            int counthealedcombineresult = cursor.getInt(cursor.getColumnIndex("COUNT(" + FeedClassificationContract.FeedResult._ID+")"));
//
//            healresults[healedcombineresult] = counthealedcombineresult;
//
//            Log.d("DailyResult", date + " : " + "healedresult=" + healedcombineresult + ", count=" + counthealedcombineresult);
//        }
//        cursor.close();
//
//        for(i=0; i<7; i++){
//            DailyResult dailyResult = new DailyResult(i, resultStrings[i], combineresults[i], resultImages[i]);
//            dailyResultList.add(dailyResult);
//        }


    }



}
