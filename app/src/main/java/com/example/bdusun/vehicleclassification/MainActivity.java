package com.example.bdusun.vehicleclassification;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.example.bdusun.vehicleclassification.Constants.FILE.ARFF_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.MODEL_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.REQUIRED_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.HEAL_RESULT_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.JOURNEY_DATA_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_EXTRACT_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_RECORD_FOLDER;

public class MainActivity extends AppCompatActivity  {
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS

    private static final String TAG = "MainActivity";
    private BroadcastReceiver broadcastReceiver;
    public static final double  MOTION_THRESHOLD=1;
    public static final double  MOTION_DEC_THRESHOLD=-1;
    public static final double  GYRO_THRESHOLD_LEFT =    0.087;
    public static final double  GYRO_THRESHOLD_RIGHT=   -0.087;
    public static String longFlag="S";

    public int result;

    private ClassificationDBHelper clsResDBHelper;
    private SQLiteDatabase db;

    private ViewPager cls_ViewPager;
    public  ViewPagerAdaptor cls_ViewPagerAdaptor;

    private boolean combinedThresholdCountValue;
    private int walkingAndStationaryCountThreshold;
    private int stationaryCountThreshold;
    private int walkingCountThreshold;
    private float walkingStddevThreshold;
    private float stationaryStddevThreshold;


    private TextView mTextViewAccX;
    private TextView mTextViewAccY;
    private TextView mTextViewAccZ;
    private TextView mTextViewGyroX;
    private TextView mTextViewGyroY;
    private TextView mTextViewGyroZ;
    private TextView mTextViewLat;
    private TextView mTextViewLong;


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    //public native String stringFromJNI();
    //public static native double processFile(String jcsvPath, String jarffFile, String jlocationFile);
    public static native int islem(double degerlerJ[][], int baslangic, int argSayisi, double sonuclar[][]);

    //public static native double processFile(String jcsvPath, String jarffFile);
    public static native void heal(int results[], int numOfResult, int numOfClass);
//    public static native void arffExtract(String sensorDataPath, String arffPath, String fileName);
//    public static native void healFile(String classifyResultFileName, String outputFileName);



    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        db = clsResDBHelper.getReadableDatabase();

        mTextViewAccX   = (TextView)findViewById(R.id.tViewAccX);
        mTextViewAccY   = (TextView)findViewById(R.id.tViewAccY);
        mTextViewAccZ   = (TextView)findViewById(R.id.tViewAccZ);
        mTextViewGyroX  = (TextView)findViewById(R.id.tViewGyroX);
        mTextViewGyroY  = (TextView)findViewById(R.id.tViewGyroY);
        mTextViewGyroZ  = (TextView)findViewById(R.id.tViewGyroZ);
        mTextViewLat    = (TextView)findViewById(R.id.tViewLatitude);
        mTextViewLong   = (TextView)findViewById(R.id.tViewLong);

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String lat = intent.getStringExtra(ServiceClassifyAndRecord.EXTRA_LAT);
                String lon = intent.getStringExtra(ServiceClassifyAndRecord.EXTRA_LONG);
                mTextViewLat.setText("LAT: "+lat);
                mTextViewLong.setText("LONG: "+lon);
            }
        },new IntentFilter(ServiceClassifyAndRecord.LOCATION_BROADCAST));


        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        float linearAccZ = intent.getFloatExtra(ServiceClassifyAndRecord.EXTRA_LINEAR_ACCZ,0);
                        float linearAccX = intent.getFloatExtra(ServiceClassifyAndRecord.EXTRA_LINEAR_ACCX,0);
                        float linearAccY = intent.getFloatExtra(ServiceClassifyAndRecord.EXTRA_LINEAR_ACCY,0);
                        float gx    = intent.getFloatExtra(ServiceClassifyAndRecord.EXTRA_GYROX,0);
                        float gy    = intent.getFloatExtra(ServiceClassifyAndRecord.EXTRA_GYROY,0);
                        float gz    = intent.getFloatExtra(ServiceClassifyAndRecord.EXTRA_GYROZ,0);
                        mTextViewGyroX.setText("GYRO - X: "+String.valueOf(gx));
                        mTextViewGyroY.setText("GYRO - Y: "+String.valueOf(gy));
                        mTextViewGyroZ.setText("GYRO - Z: "+String.valueOf(gz));
                        //acceleratıng-decelerating aspect of Z axis. ( in perspective of bird's eye)
                        mTextViewAccX.setText("ACC-X: "+String.valueOf(linearAccX));
                        mTextViewAccY.setText("ACC-Y: "+String.valueOf(linearAccY));
                        mTextViewAccZ.setText("ACC-Z: " + String.valueOf(linearAccZ));

/*
                        if (linearAccZ > MOTION_THRESHOLD)
                        {
                            mTextViewAccZ.setText("Z-IN ACC: " + String.valueOf(linearAccZ));
                            mTextViewAccZ.setTextColor(Color.GREEN);
                        }
                        else if (linearAccZ < MOTION_DEC_THRESHOLD)
                        {
                            mTextViewAccZ.setTextColor(Color.RED);
                            mTextViewAccZ.setText("Z-IN DEC: " + String.valueOf(linearAccZ));
                        }
                        else
                        {
                            mTextViewAccZ.setText("N/A: " + String.valueOf(linearAccZ));
                            mTextViewAccZ.setTextColor(Color.BLACK);
                        }
*/
                        //gyro events.
                        if (gz > GYRO_THRESHOLD_LEFT)
                            mTextViewGyroX.setText("GYRO - Z - LEFT: "+String.valueOf(gz));
                        else if(gz < GYRO_THRESHOLD_RIGHT)
                            mTextViewGyroX.setText("GYRO - Z - RIGHT: "+String.valueOf(gz));
                        else
                            mTextViewGyroX.setText("GYRO - Z - NOLAT: "+String.valueOf(gz));

//                        if (Math.abs(linearAccX) > MOTION_THRESHOLD)
//                            mTextViewAccX.setText("X-IN MOTION !");
//                        else
//                            mTextViewAccX.setText("N/A");
//
//                        if (Math.abs(linearAccY) > MOTION_THRESHOLD)
//                            mTextViewAccY.setText("Y-IN MOTION !");
//                        else
//                            mTextViewAccY.setText("N/A");


                        //                        if (linearAccZ > 1)
//                            mTextViewAccZ.setText("LONGITUDINAL EVENT: ACCELERATION OCCURING ! ");
//                        else if (linearAccZ < -1.5)
//                            mTextViewAccZ.setText("LONGITUDINAL EVENT: DECELERATION OCCURING ! ");
//                        else
//                            mTextViewAccZ.setText("LONGITUDINAL EVENT: MOTION STABLE");

                    }
                },new IntentFilter(ServiceClassifyAndRecord.LINEAR_ACC_BROADCAST)
        );

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        getThresholdsFromPrefs();

//        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
//        int recordMode = Integer.valueOf(sharedPref.getString(getString(R.string.pref_key_recording_mode),
//                getString(R.string.pref_recording_mode_default)));
//
//        //Toast.makeText(this,recordModeTitle,Toast.LENGTH_LONG).show();
//
//        String recordModeTitleArray[] = getResources().getStringArray(R.array.pref_recording_mode_list_titles);
//        final String recordModeTitle = recordModeTitleArray[recordMode];
//
//        boolean isCsvEnabled = false;
//        boolean isClassificationEnabled = true;
//
//        switch (recordMode) {
//            case 0: //Classification only
//                isCsvEnabled = false;
//                isClassificationEnabled = true;
//                break;
//            case 1: //csv only
//                isCsvEnabled = true;
//                isClassificationEnabled = false;
//                break;
//            case 2: //both
//                isCsvEnabled = true;
//                isClassificationEnabled = true;
//                break;
//        }
//
//        final boolean isCsvEnabledFinal = isCsvEnabled;
//        final boolean isClassificationEnabledFinal = isClassificationEnabled;

        if(isMyServiceRunning(ServiceClassifyAndRecord.class)){
            fab.setImageResource(R.drawable.ic_stop_white_24dp);
        }
        else {
            fab.setImageResource(R.drawable.ic_play_arrow_white_24dp);
        }

        fab.setOnClickListener(new View.OnClickListener() {

            FloatingActionButton fabIn = (FloatingActionButton) MainActivity.this.findViewById(R.id.fab);

            @Override
            public void onClick(View view) {
//                Snackbar.make(view, recordModeTitle, Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                int recordMode = Integer.valueOf(sharedPref.getString(getString(R.string.pref_key_recording_mode),
                        getString(R.string.pref_recording_mode_default)));

                String recordModeTitleArray[] = getResources().getStringArray(R.array.pref_recording_mode_list_titles);
                final String recordModeTitle = recordModeTitleArray[recordMode];

                boolean isCsvEnabled = true;
                boolean isClassificationEnabled = true;

                switch (recordMode) {
                    case 0: //Classification only
                        isCsvEnabled = false;
                        isClassificationEnabled = true;
                        break;
                    case 1: //csv only
                        isCsvEnabled = true;
                        isClassificationEnabled = false;
                        break;
                    case 2: //both
                        isCsvEnabled = true;
                        isClassificationEnabled = true;
                        break;
                }

                final boolean isCsvEnabledFinal = isCsvEnabled;
                final boolean isClassificationEnabledFinal = isClassificationEnabled;

                Intent intent = new Intent(MainActivity.this, ServiceClassifyAndRecord.class);

                if(isMyServiceRunning(ServiceClassifyAndRecord.class)){
                    intent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
                    fabIn.setImageResource(R.drawable.ic_play_arrow_white_24dp);
                    Snackbar.make(view, "Stopping " + recordModeTitle, Snackbar.LENGTH_LONG).show();
//                    mService.stop();
                }
                else {
                    //TODO: burada model oluşturmak için kullanılacak olan yeni dosyalar oluşturulacak
//                    mService = new ServiceClassifyAndRecord(TravelOperationsActivity.this);
                    SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
                    String forModelCreatingPath = ARFF_FOLDER + dateFormatFileName.format(new Date()) + ".txt";
                    intent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
                    fabIn.setImageResource(R.drawable.ic_stop_white_24dp);
                    intent.putExtra(getString(R.string.param_csv_enabled),isCsvEnabledFinal);
                    intent.putExtra(getString(R.string.param_classification_enabled),isClassificationEnabledFinal);
                    Snackbar.make(view, "Starting " + recordModeTitle, Snackbar.LENGTH_LONG).show();
//                    mService.start(intent);
                    ServiceClassifyAndRecord.setStationaryThresholdStddev(stationaryStddevThreshold);
                    ServiceClassifyAndRecord.setWalkingThresholdStddev(walkingStddevThreshold);
                    RunnableIslem.setStationaryCountThreshold(stationaryCountThreshold);
                    RunnableIslem.setWalkingAndStationaryCountThreshold(walkingAndStationaryCountThreshold);
                    RunnableIslem.setCombinedThresholdCountValue(combinedThresholdCountValue);
                    RunnableIslem.setWalkingCountThreshold(walkingCountThreshold);
                    RunnableIslem.setForModelCreatingPath(forModelCreatingPath);

                }
                Log.d(TAG, "isClassificationEnabled = " + isClassificationEnabled);
                Log.d(TAG, "isCSVEnabled = " + isCsvEnabled);
                startService(intent);
                //Log.d(TAG, "starting service from TravelOperations");

            }
        });

        ApplicationDrawer applicationDrawer = new ApplicationDrawer(this);

        createFolders();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void getThresholdsFromPrefs(){
        SharedPreferences prefs = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        boolean defaultCombinedThresholdCountValue = Boolean.parseBoolean(getString(R.string.pref_threshold_combined_default));
        combinedThresholdCountValue = prefs.getBoolean(getString(R.string.pref_key_threshold_combined),defaultCombinedThresholdCountValue);

        int defaultWalkingAndStationaryCountThreshold = Integer.parseInt(getString(R.string.pref_threshold_walkingandstationary_default));
        walkingAndStationaryCountThreshold = prefs.getInt(getString(R.string.pref_key_threshold_walkingandstationary_count), defaultWalkingAndStationaryCountThreshold);

        int defaultStationaryCountThreshold = Integer.parseInt(getString(R.string.pref_threshold_stationary_default));
        stationaryCountThreshold = prefs.getInt(getString(R.string.pref_key_threshold_stationary_count),defaultStationaryCountThreshold);

        int defaultWalkingCountThreshold = Integer.parseInt(getString(R.string.pref_threshold_walking_default));
        walkingCountThreshold = prefs.getInt(getString(R.string.pref_key_threshold_walking_count),defaultWalkingCountThreshold);

        float defaultWalkingStddevThreshold = Float.parseFloat(getString(R.string.pref_threshold_walking_stddev_default));
        walkingStddevThreshold = prefs.getFloat(getString(R.string.pref_key_threshold_walking_stddev),defaultWalkingStddevThreshold);

        float defaultStationaryStddevThreshold = Float.parseFloat(getString(R.string.pref_threshold_stationary_stddev_default));
        stationaryStddevThreshold = prefs.getFloat(getString(R.string.pref_key_threshold_stationary_stddev),defaultStationaryStddevThreshold);

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent = new Intent();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            intent.setClass(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void createDirectory(String directoryName){
        File directory = new File(directoryName);
        if (!directory.exists()) {
            if(!directory.mkdirs()){
                Toast.makeText(getBaseContext(),"mkdir failed "+directory, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void createFolders(){
//        createDirectory(SENSOR_DATA_TRAIN_FOLDER);
//        createDirectory(SENSOR_DATA_TEST_FOLDER);
        createDirectory(SENSOR_DATA_FOLDER);
        createDirectory(SENSOR_DATA_EXTRACT_FOLDER);
        createDirectory(SENSOR_DATA_RECORD_FOLDER);
        createDirectory(REQUIRED_FOLDER);
        createDirectory(ARFF_FOLDER);
        createDirectory(CLASSIFICATION_RESULT_FOLDER);
        createDirectory(HEAL_RESULT_FOLDER);
        createDirectory(JOURNEY_DATA_FOLDER);
        createDirectory(MODEL_FOLDER);
    }

    public void generateResults(String beginDate){
        String[] projection = {
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT,
                FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME
//                FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT
        };


//        Select * from users where created_at between '2017-05-05 10:45:46' and '2017-05-05 11:46:19'
        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + "  BETWEEN ? AND ?";
        String selecttionArgs[] = {
                beginDate,
                dateFormat.format(new Date())
        };

        String sortOrder = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME;// + DESC ve artan gibi bir şey eklenebilir

        Cursor cursor = db.query(
                FeedClassificationContract.FeedResult.TABLE_NAME,
                projection,
                selection,
                selecttionArgs,
                null,
                null,
                sortOrder
        );


        while (cursor.moveToNext()){
//            int itemId = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT));
            int result = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT));

//            Log.d(TAG, "result: " + result);


            this.cls_ViewPagerAdaptor.addImages(result);

//            Log.d(TAG,"DATE TIME: " + cursor.getString(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME)));
        }



        cursor.close();
    }
//

    public String generateBeginDate(){
        Calendar cal = Calendar.getInstance();
//        int k = Calendar.HOUR_OF_DAY
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

// get start of this week in milliseconds
        cal.set(Calendar.HOUR_OF_DAY , 0);
        return dateFormat.format(cal.getTime());
    }
//
//
//    public static ViewPagerAdaptor cls_ViewPagerAdaptor(){
//        return cls_ViewPagerAdaptor;
//    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        if (this.broadcastReceiver == null){
            this.broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    cls_ViewPagerAdaptor.addImages((int) intent.getExtras().get("result"));
                    result = (int) intent.getExtras().get("result");
                    cls_ViewPagerAdaptor.addImages(result);
                    MainActivity.this.onStart();
                }
            };

            registerReceiver(this.broadcastReceiver, new IntentFilter("resim"));
            this.cls_ViewPager.removeAllViews();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (this.broadcastReceiver != null){
            unregisterReceiver(this.broadcastReceiver);
        }


        if (db != null) {
            db.close();
        }

        if (this.db != null){
            this.db.close();
        }
    }



    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, "onStart");

        this.cls_ViewPager = (ViewPager) findViewById(R.id.clsViewPager);
        this.cls_ViewPagerAdaptor = new ViewPagerAdaptor(MainActivity.this);
        this.cls_ViewPager.setAdapter(this.cls_ViewPagerAdaptor);
        String beginDate = this.generateBeginDate();
        this.generateResults(beginDate);
        Log.d(TAG, "Begin Date: " + beginDate);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");


    }


}


