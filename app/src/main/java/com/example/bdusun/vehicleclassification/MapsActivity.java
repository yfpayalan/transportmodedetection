package com.example.bdusun.vehicleclassification;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bdusun.vehicleclassification.ClassificationDBHelper;
import com.example.bdusun.vehicleclassification.FeedClassificationContract;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.BufferedWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private String TAG = MapsActivity.class.getSimpleName();
    private Intent intent;
    private String action;
    private Marker myPositionMarker;
    private Polyline polyline;
    //private String endDate = "ongoing";
    float duration=1/60;
    private String date;
    private String resultColumn = FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT;

//    private int colorArray[] = {
//            Color.RED,
//            Color.CYAN,
//            Color.GREEN,
//            Color.GRAY,
//            Color.BLACK,
//            Color.BLUE,
//            Color.DKGRAY, Color.YELLOW, Color.LTGRAY, Color.MAGENTA, Color.WHITE};
    private String colorArrayHtml[] = {
            "#FF2E9A",
            "#674523",
            "#D2B48C",
            "#A8ADF5",
            "#E8F0F7",
            "#0F1BFE",
            "#000000"};

    private String resultNames[] = {
            "Walking",
//            "Minibus",
            "Car",
            "Bus",
            "Tram",
//            "Hafif Raylı Metro",
            "Metro",
            "Marmaray",
//            "Metrobus",
//            "Ferry",
            "Stationary"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        intent = getIntent();
//        action = intent.getAction();

        date = intent.getStringExtra("date"); // get date
        //resultColumn = intent.getStringExtra("resultcolumn"); // get healed results or normal results

        Log.d(TAG, "intent-date: " + date);
        Log.d(TAG, "intent-resultColumn" + resultColumn);

        Button dateButton = (Button) findViewById(R.id.button_date);
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });

//        Button changeModeButton = (Button) findViewById(R.id.button_mode);
//        changeModeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(resultColumn.equals(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT)){
//                    resultColumn = FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT;
//                } else {
//                    resultColumn = FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT;
//                }
//                Intent intent = MapsActivity.this.getIntent();
//                intent.putExtra("date",MapsActivity.this.date);
//                intent.putExtra("resultcolumn", resultColumn);
//                MapsActivity.this.finish();
//                MapsActivity.this.startActivity(intent);
//            }
//        });

        TextView legend = (TextView) findViewById(R.id.textview_legend);
        String text="";
        int i=0;
        for(String color : colorArrayHtml){
            text += "<font color=" + color + ">" + resultNames[i] + "</font> " ;
            if(i%4==0){
                text += "\n";
            }
            i++;
        }
        //String text = "<font color=#cc0029>Erste Farbe</font> <font color=#ffcc00>zweite Farbe</font>";
        legend.setText(Html.fromHtml(text));
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            legend.setText(Html.fromHtml(text,Html.FROM_HTML_MODE_LEGACY));
        } else {
            legend.setText(Html.fromHtml(text));
        }

//        //legend.setColor
//        String text = "<font color=#cc0029>Erste Farbe</font> <font color=#ffcc00>zweite Farbe</font>";
//        legend.setText(Html.fromHtml(text));
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            legend.setText(Html.fromHtml(text,Html.FROM_HTML_MODE_LEGACY));
//        } else {
//            legend.setText(Html.fromHtml(text));
//        }
//        legend.setBackgroundColor(Color.TRANSPARENT);
//        params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//        addContentView(legend, params);

//        Button button1 = new Button(this);
//        button1.setText("Date");
//
//        RelativeLayout.LayoutParams params = new RelativeLayout
//                .LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
//        addContentView(button1, params);
//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDatePickerDialog(v);
//            }
//        });
//
//        Button button2 = new Button(this);
//        button2.setText("Change Mode");
//        params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        //params.addRule(RelativeLayout.RIGHT_OF, button1.getId());
//        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
//        addContentView(button2, params);
//        button2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(resultColumn.equals(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT)){
//                    resultColumn = FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT;
//                } else {
//                    resultColumn = FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT;
//                }
//                Intent intent = MapsActivity.this.getIntent();
//                intent.putExtra("date",MapsActivity.this.date);
//                intent.putExtra("resultcolumn", resultColumn);
//                MapsActivity.this.finish();
//                MapsActivity.this.startActivity(intent);
//            }
//        });

    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        int year;
        int month;
        int day;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);

//            dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Healed", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    //Your code
//                    Calendar c = Calendar.getInstance();
//                    c.set(year,month,day,0,0);
//
//                    Log.d("date-healed",dateFormat.format(c.getTime()));
//
//                }
//            });

            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            final Calendar c = Calendar.getInstance();
            c.set(year,month,day,0,0);

            Intent intent = getActivity().getIntent();
            intent.putExtra("date",dateFormat.format(c.getTime()));
            //intent.putExtra("resultcolumn", FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT);
            getActivity().finish();
            getActivity().startActivity(intent);


        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

//        setMyPosition();

        //String resultColumn = FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT;

        drawPrimaryLinePath(generateResults(date, resultColumn));
//        date = "2017-06-05";
//        ClassificationDBHelper clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
//        SQLiteDatabase db = clsResDBHelper.getWritableDatabase();
//        db.rawQuery("DELETE "
//                + " FROM " + FeedClassificationContract.FeedResult.TABLE_NAME
//                + " WHERE " + FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " LIKE " + "'"
//                + date + "%'" , null).moveToFirst();
//        //cursor.close();
//        Log.d(TAG,date);
    }

//    private void drawPrimaryLinePath(List<MyLatLng> latLngList)
//    {
//        if ( mMap == null || latLngList.size() < 2) {
//            Log.e(TAG, "Empty latLngList");
//            return;
//        }
//        if(polyline!=null){polyline.remove();}
//
//        PolylineOptions options = new PolylineOptions();
//        options.color(Color.parseColor("#05b1fb"));
//        options.width(10);
//        options.visible( true );
//        for ( MyLatLng myLatLng : latLngList )
//        {
//            options.add( new LatLng( myLatLng.latitude,
//                    myLatLng.longitude ) );
//        }
//        polyline = mMap.addPolyline(options);
//        //return mMap.addPolyline( options );
//
//    }

    private void drawPrimaryLinePath(List<MyLatLng> latLngList)
    {
        if ( mMap == null || latLngList.size() < 2) {
            Log.e(TAG, "Empty latLngList, size: " +latLngList.size());
            return;
        }
        //if(polyline!=null){polyline.remove();}
        mMap.clear();

//        int colorArray[] = {Color.RED, Color.CYAN, Color.GREEN, Color.GRAY, Color.BLACK,
//                Color.BLUE, Color.DKGRAY, Color.YELLOW, Color.LTGRAY, Color.MAGENTA, Color.WHITE};
        int prevResult = -1;
        PolylineOptions options = null;

        MyLatLng myLatLngStart = latLngList.get(0);
        LatLng latLngStart = new LatLng(myLatLngStart.latitude,myLatLngStart.longitude);
        mMap.addMarker(new MarkerOptions().visible(true).title("Start").position(latLngStart));

        MyLatLng myLatLngEnd = latLngList.get(latLngList.size()-1);
        LatLng latLngEnd = new LatLng(myLatLngEnd.latitude,myLatLngEnd.longitude);
        mMap.addMarker(new MarkerOptions().visible(true).title("End").position(latLngEnd));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latLngList.get(0).latitude,latLngList.get(0).longitude)));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(15.0f));


        for ( MyLatLng myLatLng : latLngList )
        {
            Log.d(TAG, myLatLng.toString());
            int result = myLatLng.result;
            if(result == prevResult) {
                options.add( new LatLng( myLatLng.latitude,
                        myLatLng.longitude ) );
            }
            else {
                if(options!=null){
                    options.add( new LatLng( myLatLng.latitude,
                            myLatLng.longitude ) );
                    mMap.addPolyline(options);
                    Log.d(TAG, "Polyline added");
                }
                options = new PolylineOptions();
                //options.color(Color.parseColor("#05b1fb"));
                options.color(Color.parseColor(colorArrayHtml[result]));
                options.width(10);
                options.visible(true);
                options.add( new LatLng( myLatLng.latitude,
                        myLatLng.longitude ) );
            }
            prevResult = result;
        }
        mMap.addPolyline(options);
        //return mMap.addPolyline( options );

    }

    public List<MyLatLng> generateResults(String date, String resultColumn){
        ClassificationDBHelper clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        SQLiteDatabase db = clsResDBHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT " + FeedClassificationContract.FeedResult.COLUMN_NAME_LATITUDE + ","
        + FeedClassificationContract.FeedResult.COLUMN_NAME_LONGITUDE + ","
        + resultColumn
        + " FROM " + FeedClassificationContract.FeedResult.TABLE_NAME
        + " WHERE " + FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " LIKE " + "'" + date + "%'"
                + " AND " + FeedClassificationContract.FeedResult.COLUMN_NAME_LATITUDE + " IS NOT NULL" , null);

        List<MyLatLng> myLatLngList = new ArrayList<>();
        while (cursor.moveToNext()){

            int result = cursor.getInt(cursor.getColumnIndex(resultColumn));

            double latitude = cursor.getDouble(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_LATITUDE));
            double longitude = cursor.getDouble(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_LONGITUDE));
            MyLatLng myLatLng = new MyLatLng(latitude, longitude, result);
            myLatLngList.add(myLatLng);
            Log.d(TAG+"-query", date + " : " + myLatLng.toString());
        }
        cursor.close();

        return myLatLngList;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        //disableMyPosition();
        super.onStop();
    }

    private class MyLatLng{
        double latitude;
        double longitude;
        int result;

        public MyLatLng(double latitude, double longitude, int result) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.result = result;
        }

        public String toString(){
            return "latitude: " + this.latitude + "\nlongitude: " + longitude + "\nresult: " + result + "\n";
        }

//        public double getLatitude(){return this.latitude;}
//        public double getLongitude(){return this.longitude;}
//        public int getResult(){return this.result;}

    }


}
