//package com.example.bdusun.vehicleclassification;
//
//import android.util.Log;
//
//import java.io.BufferedWriter;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.Writer;
//
//import weka.classifiers.Classifier;
//import weka.core.DenseInstance;
//import weka.core.Instance;
//import weka.core.Instances;
//import weka.core.converters.ConverterUtils;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.REQUIRED_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
//import static com.example.bdusun.vehicleclassification.MainActivity.islem;
//
///**
// * Created by bdusun on 20/02/17.
// */
//
//public class RunnableClassify implements Runnable {
//
//
//
//    private String resp;
//    private long beginTime = System.currentTimeMillis();
//    private int baslangic;
//    private double degerler[][];
//    private static int argSayisi = 12;
//    private static int ozellikSayisi = 29;
//    private String label;
//    private int instanceIndex;
//
//    private String trainPath = REQUIRED_FOLDER + "model-17-02-17.m";;
//    //private String testPath;
//    private String resultPath = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME;
//
//
//    public RunnableClassify(int instanceIndex, int baslangic, double degerler[][], String label) {
//        this.baslangic = baslangic;
//        this.degerler = degerler;
//        this.label = label;
//        this.instanceIndex = instanceIndex;
//    }
//
//
//    @Override
//    public void run() {
//        try {
//            // Do your long operations here and return the result
//
//            //instanceIndex++;
//
//            Classifier cls = (Classifier) weka.core.SerializationHelper.read(trainPath);
//
//            // Read all the instances in the file (ARFF, CSV, XRFF, ...)
//            //ConverterUtils.DataSource testSource = new ConverterUtils.DataSource(testPath);
//            //Instances test = testSource.getDataSet();
//
//            //test.setClassIndex(test.numAttributes() - 1);
//
//            ConverterUtils.DataSource source = new ConverterUtils.DataSource(REQUIRED_FOLDER +"source.arff");
//            Instances instances = source.getDataSet();
//            instances.setClassIndex(argSayisi*ozellikSayisi);
//
//            Instance instance = new DenseInstance(argSayisi*ozellikSayisi+1);
//            instance.setDataset(instances);
//
//            double instanceValues[][] = new double[argSayisi][ozellikSayisi];
//
//            islem(degerler, baslangic, argSayisi, instanceValues);
//
//            int i, j, sayac;
//
//            sayac = 0;
//            for(i=0; i<argSayisi; i++) {
//                for(j=0; j<ozellikSayisi; j++){
//                    instance.setValue(sayac, instanceValues[i][j]);
//                    sayac++;
//                }
//            }
//
//            //instance.setClassMissing();
//            instance.setValue(sayac, label);
//
//            Writer writer = null;
//            try {
//                writer = new BufferedWriter(new OutputStreamWriter(
//                        new FileOutputStream(resultPath,true), "utf-8"));
//
//                double real = instance.classValue(); //Bunlar değişecek
//                double pred = cls.classifyInstance(instance); //double pred = rf.classifyInstance(test.instance(i));
//                String error = (((int) pred)==((int) real) ? "" : "+" );
//                writer.write((instanceIndex) +","
//                        + (int) real + ":"
//                        + instance.classAttribute().value((int) real) + ","
//                        + (int) pred + ":"
//                        + instance.classAttribute().value((int) pred) + ","
//                        + error + "," + "\n");
////                for(i=0; i<degerler.length; i++){
////                    for(j=0; j<degerler[i].length; j++){
////                        writer.write(degerler[i][j]+" ");
////                    }
////                    writer.write("\n");
////                }
//                Log.d("VEHCLA", (instanceIndex+1) +","
//                        + (int) real + ":"
//                        + instance.classAttribute().value((int) real) + ","
//                        + (int) pred + ":"
//                        + instance.classAttribute().value((int) pred) + ","
//                        + error + ",");
//
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            } finally {
//                try {
//                    writer.close();
//                } catch (Exception ex) {/*ignore*/}
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            resp = e.getMessage();
//        }
//
//
//    }
//}
