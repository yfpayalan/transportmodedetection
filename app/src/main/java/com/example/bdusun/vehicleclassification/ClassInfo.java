package com.example.bdusun.vehicleclassification;

import java.util.ArrayList;

/**
 * Created by bariscan on 12/09/2017.
 */

public class ClassInfo {
    private ArrayList<double[]> features;
    private int numOfFreq;
    private String dateTime;
    private boolean ekleOnay;
    private int indexOfClass;

    public ClassInfo() {
        this.features = new ArrayList<>();
        this.numOfFreq = 0;
        this.ekleOnay = false;
        this.indexOfClass = -1;
    }

    public void addClass(double[] classFeatures){
        this.features.add(classFeatures);
        this.numOfFreq++;
    }


    public ArrayList<double[]> getFeatures() {
        return features;
    }

    public int getNumOfFreq() {
        return numOfFreq;
    }


    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isEkleOnay() {
        return ekleOnay;
    }

    public void setEkleOnay(boolean ekleOnay) {
        this.ekleOnay = ekleOnay;
    }


    public int getIndexOfClass() {
        return indexOfClass;
    }

    public void setIndexOfClass(int indexOfClass) {
        this.indexOfClass = indexOfClass;
    }
}
