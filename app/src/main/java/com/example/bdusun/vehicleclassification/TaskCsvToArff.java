//package com.example.bdusun.vehicleclassification;
//
//import android.os.AsyncTask;
//import android.util.Log;
//import android.widget.Toast;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
//import static com.example.bdusun.vehicleclassification.MainActivity.processFile;
//
//
///**
// * Created on 24/01/17.
// */
//
//public class TaskCsvToArff extends AsyncTask<String,String,String> {
//
//    private String resp;
//    private long beginTime;
//    private double processedFileSize = 0;
//
//    String csvFolder;
//    String arffFolder;
//
//
//    public TaskCsvToArff(String csvFolder, String arffFolder) {
//        this.csvFolder = csvFolder;
//        this.arffFolder = arffFolder;
//    }
//
//    @Override
//    protected String doInBackground(String... params) {
//        //publishProgress("Processing file..."); // Calls onProgressUpdate()
//
////        String csvPath = params[0];
////        String arffFile = params[1];
//        String locationFile;
//        String arffFile;
//
//        String mode = params[0];
//
////        if(mode.equals("train")){
////            arffFile = arffFolder + TRAIN_ARFF_FILENAME;
////        }
////        else if (mode.equals("test")){
////            arffFile = arffFolder + TEST_ARFF_FILENAME;
////        }
////        else{
////            Log.d("VEHCLA-Csv2Arff","mode error!");
////            arffFile = null;
////        }
//
//        arffFile = arffFolder + mode + ".arff";
//        Log.d("VEHCLA-Csv2Arff", "csvFolder: " + csvFolder + " arffFile: " +arffFile);
//
////        String arffFile = arffFolder + CLASSIFICATION_RESULT_FILENAME;
////        locationFile += locationFolder + CLASSIFICATION_RESULT_FILENAME;
//
//        try {
//            // Do your long operations here and return the result
//            Log.d("VEHCLA", csvFolder + "  | " + arffFile);
//            //processedFileSize = processFile(csvPath,arffFile);
//            //processedFileSize = processFile(csvPath,arffFile,outputFileName);
//            processedFileSize = processFile(csvFolder,arffFile);
//            processedFileSize /= (1000000); //(1024*1024)
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            resp = e.getMessage();
//        }
//        return resp;
//    }
//
//    @Override
//    protected void onPostExecute(String result) {
//        // execution of result of Long time consuming operation
//        //finalResult.setText(result);
//        long endTime = System.currentTimeMillis();
//        long dif = endTime - beginTime;
//
//        // TODO Toast.makeText(getBaseContext(),processedFileSize + " MB Csv To Arff completed in " + dif + " ms" ,Toast.LENGTH_SHORT).show();
//        Log.d("VEHCLA-csv2arff", processedFileSize + " MB Csv To Arff completed in " + dif + " ms");
//
//    }
//
//    @Override
//    protected void onPreExecute() {
//        // Things to be done before execution of long running operation. For
//        // example showing ProgessDialog
//        beginTime = System.currentTimeMillis();
//    }
//
//    @Override
//    protected void onProgressUpdate(String... text) {
//        //finalResult.setText(text[0]);
//        // Things to be done while execution of long running operation is in
//        // progress. For example updating ProgessDialog
//    }
//
//
//
//
//}
