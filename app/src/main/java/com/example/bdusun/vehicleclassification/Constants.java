package com.example.bdusun.vehicleclassification;

import android.os.Environment;

/**
 * Created on 21/01/17.
 */

public class Constants {

    public interface ACTION {
        public static String MAIN_ACTION = "com.diex0n.mac.minisensormodule.action.main";
        public static String STARTFOREGROUND_ACTION = "com.diex0n.mac.minisensormodule.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.diex0n.mac.minisensormodule.action.stopforeground";
//        public static String RECORD_ACTION = "com.diex0n.mac.minisensormodule.action.record";

        public static String STARTBACKGROUND_ACTION = "com.diex0n.mac.minisensormodule.action.startbackground";
        public static String STOPBACKGROUND_ACTION = "com.diex0n.mac.minisensormodule.action.stopbackground";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
        public static int BACKGROUND_SERVICE = 102;
    }

    public interface PERMISSONS{
        public static int REQUEST_WRITE_EXTERNAL_STORAGE = 110;
        public static int REQUEST_ACCESS_FINE_LOCATION = 111;
    }

    public interface FILE{
        String EXTERNAL_STORAGE_DIRECTORY = Environment.getExternalStorageDirectory().toString() + "/";
        String APPLICATION_FOLDER = EXTERNAL_STORAGE_DIRECTORY + "TransportModeDetection/";
        String SENSOR_DATA_FOLDER = APPLICATION_FOLDER + "SensorData/";
//        String SENSOR_DATA_TRAIN_FOLDER = SENSOR_DATA_FOLDER + "Train/";
//        String SENSOR_DATA_TEST_FOLDER = SENSOR_DATA_FOLDER + "Test/";
        String SENSOR_DATA_RECORD_FOLDER = SENSOR_DATA_FOLDER + "Record/";
        String SENSOR_DATA_EXTRACT_FOLDER = SENSOR_DATA_FOLDER + "ExtractFrom/";
//        String SENSOR_DATA_TEST_FOLDER = SENSOR_DATA_FOLDER;
        String REQUIRED_FOLDER = APPLICATION_FOLDER + "Required/";
        String ARFF_FOLDER = APPLICATION_FOLDER + "Arff/";
        String MODEL_FOLDER = APPLICATION_FOLDER + "Model/";

        String CLASSIFICATION_RESULT_FOLDER = APPLICATION_FOLDER + "ClassificationResults/";
        String HEAL_RESULT_FOLDER = APPLICATION_FOLDER + "HealResults/";
//        String TRAIN_ARFF_FILENAME = "train.arff";
//        String TEST_ARFF_FILENAME = "test.arff";
        String CLASSIFICATION_RESULT_FILENAME = "classification_result.txt";
//        String HEAL_RESULT_FILENAME = "heal_result.txt";

        //String TRAIN_PATH = "";
        String TRAIN_PATH = REQUIRED_FOLDER + "all.model";
        String JOURNEY_DATA_FOLDER = APPLICATION_FOLDER + "JourneyData/";
        String SOURCE_PATH = REQUIRED_FOLDER + "source.arff";
//        String CSV2DEGERLER_RESULT_PATH = CLASSIFICATION_RESULT_FOLDER + "csv2degerler.txt";
//        String CLASSIFICATION_RESULT_FILENAME2 = "classification_result_debug.txt";
//        String TEST_ARFF_PATH = ARFF_FOLDER + "test.arff";

        String precision = "%.3f";
    }

    public interface TRAVEL_MODE{
        public static int TRAVEL_MODE_PEDESTRIAN = 0;
        public static int TRAVEL_MODE_STATIONARY = -1;
        public static int TRAVEL_MODE_MOTORIZED = -2;
    }


//    public interface THRESHOLD{
//        public static String PREFERENCE_FILE_KEY = "";
//        public static String THRESHOLD_WALKING_COUNT = 0;
//        public static String THRESHOLD_STATIONARY_COUNT = -1;
//        public static String THRESHOLD_WALKINGANDSTATIONARY_COUNT = -1;
//    }


}