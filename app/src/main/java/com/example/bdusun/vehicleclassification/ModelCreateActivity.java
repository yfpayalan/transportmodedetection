package com.example.bdusun.vehicleclassification;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.bdusun.vehicleclassification.Functions.AddToArff;
import com.example.bdusun.vehicleclassification.Functions.CreateNewModel;
import com.example.bdusun.vehicleclassification.Functions.ReadinArffDir;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;

import static com.example.bdusun.vehicleclassification.Constants.FILE.ARFF_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.MODEL_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.REQUIRED_FOLDER;
import static com.example.bdusun.vehicleclassification.MainActivity.heal;

/**
 * Created by bariscan on 12/09/2017.
 */

public class ModelCreateActivity extends AppCompatActivity{
    private static final String TAG = "ModelCreateActivity";


    private static final int STATIONARY_INDIS = 11;
    private static final int WALKING_INDIS = 0;



    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private Button addToArff_Button;
    private Button createModel_Button;

    private List<ClassInfo> classInfoList;

    private Intent intent;
    private String date;

//    private static int argSayisi = 12;
//    private static int ozellikSayisi = 29;

    private SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        intent = getIntent();
        date = intent.getStringExtra("date"); // get date


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//
//
//
//        fab.setOnClickListener(new View.OnClickListener() {
//
//            FloatingActionButton fabIn = (FloatingActionButton) ModelCreateActivity.this.findViewById(R.id.fab);
//
//            @Override
//            public void onClick(View view) {
//
//                //TODO: çok büyük ihtimal seçilmiş olanları arff ye append etme bu buton ile gerçekleşecek
//                Snackbar.make(view, "MENU ACTIVITY", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//                createModel();
//            }
//        });

        ApplicationDrawer applicationDrawer = new ApplicationDrawer(this);
        this.classInfoList = new ArrayList<>();

//        Log.d(TAG,"burda1");
//        this.generateValue();
//        Log.d(TAG,"burda2");



//        this.generateClassInfoList();
        new ReadinArffDir(this, date, this.classInfoList).execute("https://mcdn01.gittigidiyor.net/28981/tn30/289818940_tn30_0.jpg");
        this.initializeLayout();
//        Log.d(TAG,"burda3");
//        this.loadAdaptor();
//        Log.d(TAG,"burda4");






        Log.d(TAG, "date: " + date);

    }


    public void loadAdaptor(){
//        String title = getIntent().getStringExtra(Constant.IntentKeys.TITLE_INTENT_KEY);
//        this.setTitle(title);
//
//        Bundle b = getIntent().getBundleExtra(Constant.IntentKeys.LIST_ITEMS_INTENT_KEY);
//        this.listItems = (List<FilesListItem>) b.getSerializable(Constant.IntentKeys.LIST_ITEMS_KEY);

//        for(ClassInfo ci : this.classInfoList){
//            Log.d(TAG, "index_of_class: " + ci.getIndexOfClass());
//        }
        adapter = new ModelCreateAdapter(this.classInfoList);
        recyclerView.setAdapter(adapter);

//        Log.d(TAG, listItems.size() + "");

    }

    public void CreateModel(View view){
        new CreateNewModel(this).execute("https://mcdn01.gittigidiyor.net/28981/tn30/289818940_tn30_0.jpg");
//        Log.d(TAG, "create Model");
//        this.createModel_Button.setClickable(false);
//        this.createModel_Button.setText("Model Oluşturuluyor");
//        String modelPath = REQUIRED_FOLDER + "all.model";
//        String oldModelPath = REQUIRED_FOLDER + "all_" + this.dateFormatFileName.format(new Date()) + ".model";
//        String yeni = REQUIRED_FOLDER + "yeni.model";
//        try {
//            this.fileClone(modelPath,oldModelPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        BufferedReader reader = null;
//        int argSayisi = 12;
//        int ozellikSayisi = 29;
//        try {
//            reader = new BufferedReader(new FileReader(REQUIRED_FOLDER + "all.arff"));
//            Instances train = new Instances(reader);
//            train.setClassIndex(argSayisi*ozellikSayisi+1);
//            reader.close();
//
//            RandomForest rf = new RandomForest();
//            rf.buildClassifier(train);
//
//            ObjectOutputStream oos = new ObjectOutputStream(
//                    new FileOutputStream(yeni));
//            oos.writeObject(rf);
//            oos.flush();
//            oos.close();
////            weka.core.SerializationHelper.write(modelPath, rf);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        this.createModel_Button.setText("Model Oluşturuldu");
//        this.createModel_Button.setClickable(true);
//        Toast.makeText(this,"Model oluşturuldu",Toast.LENGTH_LONG).show();
//        Log.d(TAG, "create Model END");
    }


    public void SelectAll(View view){
        for(ClassInfo classInfo:this.classInfoList){
//            System.out.println(classInfo.isEkleOnay());
            if ((classInfo.getIndexOfClass() != STATIONARY_INDIS) && (classInfo.getIndexOfClass() != WALKING_INDIS)){
                classInfo.setEkleOnay(true);
            }

        }
        this.loadAdaptor();

    }

    public void initializeLayout(){
        this.addToArff_Button = (Button) findViewById(R.id.addToArff_Button);
        this.createModel_Button = (Button) findViewById(R.id.createModel_Button);
        this.recyclerView = (RecyclerView) findViewById(R.id.menu_recyclerView);
        Log.d(TAG, "recy view: " + this.recyclerView);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        this.loadAdaptor();
    }




//    private void generateClassInfoList(){
//        List<String> filenames = getFilenames(ARFF_FOLDER, this.date);
//        int i;
//        int numOfFiles = filenames.size();
//        Log.d(TAG, "numOfFiles: " + numOfFiles);
//        for(i=0; i<numOfFiles;i++){
//            this.addFromFileToClassInfoList(filenames.get(i));
//        }
//
//    }

//    private void addFromFileToClassInfoList(String fileName){
//        int i, j, k;
//        double features[];
//        int results[] = new int[0];
//        int indis = 0;
//
//        int indexOfClass;
//        String dateTime;
//        int numOfResults;
//        ClassInfo classInfo = new ClassInfo();
//
//
//        try {
//            results = healFile(fileName);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        numOfResults = results.length;
//        try {
////            Log.d(TAG, "readAFileAndInsertSQL: " + fileName);
//            Scanner scanner = new Scanner(new File(ARFF_FOLDER + fileName));
//            scanner.useDelimiter(" ");
//
//            String tmpS;
//            String tmpS2;
//            int tmpI;
//            for (k = 0; k < numOfResults; k++){
//                //TODO: burada debug yapılacak
//                features = new double[argSayisi*ozellikSayisi];
//
//                //ozellikleri alıyor.
//                for(i=0; i<argSayisi; i++) {
//                    for(j=0; j<ozellikSayisi; j++){
//                        features[indis] = Double.parseDouble(scanner.next());
////                        Log.d(TAG, "nextDouble: " + scanner.next());
//                        indis++;
//                    }
//                }
//
//                scanner.nextInt();
//                indexOfClass = results[k];
//                dateTime = scanner.nextLine();
//                if (classInfo.getIndexOfClass() != indexOfClass){
//                    if (classInfo.getIndexOfClass() != -1){
//                        this.classInfoList.add(classInfo);
//                    }
//
//                    classInfo = new ClassInfo();
//                    classInfo.setDateTime(dateTime);
//                }
//                classInfo.addClass(features);
//                classInfo.setIndexOfClass(indexOfClass);
//                indis = 0;
//            }
//
//            while(scanner.hasNextLine()){
//
//            }
//
//            if (classInfo.getIndexOfClass() != -1){
//                this.classInfoList.add(classInfo);
//            }
//
//        } catch (FileNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }


//    private int[] healFile(String fileName) throws FileNotFoundException {
//        int numOfResult = 0;
//        int i, j, k;
//        int results[];
//        Scanner scanner = new Scanner(new File(ARFF_FOLDER + fileName));
//        scanner.useDelimiter(" ");
//        while (scanner.hasNextLine()){
//            scanner.nextLine();
//            numOfResult++;
//        }
//
//        results = new int[numOfResult];
//
//        scanner = new Scanner(new File(ARFF_FOLDER + fileName));
//        scanner.useDelimiter(" ");
//
//        for (k = 0; k < numOfResult; k++){
//            for(i=0; i<argSayisi; i++) {
//                for(j=0; j<ozellikSayisi; j++){
//                    scanner.next();
//                }
//            }
//            results[k] = scanner.nextInt();
//            scanner.nextLine();
//        }
//
//        heal(results, numOfResult, 12);
//        return results;
//    }

//    private int howMuchLine(String fileName){
//
//    }

//    private List<String> getFilenames(String directory, String whichDate) {
//
//        List<String> files = new ArrayList<String>();
//        File folder = new File(directory);
//        File[] listOfFiles = folder.listFiles();
//        if (listOfFiles!=null){
//            for (int i = 0; i < listOfFiles.length; i++) {
//                String filename = listOfFiles[i].getName();
//                Log.d(TAG,"fileName: " + filename);
//                Log.d(TAG,"whichDate: " + whichDate);
//                if (filename.startsWith(whichDate)) {
//                    //System.out.println("File " + listOfFiles[i].getName());
//                    files.add(filename);
//                }
//            }
//            return files;
//        }
//        else
//            return null;
//    }

    private void generateValue(){
        ClassInfo classInfo = new ClassInfo();
        classInfo.addClass(null);
        classInfo.addClass(null);
        classInfo.addClass(null);

        classInfo.setDateTime("dateTime1wqeçqwewqeqöwe");
        classInfoList.add(classInfo);

        classInfo = new ClassInfo();
        classInfo.addClass(null);
        classInfo.addClass(null);
        classInfo.addClass(null);

        classInfo.addClass(null);
        classInfo.addClass(null);
        classInfo.addClass(null);

        classInfo.setDateTime("dateTime2ewqeqopwepqoweq");

        classInfoList.add(classInfo);


        classInfo = new ClassInfo();
        classInfo.addClass(null);
        classInfo.addClass(null);
        classInfo.addClass(null);

        classInfo.addClass(null);
        classInfo.addClass(null);
        classInfo.addClass(null);

        classInfo.addClass(null);
        classInfo.addClass(null);

        classInfo.addClass(null);
        classInfo.addClass(null);
        classInfo.addClass(null);

        classInfo.setDateTime("dateTime3qweqweqwewq");

        classInfoList.add(classInfo);
    }


    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new MapsActivity.DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void AddToArff(final View view){
        new AddToArff(this, this.classInfoList).execute("https://mcdn01.gittigidiyor.net/28981/tn30/289818940_tn30_0.jpg");
//        this.addToArff_Button.setClickable(false);
//        this.addToArff_Button.setText("Arff'ye EKLENİYOR");
//        this.addToArff_Button.setVisibility(View.INVISIBLE);
//        Writer writer = null;
//        int i;
//
//        String allArffPath = REQUIRED_FOLDER + "all.arff";
//        String oldArffPath = REQUIRED_FOLDER + "all_" + dateFormatFileName.format(new Date()) + ".arff";
//
//        try {
//            fileClone(allArffPath, oldArffPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//
////
//
//        for (ClassInfo classInfo:classInfoList){
//            Log.d(TAG, "sinif indisi: " + classInfo.getIndexOfClass());
//            Log.d(TAG, "ekle_onay: " + classInfo.isEkleOnay());
//            if (classInfo.isEkleOnay()){
//                Log.d(TAG, "tarih: " + classInfo.getDateTime());
//
//                try {
//                    writer = new BufferedWriter(new OutputStreamWriter(
//                            new FileOutputStream(allArffPath, true), "utf-8"));
//
//                    for (double [] results:classInfo.getFeatures()){
//                        for (double result:results){
//                            writer.write(result + ",");
//                        }
//                        writer.write(resultNames[classInfo.getIndexOfClass()]+"\n");
//                    }
//
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } finally {
//                    try {
//                        writer.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        }

//        addToArff_Button.setText("Arff'ye EKLENDİ");
//        addToArff_Button.setClickable(true);
//        Toast.makeText(this,"Arff oluşturuldu",Toast.LENGTH_LONG).show();




    }







    public void fileClone(String oldPath, String newPath) throws IOException {
        int BUFFER_LEN = 1024;

        InputStream inputStream = new FileInputStream(oldPath);
        OutputStream outputStream = new FileOutputStream(newPath);

        byte [] buffer = new byte[BUFFER_LEN];
        int len ;

        while ((len = inputStream.read(buffer)) > 0){
            outputStream.write(buffer, 0, len);
        }

        inputStream.close();
        outputStream.close();

//        BufferedReader br = null;
//        PrintWriter pw = null;
//
//        try {
//            br = new BufferedReader(new FileReader(sourceFile));
//            pw = new PrintWriter(new FileWriter(destFile));
//
//            String line;
//            while ((line = br.readLine()) != null) {
//                pw.println(line);
//                System.out.println(line);
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }



    }



}
