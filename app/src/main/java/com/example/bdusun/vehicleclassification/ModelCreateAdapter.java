package com.example.bdusun.vehicleclassification;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

/**
 * Created by bariscan on 12/09/2017.
 */

public class ModelCreateAdapter extends RecyclerView.Adapter<ModelCreateAdapter.ViewHolder> {
    private static final String TAG = "ModelCreateAdapter";

    private List<ClassInfo> classInfoList;



    public ModelCreateAdapter(List<ClassInfo> classInfoList){
        this.classInfoList = classInfoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_menu, parent, false);
        return new ViewHolder(v) ;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ClassInfo classInfo = this.classInfoList.get(position);

        holder.numOfClass_TextView.setText(classInfo.getNumOfFreq() + "");
        holder.dateOfClass_TextView.setText(classInfo.getDateTime());
        holder.nameOfClass_Spinner.setSelection(classInfo.getIndexOfClass());
//        Log.d(TAG, "pos: " + position + " : " + classInfo.isEkleOnay());
        holder.ekleOnay_CheckBox.setChecked(classInfo.isEkleOnay());

        holder.ekleOnay_CheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override

            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                Log.d(TAG, "isChecked: " + isChecked);
                Log.d(TAG,"ekleOnay: " + classInfo.isEkleOnay());
//                classInfo.setEkleOnay(isChecked);
//                Log.d(TAG)
                classInfoList.get(holder.getPosition()).setEkleOnay(isChecked);
//                Log.d(TAG, "hold-pos: " + holder.getPosition());
            }
        }
        );

        holder.nameOfClass_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Your code here
                Log.d(TAG, "i: " + i + " l: " + l);
                classInfo.setIndexOfClass(i);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

    }

    @Override
    public int getItemCount() {
        if (this.classInfoList != null){
            return this.classInfoList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
//        private List<String> resultNames;
        public TextView numOfClass_TextView;
        public TextView dateOfClass_TextView;
        public Spinner nameOfClass_Spinner;
        public CheckBox ekleOnay_CheckBox;

        public ViewHolder(View itemView) {
            super(itemView);

            this.numOfClass_TextView = (TextView) itemView.findViewById(R.id.num_of_class_textView);
            this.dateOfClass_TextView = (TextView) itemView.findViewById(R.id.date_of_class_textView);
            this.nameOfClass_Spinner = (Spinner) itemView.findViewById(R.id.name_of_class_spinner);
            this.ekleOnay_CheckBox = (CheckBox) itemView.findViewById(R.id.ekle_onay_checkbox);
        }
    }
}
