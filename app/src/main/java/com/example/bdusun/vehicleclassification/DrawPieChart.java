//package com.example.bdusun.vehicleclassification;
//
//import android.app.Activity;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.github.mikephil.charting.charts.PieChart;
////import com.github.mikephil.charting.components.Description;
//import com.github.mikephil.charting.components.Legend;
//import com.github.mikephil.charting.data.Entry;
//import com.github.mikephil.charting.data.PieData;
//import com.github.mikephil.charting.data.PieDataSet;
////import com.github.mikephil.charting.data.PieEntry;
//import com.github.mikephil.charting.formatter.PercentFormatter;
//import com.github.mikephil.charting.highlight.Highlight;
//import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
//import com.github.mikephil.charting.utils.ColorTemplate;
//
//import java.util.ArrayList;
//
///**
// * Created by bariscan on 16/04/2017.
// */
//
//public class DrawPieChart extends Activity{
//
//    private static String TAG = "DrawPieChart";
////    private float[] yData = {25.3f,10.6f,66.76f,44.32f,46,01f,16.89f,23.9f};
////    private String[] xData = {"a","b","c","d","e","f","g"};
//private float[] yData = {5,10,15,30,40};
//    private String[] xData = {"Sony","Huawei","LG","Apple","Samsung"};
//    PieChart pieChart;
//
//
//    ArrayList<PieEntry> yPieEntries = new ArrayList<>();
//    ArrayList<String> xPieEntries = new ArrayList<>();
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_draw_pie_chart);
//        Log.d(TAG, "starting pie chart");
//        Description desc = new Description();
//        desc.setText("Araç kullanım oranları");
//        pieChart = (PieChart) findViewById(R.id.aracSayisi_pieChart);
//        //burada ilk videodan yaptıklarım var
//        System.out.println("PieChart: " + pieChart);
//        pieChart.setCenterText("asda");
//        pieChart.setDescription(desc);
//        pieChart.setRotationEnabled(true);
//        pieChart.setHoleRadius(25f);
//        pieChart.setTransparentCircleAlpha(0);
//        pieChart.setCenterText("Araç Sınıfları");
//        pieChart.setCenterTextSize(10);
//        pieChart.setDrawEntryLabels(true);
//
//        addDataSet();
//
//        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
//            @Override
//            public void onValueSelected(Entry e, Highlight h) {
//                Log.d(TAG,"Tıkladı");
//
//                String message = "";
//                if (e == null){
//                    return;
//                }
//
//
//                System.out.println("indis: " + e.getX());
//                System.out.println("indis: " + e.getY());
//                System.out.println("indis: " + e.getData());
//                System.out.println("indis: " + h.getX());
//                System.out.println("indis: " + h.getDataIndex());
//                System.out.println("indis: " + h.getY());
//                System.out.println("indis: " + h.getAxis());
//
////xData[(int) e.getData()]
//                message = xData[(int) h.getX()]  + " = " + e.getY() + "%";
//
//                yPieEntries.set((int) h.getX(), new PieEntry(e.getY() + 1, xData[(int) h.getX()]));
//                Toast.makeText(DrawPieChart.this,message,Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onNothingSelected() {
//
//            }
//        });
//
//
//
//        //ikinci videodan yazdıklarım
////        pieChart.setUsePercentValues(true);
////        pieChart.setDescription(desc);
////        pieChart.setDrawHoleEnabled(true);
////        pieChart.setHoleRadius(7);
////        pieChart.setTransparentCircleRadius(10);
////        pieChart.setRotationAngle(0);
////        pieChart.setRotationEnabled(true);
////
////        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
////            @Override
////            public void onValueSelected(Entry e, Highlight h) {
////                Log.d(TAG,"Tıkladı");
////                String message = "";
////                if (e == null){
////                    return;
////                }
////
////
////
////
////                message = xData[(int) e.getData()] + " = " + e.getY() + "%";
////                Toast.makeText(DrawPieChart.this,message,Toast.LENGTH_SHORT).show();
////            }
////
////            @Override
////            public void onNothingSelected() {
////
////            }
////        });
////
////        addData();
////
////        Legend l = pieChart.getLegend();
////        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
////        l.setXEntrySpace(7);
////         l.setYOffset(5);
//
//
//
//    }
//
//
////    private void addData(){
////        int i;
////
////        ArrayList<PieEntry> yVals1 = new ArrayList<>();
////        ArrayList<String> xVals1 = new ArrayList<>();
////
////        for (i = 0; i < yData.length; i ++){
////            yVals1.add(new PieEntry(yData[i], i));
////        }
////
////        for (i = 0; i < xData.length; i ++){
////            xVals1.add(xData[i]);
////        }
////
////        PieDataSet dataSet = new PieDataSet(yVals1, "Market Share");
////        dataSet.setSliceSpace(3);
////        dataSet.setSelectionShift(5);
////
////
////        ArrayList<Integer> colors = new ArrayList<>();
////        for (int c: ColorTemplate.VORDIPLOM_COLORS){
////            colors.add(c);
////        }
////
////        for (int c: ColorTemplate.JOYFUL_COLORS){
////            colors.add(c);
////        }
////
////        for (int c: ColorTemplate.COLORFUL_COLORS){
////            colors.add(c);
////        }
////
////        for (int c: ColorTemplate.LIBERTY_COLORS){
////            colors.add(c);
////        }
////
////        for (int c: ColorTemplate.PASTEL_COLORS){
////            colors.add(c);
////        }
////
////
////
////        colors.add(ColorTemplate.getHoloBlue());
////        dataSet.setColors(colors);
////
////        PieData data = new PieData(dataSet);
////        dataSet.setValueFormatter(new PercentFormatter());
////        dataSet.setValueTextSize(11f);
////        dataSet.setValueTextColor(Color.GRAY);
////
////        pieChart.setData(data);
////        pieChart.highlightValues(null);
////        pieChart.invalidate();
////
////
////    }
//
//
//
//    //ilk videodaki yaptığım şey
//    private void addDataSet(){
//        Log.d(TAG, "add data set");
//
//
//        int i;
//        for (i = 0; i < yData.length; i++){
//            yPieEntries.add(new PieEntry(this.yData[i], this.xData[i]));
//        }
//
//        for (i = 0; i < xData.length; i++){
//            xPieEntries.add(this.xData[i]);
//        }
//
//        PieDataSet pds= new PieDataSet(yPieEntries,"Araç Sınıf");
//        pds.setSliceSpace(2);
//        pds.setValueTextSize(12);
//
//        ArrayList<Integer> colors = new ArrayList<>();
////        colors.add(Color.GRAY);
////        colors.add(Color.BLUE);
////        colors.add(Color.RED);
////        colors.add(Color.GREEN);
////        colors.add(Color.CYAN);
////        colors.add(Color.YELLOW);
////        colors.add(Color.MAGENTA);
//
//        for (int c: ColorTemplate.VORDIPLOM_COLORS){
//            colors.add(c);
//        }
//
//        for (int c: ColorTemplate.JOYFUL_COLORS){
//            colors.add(c);
//        }
//
//        for (int c: ColorTemplate.COLORFUL_COLORS){
//            colors.add(c);
//        }
//
//        for (int c: ColorTemplate.LIBERTY_COLORS){
//            colors.add(c);
//        }
//
//        for (int c: ColorTemplate.PASTEL_COLORS){
//            colors.add(c);
//        }
////
//        pds.setColors(colors);
//
//
//        Legend legend = pieChart.getLegend();
//        legend.setForm(Legend.LegendForm.CIRCLE);
//        legend.setPosition(Legend.LegendPosition.LEFT_OF_CHART);
//
//        PieData pd = new PieData(pds);
//        pieChart.setData(pd);
//        pieChart.invalidate();
//    }
//
//}
