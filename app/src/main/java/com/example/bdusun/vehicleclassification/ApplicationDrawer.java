package com.example.bdusun.vehicleclassification;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bdusun on 02/05/17.
 */

public class ApplicationDrawer implements NavigationView.OnNavigationItemSelectedListener  {

    private Activity mActivity;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private String date;
    public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    public ApplicationDrawer(Activity activity){
        this.mActivity = activity;

        navigationView = (NavigationView) mActivity.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);

        drawer = (DrawerLayout) mActivity.findViewById(R.id.drawer);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                mActivity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent intent = new Intent();

        if (id == R.id.nav_travel) {
            // Handle the camera action
            intent.setClass(mActivity, TravelOperationsActivity.class);
            mActivity.startActivity(intent);
        } else if (id == R.id.nav_route) {
            intent.setClass(mActivity, MapsActivity.class);
            String date = dateFormat.format(new Date());
            Log.d("AppDrawer", date);
            intent.putExtra("date", date);
            intent.putExtra("resultcolumn", FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT);
            mActivity.startActivity(intent);
        } else if (id == R.id.nav_statistics) {
            intent.setClass(mActivity, StatisticsActivity.class);
            mActivity.startActivity(intent);
        } else if (id == R.id.nav_dailystatistics) {
            intent.setClass(mActivity, DailyResultActivity.class);
            String date = dateFormat.format(new Date());
            Log.d("AppDrawer", date);
            intent.putExtra("date", date);
            intent.setClass(mActivity, DailyResultActivity.class);
            mActivity.startActivity(intent);
        }
        else if (id == R.id.nav_settings) {
            intent.setClass(mActivity, SettingsActivity.class);
            mActivity.startActivity(intent);
        }
        else if (id == R.id.nav_menu) {
            intent.setClass(mActivity, ModelCreateActivity.class);
            String date = dateFormat.format(new Date());
            Log.d("AppDrawer", date);
            intent.putExtra("date", date);
            mActivity.startActivity(intent);
        }
//        else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) mActivity.findViewById(R.id.drawer);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }





}
