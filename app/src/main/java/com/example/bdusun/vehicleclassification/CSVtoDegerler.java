//package com.example.bdusun.vehicleclassification;
//
//import android.util.Log;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.Writer;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Scanner;
//
//import weka.classifiers.Classifier;
//import weka.core.DenseInstance;
//import weka.core.Instance;
//import weka.core.Instances;
//import weka.core.converters.ConverterUtils;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME2;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TEST_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.SOURCE_PATH;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.TRAIN_PATH;
//import static com.example.bdusun.vehicleclassification.MainActivity.islem;
//
//public class CSVtoDegerler /*implements Runnable*/ {
//
//    private double degerler[][];
//    private double degerlerCPY[][];
//    private String label;
//
//    private String data;
//    //private final int argSayisi = 9;
//    private int degerlerBoyut;
//    private int atlamaMiktari;
//    private File folder;
//    private File[] listOfFiles;
//
//    public static SimpleDateFormat newDateFormatFileName = new SimpleDateFormat("yyyyMMdd");
//    public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm");
//    //public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm:ss");
//
//    private Thread t;
//
//    private Classifier cls = null;
//    private ConverterUtils.DataSource source = null;
//    private Instances instances = null;
//    private double instanceValues[][] = null;
//    private Instance instance = null;
//
//    private static int argSayisi2 = 12;
//    private static int ozellikSayisi = 29;
//
//    private int instanceIndex=0;
//    //private String trainPath = REQUIRED_FOLDER + "271116cd1ov.model";
//    //    private String trainPath = REQUIRED_FOLDER + "model-17-02-17.m";
//    private String trainPath = TRAIN_PATH;
//    private String resultPath = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME;
//    private String resultPath2 = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME2;
//
//    public CSVtoDegerler(/*String folderName, */int degerlerBoyut, double overlapOrani) {
//        this.folder = new File(SENSOR_DATA_TEST_FOLDER); // bu kisim androidde degisecek
//        this.listOfFiles = this.folder.listFiles();
//        this.degerlerBoyut = degerlerBoyut;
//
//        this.atlamaMiktari = (int) (degerlerBoyut*(1 - overlapOrani));
//        degerler = new double [argSayisi2][degerlerBoyut];
//        //degerlerCPY = new double [12][this.degerlerBoyut];
//
//        try {
//            cls = (Classifier) weka.core.SerializationHelper.read(trainPath);
//            source = new ConverterUtils.DataSource(SOURCE_PATH);
//            instances = source.getDataSet();
//
//            instances.setClassIndex(argSayisi2*ozellikSayisi);
//
//            instance = new DenseInstance(argSayisi2*ozellikSayisi+1);
//            instance.setDataset(instances);
//
//            instanceValues = new double[argSayisi2][ozellikSayisi];
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//
//    public void oku() throws FileNotFoundException, InterruptedException{
////    	long startTime;
////    	long endTime;
////    	long duration;
//        int instanceIndex = 0;
//        int degerSayisi = 0;
//        int r = 0;
//        int i, indis;
//
//        Scanner scanner;
//        String fileName;
//        //RunnableClassify rc;
//        //Thread t;
////    	System.out.println(this.listOfFiles.length);
//        for (File file : this.listOfFiles) {
//            fileName = file.getName();
//            if (this.isCSV(fileName)) {
//                System.out.println(file.getName() + " okunuyor...");
//                scanner = new Scanner(file);
//                scanner.useDelimiter(",");
//                scanner.nextLine();
//                //like initialize
//                label = fileName.split("_")[1];
//
//                while(scanner.hasNextLine()){
//                    if(degerSayisi < this.degerlerBoyut){
//                        for(i = 3; i < argSayisi2; i++){
//                            this.degerler[i][r] = scanner.nextDouble();
//                        }
//                        for(i = 0; i < 3; i++){
//                            indis = (i + 1) * 3;
//                            this.degerler[r][i] = Math.sqrt(Math.pow(this.degerler[indis][r], 2)
//                                    + Math.pow(this.degerler[indis + 1][r], 2)
//                                    + Math.pow(this.degerler[indis + 2][r], 2));
//                        }
//                        r++;
//                        r %= this.degerlerBoyut;
//                        degerSayisi++;
//                        scanner.nextLine();
//
//                    }else{
//
//                        instanceIndex++;
//
//                        degerSayisi -= atlamaMiktari;
//
//                        //System.arraycopy(degerler, 0, degerlerCPY, 0, degerler.length);
//                        //islem(degerlerCPY, r, argSayisi2, instanceValues);
//                        int result = islem(degerler, r, argSayisi2, instanceValues);
//
//                        int j, sayac;
//
//                        sayac = 0;
//                        for(i=0; i<argSayisi2; i++) {
//                            for(j=0; j<ozellikSayisi; j++){
//                                instance.setValue(sayac, instanceValues[i][j]);
//                                sayac++;
//                            }
//                        }
//
//                        //instance.setClassMissing();
//                        instance.setValue(sayac, label);
//
//
//                        //double instanceValues[][] = new double[argSayisi2][ozellikSayisi];
//
//                        String timeStamp = newDateFormat.format(new Date());
//                        String location = null;
//
//                        int thresholdResult = islem(degerler, r, argSayisi2, instanceValues);
//
//                        //double prediction;
//
//                        String classificationResult = null;
//
//                        String thresholdClassificationResult = "VEHICULAR";
//
//                        int thresholdIndis = -1;
//
//                        switch (thresholdResult) {
//
//                            case -1:
//                                //classify
//                                thresholdIndis = -1;
//                                break;
//                            case 10:
//                                //stationary
//                                classificationResult = "STATIONARY";
//                                thresholdClassificationResult = classificationResult;
//                                thresholdIndis = 10;
//                                break;
//                            case 0:
//                                //walking
//                                classificationResult = "WALKING";
//                                thresholdClassificationResult = classificationResult;
//                                thresholdIndis = 0;
//                                break;
//                            default:
//                                //error
//                                classificationResult = "error wrong class: " + result;
//                                thresholdClassificationResult = classificationResult;
//                                break;
//                        }
//
//                        //bu case -1'de olacak, debug amacli burda
//                        Instance instance = new DenseInstance(argSayisi2*ozellikSayisi+1);
//                        instance.setDataset(instances);
//                        sayac = 0;
//                        for(i=0; i<argSayisi2; i++) {
//                            for(j=0; j<ozellikSayisi; j++){
//                                instance.setValue(sayac, instanceValues[i][j]);
//                                sayac++;
//                            }
//                        }
//                        instance.setValue(sayac, "bus");
//                        try {
//                            result = (int) cls.classifyInstance(instance);
//                            classificationResult = instance.classAttribute().value(result);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        Writer writer = null;
//
//                        //TODO debugClassificationResult classificationResult ile yer degistirecek
//                        try {
//                            //System.out.println(journeyFilePath);
//                            writer = new BufferedWriter(new OutputStreamWriter(
//                                    new FileOutputStream(resultPath, true), "utf-8"));
//
//                            writer.write(
//                                    timeStamp
//                                            + " "
//                                            + thresholdClassificationResult
//                                            + " "
//                                            + thresholdIndis
//                                            + " "
//                                            + classificationResult
//                                            + " "
//                                            + result
//                                            + " "
//                                            + location
//                                            + "\n");
//
//                            Log.d("VEHCLA", timeStamp + "," + thresholdClassificationResult + ","
//                                    + classificationResult + ":" + result + "," + location);
//
//                        } catch (IOException ex) {
//                            ex.printStackTrace();
//                        } finally {
//                            try {
//                                writer.close();
//                            } catch (Exception ex) {/*ignore*/}
//                        }
//
//
////                        Writer writer2 = null;
////                        try {
////                            writer2 = new BufferedWriter(new OutputStreamWriter(
////                                    new FileOutputStream(resultPath2,true), "utf-8"));
//=======
////package com.example.bdusun.vehicleclassification;
//>>>>>>> origin/master
////
////import android.util.Log;
////
////import java.io.BufferedWriter;
////import java.io.File;
////import java.io.FileNotFoundException;
////import java.io.FileOutputStream;
////import java.io.IOException;
////import java.io.OutputStreamWriter;
////import java.io.Writer;
////import java.util.Date;
////import java.util.Scanner;
////
////import weka.classifiers.Classifier;
////import weka.core.DenseInstance;
////import weka.core.Instance;
////import weka.core.Instances;
////import weka.core.converters.ConverterUtils;
////
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME2;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TEST_FOLDER;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.SOURCE_PATH;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.TRAIN_PATH;
////import static com.example.bdusun.vehicleclassification.MainActivity.islem;
////import static com.example.bdusun.vehicleclassification.ServiceSensorRead.newDateFormat;
////
////public class CSVtoDegerler /*implements Runnable*/ {
////
////    private double degerler[][];
////    private double degerlerCPY[][];
////    private String label;
////
////    private String data;
////    //private final int argSayisi = 9;
////    private int degerlerBoyut;
////    private int atlamaMiktari;
////    private File folder;
////    private File[] listOfFiles;
////
////    private Thread t;
////
////    private Classifier cls = null;
////    private ConverterUtils.DataSource source = null;
////    private Instances instances = null;
////    private double instanceValues[][] = null;
////    private Instance instance = null;
////
////    private static int argSayisi2 = 12;
////    private static int ozellikSayisi = 29;
////
////    private int instanceIndex=0;
////    //private String trainPath = REQUIRED_FOLDER + "271116cd1ov.model";
////    //    private String trainPath = REQUIRED_FOLDER + "model-17-02-17.m";
////    private String trainPath = TRAIN_PATH;
////    private String resultPath = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME;
////    private String resultPath2 = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME2;
////
////    public CSVtoDegerler(/*String folderName, */int degerlerBoyut, double overlapOrani) {
////        this.folder = new File(SENSOR_DATA_TEST_FOLDER); // bu kisim androidde degisecek
////        this.listOfFiles = this.folder.listFiles();
////        this.degerlerBoyut = degerlerBoyut;
////
////        this.atlamaMiktari = (int) (degerlerBoyut*(1 - overlapOrani));
////        degerler = new double [argSayisi2][degerlerBoyut];
////        //degerlerCPY = new double [12][this.degerlerBoyut];
////
////        try {
////            cls = (Classifier) weka.core.SerializationHelper.read(trainPath);
////            source = new ConverterUtils.DataSource(SOURCE_PATH);
////            instances = source.getDataSet();
////
////            instances.setClassIndex(argSayisi2*ozellikSayisi);
////
////            instance = new DenseInstance(argSayisi2*ozellikSayisi+1);
////            instance.setDataset(instances);
////
////            instanceValues = new double[argSayisi2][ozellikSayisi];
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////    }
////
////
////    public void oku() throws FileNotFoundException, InterruptedException{
//////    	long startTime;
//////    	long endTime;
//////    	long duration;
////        int instanceIndex = 0;
////        int degerSayisi = 0;
////        int r = 0;
////        int i, indis;
////
////        Scanner scanner;
////        String fileName;
////        //RunnableClassify rc;
////        //Thread t;
//////    	System.out.println(this.listOfFiles.length);
////        for (File file : this.listOfFiles) {
////            fileName = file.getName();
////            if (this.isCSV(fileName)) {
////                System.out.println(file.getName() + " okunuyor...");
////                scanner = new Scanner(file);
////                scanner.useDelimiter(",");
////                scanner.nextLine();
////                //like initialize
////                label = fileName.split("_")[1];
////
////                while(scanner.hasNextLine()){
////                    if(degerSayisi < this.degerlerBoyut){
////                        for(i = 3; i < argSayisi2; i++){
////                            this.degerler[i][r] = scanner.nextDouble();
////                        }
////                        for(i = 0; i < 3; i++){
////                            indis = (i + 1) * 3;
////                            this.degerler[r][i] = Math.sqrt(Math.pow(this.degerler[indis][r], 2)
////                                    + Math.pow(this.degerler[indis + 1][r], 2)
////                                    + Math.pow(this.degerler[indis + 2][r], 2));
////                        }
////                        r++;
////                        r %= this.degerlerBoyut;
////                        degerSayisi++;
////                        scanner.nextLine();
////
////                    }else{
////
////                        instanceIndex++;
////
////                        degerSayisi -= atlamaMiktari;
////
////                        //System.arraycopy(degerler, 0, degerlerCPY, 0, degerler.length);
////                        //islem(degerlerCPY, r, argSayisi2, instanceValues);
////                        int result = islem(degerler, r, argSayisi2, instanceValues);
////
////                        int j, sayac;
////
////                        sayac = 0;
////                        for(i=0; i<argSayisi2; i++) {
////                            for(j=0; j<ozellikSayisi; j++){
////                                instance.setValue(sayac, instanceValues[i][j]);
////                                sayac++;
////                            }
////                        }
////
////                        //instance.setClassMissing();
////                        instance.setValue(sayac, label);
////
////
////                        //double instanceValues[][] = new double[argSayisi2][ozellikSayisi];
////
////                        String timeStamp = newDateFormat.format(new Date());
////                        String location = null;
////
////                        int thresholdResult = islem(degerler, r, argSayisi2, instanceValues);
////
////                        //double prediction;
////
////                        String classificationResult = null;
////
////                        String thresholdClassificationResult = "VEHICULAR";
////
////                        int thresholdIndis = -1;
////
////                        switch (thresholdResult) {
////
////                            case -1:
////                                //classify
////                                thresholdIndis = -1;
////                                break;
////                            case 10:
////                                //stationary
////                                classificationResult = "STATIONARY";
////                                thresholdClassificationResult = classificationResult;
////                                thresholdIndis = 10;
////                                break;
////                            case 0:
////                                //walking
////                                classificationResult = "WALKING";
////                                thresholdClassificationResult = classificationResult;
////                                thresholdIndis = 0;
////                                break;
////                            default:
////                                //error
////                                classificationResult = "error wrong class: " + result;
////                                thresholdClassificationResult = classificationResult;
////                                break;
////                        }
////
////                        //bu case -1'de olacak, debug amacli burda
////                        Instance instance = new DenseInstance(argSayisi2*ozellikSayisi+1);
////                        instance.setDataset(instances);
////                        sayac = 0;
////                        for(i=0; i<argSayisi2; i++) {
////                            for(j=0; j<ozellikSayisi; j++){
////                                instance.setValue(sayac, instanceValues[i][j]);
////                                sayac++;
////                            }
////                        }
////                        instance.setValue(sayac, "bus");
////                        try {
////                            result = (int) cls.classifyInstance(instance);
////                            classificationResult = instance.classAttribute().value(result);
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        }
////
////                        Writer writer = null;
////
////                        //TODO debugClassificationResult classificationResult ile yer degistirecek
////                        try {
////                            //System.out.println(journeyFilePath);
////                            writer = new BufferedWriter(new OutputStreamWriter(
////                                    new FileOutputStream(resultPath, true), "utf-8"));
////
////                            writer.write(
////                                    timeStamp
////                                            + " "
////                                            + thresholdClassificationResult
////                                            + " "
////                                            + thresholdIndis
////                                            + " "
////                                            + classificationResult
////                                            + " "
////                                            + result
////                                            + " "
////                                            + location
////                                            + "\n");
////
////                            Log.d("VEHCLA", timeStamp + "," + thresholdClassificationResult + ","
////                                    + classificationResult + ":" + result + "," + location);
////
////                        } catch (IOException ex) {
////                            ex.printStackTrace();
////                        } finally {
////                            try {
////                                writer.close();
////                            } catch (Exception ex) {/*ignore*/}
////                        }
////
////
//////                        Writer writer2 = null;
//////                        try {
//////                            writer2 = new BufferedWriter(new OutputStreamWriter(
//////                                    new FileOutputStream(resultPath2,true), "utf-8"));
//////
//////                            double real = instance.classValue(); //Bunlar değişecek
//////                            double pred = 0; //double pred = rf.classifyInstance(test.instance(i));
//////                            try {
//////                                pred = cls.classifyInstance(instance);
//////                            } catch (Exception e) {
//////                                e.printStackTrace();
//////                            }
//////                            String error = (((int) pred)==((int) real) ? "" : "+" );
//////                            writer2.write((instanceIndex) +","
//////                                    + (int) real + ":"
//////                                    + instance.classAttribute().value((int) real) + ","
//////                                    + (int) pred + ":"
//////                                    + instance.classAttribute().value((int) pred) + ","
//////                                    + error + "," + "\n");
//////                            Log.d("VEHCLA", (instanceIndex) +","
//////                                    + (int) real + ":"
//////                                    + instance.classAttribute().value((int) real) + ","
//////                                    + (int) pred + ":"
//////                                    + instance.classAttribute().value((int) pred) + ","
//////                                    + error + ",");
//////
//////                        } catch (IOException ex) {
//////                            ex.printStackTrace();
//////                        } finally {
//////                            try {
//////                                writer2.close();
//////                            } catch (Exception ex) {/*ignore*/}
//////                        }
////
////
////
//////                        System.arraycopy(degerler, 0, degerlerCPY, 0, degerler.length);
//////                        t = new Thread(new RunnableClassify(instanceIndex, r, degerlerCPY, label), "t"+instanceIndex);
//////                        t.start();
////
////                        //burda degerler matrisini clone layip gonderebiliriz thread olarak
//////        		startTime = System.nanoTime();
////                        //degerlerCPY = degerler.clone();
////                        //System.out.print("CALISIYOR ");
//////        		endTime = System.nanoTime();
////
//////						TaskClassify classify = new TaskClassify(r, degerlerCPY, label);
//////						classify.execute();
////
////                        //t = new Thread (this, ""+count);
////
//////        		duration = (endTime - startTime);
//////        		System.out.println(duration);
////
////                    }
////                }
////
////                scanner.close();
////
////            }
////        }
////
////    }
////
////
//////    private void classify(){}
////
////
////    private boolean isCSV(String fileName){
////        return fileName.endsWith(".csv");
////    }
////
//////    public void run(){
//////        //burada degerler matrisi hazir demek
//////        //simdi bir sekilde yollamak lazim
//////        //burada siniflandirmanin bir metodu cagirilacak.
////////    	System.out.println("Running ");
//////    }
////
////}
