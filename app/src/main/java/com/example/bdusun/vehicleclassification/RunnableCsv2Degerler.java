//package com.example.bdusun.vehicleclassification;
//
//import android.content.Context;
//import android.util.Log;
//import android.widget.Toast;
//
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.io.Writer;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//import java.util.Scanner;
//import java.util.concurrent.Callable;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.Future;
//
//import weka.classifiers.Classifier;
//import weka.core.DenseInstance;
//import weka.core.Instance;
//import weka.core.Instances;
//import weka.core.converters.ConverterUtils;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME2;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TEST_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.SOURCE_PATH;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.TRAIN_PATH;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.precision;
//import static com.example.bdusun.vehicleclassification.MainActivity.islem;
//
//public class RunnableCsv2Degerler implements Runnable {
//
//    //private double degerler[][];
//    //private double degerlerCPY[][];
//    private String label;
//
//    public static SimpleDateFormat newDateFormatFileName = new SimpleDateFormat("yyyyMMdd");
//    public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm");
//    //public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm:ss");
//
//    //private String data;
//    //private final int argSayisi = 9;
//    private int degerlerBoyut = 6000;
//    private int atlamaMiktari = 0;
//    private File folder;
//    //private File[] listOfFiles;
//    private List<File> listOfFiles;
//
//    private Thread t;
//
//    private Classifier cls = null;
//    private ConverterUtils.DataSource source = null;
//    //private double instanceValues[][] = null;
//    //private Instance instance = null;
//
//    private static int argSayisi2 = 12;
//    private static int ozellikSayisi = 29;
//
//    //private int instanceIndex=0;
//    //private String trainPath = REQUIRED_FOLDER + "271116cd1ov.model";
//    //    private String trainPath = REQUIRED_FOLDER + "model-17-02-17.m";
//    private String trainPath = TRAIN_PATH;
//    private String resultPath = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME;
//    private String resultPath2 = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME2;
//
//    private String string1 = "";
//    private String string2 = "";
//
//    private Context context;
//    private ExecutorService esFile;
//    //private ExecutorService esWindow;
//    private Instances instances;
//    private Instances finalInstances;
//
//    public RunnableCsv2Degerler(Context c, int degerlerBoyut, double overlapOrani) {
//        this.context = c;
//        this.folder = new File(SENSOR_DATA_TEST_FOLDER);
//        this.listOfFiles = new ArrayList<>(Arrays.asList(this.folder.listFiles()));
//        Collections.sort(this.listOfFiles);
//        this.degerlerBoyut = degerlerBoyut;
//        this.atlamaMiktari = (int) (degerlerBoyut*(1 - overlapOrani));
//
//        try {
//            cls = (Classifier) weka.core.SerializationHelper.read(TRAIN_PATH);
//            source = new ConverterUtils.DataSource(SOURCE_PATH);
//            this.instances = source.getDataSet();
//            this.instances.setClassIndex(argSayisi2*ozellikSayisi);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public String[] read(File file) {
//
//        long beginTime = System.currentTimeMillis();
//        int i = 0;
//        int lineCount = 0;
//        int degerSayisi = 0;
//        int r = 0;
//        int indis = 0;
//        String filename = file.getName();
//        String label = filename.split("_")[1];
//        Scanner scanner = null;
//        String resultString[] = {"",""};
//        int instanceIndex = 0;
//
//        double[][] degerler = new double[argSayisi2][degerlerBoyut];
//
//        Instance instance = new DenseInstance(argSayisi2*ozellikSayisi+1);
//        instance.setDataset(instances);
//
//        try {
//
//            scanner = new Scanner(file);
//            scanner.useDelimiter(",");
//            scanner.nextLine();
//
//            System.out.println(filename + " is being read...");
//
//            while (scanner.hasNextLine()) {
//
//                if (degerSayisi < this.degerlerBoyut) {
//                    for (i = 3; i < argSayisi2; i++) {
//                        degerler[i][r] = scanner.nextDouble();
//                    }
//                    for (i = 0; i < 3; i++) {
//                        indis = (i + 1) * 3;
//                        degerler[i][r] = Math.sqrt(Math.pow(degerler[indis][r], 2)
//                                + Math.pow(degerler[indis + 1][r], 2)
//                                + Math.pow(degerler[indis + 2][r], 2));
//                    }
//                    r++;
//                    r %= this.degerlerBoyut;
//                    degerSayisi++;
//                    scanner.nextLine();
//                }
//
//                else {
//
//                    instanceIndex++;
//
//                    degerSayisi -= atlamaMiktari;
//                    //System.arraycopy(degerler, 0, degerlerCPY, 0, degerler.length);
//                    //degerlerCPY = degerler.clone()
//                    double instanceValues[][] = new double[argSayisi2][ozellikSayisi];
//                    int thresholdResult = islem(degerler, r, argSayisi2, instanceValues);
//                    int j, sayac;
//                    String timeStamp = newDateFormat.format(new Date());
//                    String location = null;
//                    String classificationResult = null;
//                    String thresholdClassificationResult = "VEHICULAR";
//                    int thresholdIndis = -1;
//
//                    switch (thresholdResult) {
//
//                        case -1:
//                            //classify
//                            thresholdIndis = -1;
//                            break;
//                        case 10:
//                            //stationary
//                            classificationResult = "STATIONARY";
//                            thresholdClassificationResult = classificationResult;
//                            thresholdIndis = 10;
//                            break;
//                        case 0:
//                            //walking
//                            classificationResult = "WALKING";
//                            thresholdClassificationResult = classificationResult;
//                            thresholdIndis = 0;
//                            break;
//                        default:
//                            //error
//                            classificationResult = "error wrong class: " + thresholdResult;
//                            thresholdClassificationResult = classificationResult;
//                            break;
//                    }
//
//                    sayac = 0;
//                    for (i = 0; i < argSayisi2; i++) {
//                        for (j = 0; j < ozellikSayisi; j++) {
//                            instance.setValue(sayac, instanceValues[i][j]);
//                            sayac++;
//                        }
//                    }
//                    instance.setValue(sayac, label);
//
//                    int result = -50;
//                    try {
//                        result = (int) cls.classifyInstance(instance);
//                        classificationResult = instance.classAttribute().value(result);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    double real = instance.classValue();
//                    String error = ((result) == ((int) real) ? " " : "-");
//                    double zcth = ((instanceValues[0][27] * 100) / (instanceValues[0][26] + instanceValues[0][27] + instanceValues[0][28]));
//
//                    resultString[1] += (instanceIndex) + ","
//                            + (int) real + ":"
//                            + instance.classAttribute().value((int) real) + ","
//                            + result + ":"
//                            + classificationResult + ","
//                            + ",std:" + String.format(Locale.ENGLISH, precision, instanceValues[0][12])
//                            + ",zcth:" + String.format(Locale.ENGLISH, precision, zcth)
//                            + ","
//                            + error
//                            + ","
//                            + "\n";
//
//                    resultString[0] += timeStamp
//                            + " "
//                            + thresholdClassificationResult
//                            + " "
//                            + thresholdIndis
//                            + " "
//                            + classificationResult
//                            + " "
//                            + result
//                            + " "
//                            + location
//                            + " "
//                            + location
//                            + " "
//                            + "\n";
//                }
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        finally {
//            scanner.close();
//        }
//
//        long duration = System.currentTimeMillis()-beginTime;
//        System.out.println(filename + " processed in " + duration/1000.0 + "sec");
//        //System.out.println(resultString);
//        return resultString;
//    }
//
//    @Override
//    public void run () {
//
//        long startTime = System.currentTimeMillis();
//        Writer writer = null;
//        String string2 = "";
//        String string1 = "";
//        esFile = Executors.newFixedThreadPool(listOfFiles.size()); // gerekenden buyuk pool
//        List<Callable<String[]>> todo = new ArrayList<Callable<String[]>>(listOfFiles.size());
//
//        for (final File file : this.listOfFiles) {
//            final String fileName = file.getName();
//            if (this.isCSV(fileName)) {
//                //System.out.println(file.getName() + " okunuyor...");
//                todo.add(new Callable<String[]>() {
//                    public String[] call() throws Exception {
//                        //String s = read(file);
//                        //System.out.println(s);
//                        return read(file);
//                        //return read(fileName);
//                    }
//                });
//            }
//        }
//        List<Future<String[]>> futures = null;
//        try {
//            futures = esFile.invokeAll(todo);
//
//            for (Future future : futures) {
//                try {
//                    String string[] = (String[]) future.get();
//                    string1 += string[0];
//                    string2 +=  string[1];
//                } catch (InterruptedException | ExecutionException e) {
//                    e.printStackTrace();
//                    System.out.println("Interrupted?");
//                }
//            }
//            //System.out.println(string2);
//            long endTime = System.currentTimeMillis();
//            long duration = endTime - startTime;
//            System.out.println("Csv2Degerler completed in " + duration / (60000.0) + "min");
//
//            File file = new File(resultPath);
//
//            file.delete();
//            file.createNewFile();
////            writer = new BufferedWriter(new OutputStreamWriter(
////                    new FileOutputStream(resultPath, true), "utf-8"));
//            writer = new BufferedWriter(new FileWriter(file));
//            writer.write(string1);
//            writer.flush();
//            writer.close();
//
//            file = new File(resultPath2);
//
//            int totalCount = 0;
//            int errorCount = 0;
//            float successRate;
//
//            file.delete();
//            file.createNewFile();
////            writer = new BufferedWriter(new OutputStreamWriter(
////                    new FileOutputStream(resultPath2, true), "utf-8"));
//            writer = new BufferedWriter(new FileWriter(file));
//            writer.write(string2);
//            writer.flush();
//            writer.close();
//
//            System.out.println("Calculating success..");
//
//            BufferedReader reader = new BufferedReader(new FileReader(file));
//            String line;
//            while ((line = reader.readLine()) != null) {
//                if (line.contains("-")) {
//                    errorCount++;
//                }
//                totalCount++;
//            }
//            reader.close();
//
//            successRate = ((float) (totalCount - errorCount) / totalCount) * 100;
//
//            System.out.println("Instances: "+ totalCount +" Success: %" + successRate + " Time: " + duration/60000.0 + "min");
//
//            writer = new BufferedWriter(new OutputStreamWriter(
//                    new FileOutputStream(resultPath2, true), "utf-8"));
//            duration = System.currentTimeMillis() - startTime;
//            writer.write("Instances: " + totalCount + "Success rate: %" + successRate + " completed in " + duration / 60000.0 + "min");
//            writer.flush();
//            writer.close();
//
//            Toast.makeText(context, "Csv2Degerler completed in " + duration / 60000.0 + "min", Toast.LENGTH_LONG).show();
//
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private boolean isCSV(String fileName){return fileName.endsWith(".csv");
//    }
//    public void writeResultWithDebugValues(int instanceIndex, String realLabel, double real, int result, String classificationResult, double std, double zcth){
//        Writer writer = null;
//        try {
//            //System.out.println(journeyFilePath);
//            writer = new BufferedWriter(new OutputStreamWriter(
//                    new FileOutputStream(resultPath2, true), "utf-8"));
//
//            //double pred = cls.classifyInstance(instance); //double pred = rf.classifyInstance(test.instance(i));
//            String error = ((result) == ((int) real) ? " " : "-");
//
//            writer.write((instanceIndex) + ","
//                    + (int) real + ":"
//                    + realLabel + ","
//                    + result + ":"
//                    + classificationResult + ","
//                    + ",std:" + String.format(Locale.ENGLISH, precision, std)
//                    + ",zcth:" + String.format(Locale.ENGLISH, precision, zcth)
//                    + ","
//                    + error
//                    + ","
//                    + "\n");
//
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        } finally {
//            try {
//                writer.flush();
//                writer.close();
//            } catch (Exception ex) {/*ignore*/}
//        }
//    }
//
//    public void writeResult(int instanceIndex, String realLabel, double real, int result, String classificationResult){
//        Writer writer = null;
//        try {
//            //System.out.println(journeyFilePath);
//            writer = new BufferedWriter(new OutputStreamWriter(
//                    new FileOutputStream(resultPath2, true), "utf-8"));
//
//            //double pred = cls.classifyInstance(instance); //double pred = rf.classifyInstance(test.instance(i));
//            String error = ((result) == ((int) real) ? " " : "-");
//
//            writer.write((instanceIndex) + ","
//                    + (int) real + ":"
//                    + realLabel + ","
//                    + result + ":"
//                    + classificationResult + ","
//                    + ","
//                    + error
//                    + ","
//                    + "\n");
//
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        } finally {
//            try {
//                writer.flush();
//                writer.close();
//            } catch (Exception ex) {/*ignore*/}
//        }
//    }
//
//    public void writeHealableResult(String timeStamp, String thresholdClassificationResult,
//                                    int thresholdIndis, String classificationResult, int result){
//        //long startTime
//        Writer writer = null;
//
//        try {
//            //System.out.println(journeyFilePath);
//            writer = new BufferedWriter(new OutputStreamWriter(
//                    new FileOutputStream(resultPath, true), "utf-8"));
//            //writer = new BufferedWriter(new FileWriter(filename), 32768);
//
//            writer.write(
//                    timeStamp
//                            + " "
//                            + thresholdClassificationResult
//                            + " "
//                            + thresholdIndis
//                            + " "
//                            + classificationResult
//                            + " "
//                            + result
//                            + " "
//                            + "\n");
//
//            ;
//            Log.d("VEHCLA", timeStamp + "," + thresholdClassificationResult + ","
//                    + classificationResult + ":" + result + "," + "ms");
//
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        } finally {
//            try {
//                writer.flush();
//                writer.close();
//            } catch (Exception ex) {/*ignore*/}
//        }
//    }
//
//    //    public String[] read(File file) {
////package com.example.bdusun.vehicleclassification;
////
////import android.content.Context;
////import android.util.Log;
////import android.widget.Toast;
////
////import java.io.BufferedReader;
////import java.io.BufferedWriter;
////import java.io.File;
////import java.io.FileOutputStream;
////import java.io.FileReader;
////import java.io.FileWriter;
////import java.io.IOException;
////import java.io.OutputStreamWriter;
////import java.io.Writer;
////import java.util.ArrayList;
////import java.util.Arrays;
////import java.util.Collections;
////import java.util.Date;
////import java.util.List;
////import java.util.Locale;
////import java.util.Scanner;
////import java.util.concurrent.Callable;
////import java.util.concurrent.ExecutionException;
////import java.util.concurrent.ExecutorService;
////import java.util.concurrent.Executors;
////import java.util.concurrent.Future;
////
////import weka.classifiers.Classifier;
////import weka.core.DenseInstance;
////import weka.core.Instance;
////import weka.core.Instances;
////import weka.core.converters.ConverterUtils;
////
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME2;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TEST_FOLDER;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.SOURCE_PATH;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.TRAIN_PATH;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.precision;
////import static com.example.bdusun.vehicleclassification.MainActivity.islem;
////import static com.example.bdusun.vehicleclassification.ServiceSensorRead.newDateFormat;
////
////
////public class RunnableCsv2Degerler implements Runnable {
////
////    //private double degerler[][];
////    //private double degerlerCPY[][];
////    private String label;
////
////    //private String data;
////    //private final int argSayisi = 9;
////    private int degerlerBoyut = 6000;
////    private int atlamaMiktari = 0;
////    private File folder;
////    //private File[] listOfFiles;
////    private List<File> listOfFiles;
////
////    private Thread t;
////
////    private Classifier cls = null;
////    private ConverterUtils.DataSource source = null;
////    //private double instanceValues[][] = null;
////    //private Instance instance = null;
////
////    private static int argSayisi2 = 12;
////    private static int ozellikSayisi = 29;
////
////    //private int instanceIndex=0;
////    //private String trainPath = REQUIRED_FOLDER + "271116cd1ov.model";
////    //    private String trainPath = REQUIRED_FOLDER + "model-17-02-17.m";
////    private String trainPath = TRAIN_PATH;
////    private String resultPath = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME;
////    private String resultPath2 = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME2;
////
////    private String string1 = "";
////    private String string2 = "";
////
////    private Context context;
////    private ExecutorService esFile;
////    //private ExecutorService esWindow;
////    private Instances instances;
////    private Instances finalInstances;
////
////    public RunnableCsv2Degerler(Context c, int degerlerBoyut, double overlapOrani) {
////        this.context = c;
////        this.folder = new File(SENSOR_DATA_TEST_FOLDER);
////        this.listOfFiles = new ArrayList<>(Arrays.asList(this.folder.listFiles()));
////        Collections.sort(this.listOfFiles);
////        this.degerlerBoyut = degerlerBoyut;
////        this.atlamaMiktari = (int) (degerlerBoyut*(1 - overlapOrani));
////
////        try {
////            cls = (Classifier) weka.core.SerializationHelper.read(TRAIN_PATH);
////            source = new ConverterUtils.DataSource(SOURCE_PATH);
////            this.instances = source.getDataSet();
////            this.instances.setClassIndex(argSayisi2*ozellikSayisi);
////
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
////
////    public String[] read(File file) {
////
////        long beginTime = System.currentTimeMillis();
////        int i = 0;
////        int lineCount = 0;
////        int degerSayisi = 0;
////        int r = 0;
////        int indis = 0;
////        String filename = file.getName();
////        String label = filename.split("_")[1];
////        Scanner scanner = null;
////        String resultString[] = {"",""};
////        int instanceIndex = 0;
////
////        double[][] degerler = new double[argSayisi2][degerlerBoyut];
////
////        Instance instance = new DenseInstance(argSayisi2*ozellikSayisi+1);
////        instance.setDataset(instances);
////
////        try {
////
////            scanner = new Scanner(file);
////            scanner.useDelimiter(",");
////            scanner.nextLine();
////
////            System.out.println(filename + " is being read...");
////
////            while (scanner.hasNextLine()) {
////
////                if (degerSayisi < this.degerlerBoyut) {
////                    for (i = 3; i < argSayisi2; i++) {
////                        degerler[i][r] = scanner.nextDouble();
////                    }
////                    for (i = 0; i < 3; i++) {
////                        indis = (i + 1) * 3;
////                        degerler[i][r] = Math.sqrt(Math.pow(degerler[indis][r], 2)
////                                + Math.pow(degerler[indis + 1][r], 2)
////                                + Math.pow(degerler[indis + 2][r], 2));
////                    }
////                    r++;
////                    r %= this.degerlerBoyut;
////                    degerSayisi++;
////                    scanner.nextLine();
////                }
////
////                else {
////
////                    instanceIndex++;
////
////                    degerSayisi -= atlamaMiktari;
////                    //System.arraycopy(degerler, 0, degerlerCPY, 0, degerler.length);
////                    //degerlerCPY = degerler.clone()
////                    double instanceValues[][] = new double[argSayisi2][ozellikSayisi];
////                    int thresholdResult = islem(degerler, r, argSayisi2, instanceValues);
////                    int j, sayac;
////                    String timeStamp = newDateFormat.format(new Date());
////                    String location = null;
////                    String classificationResult = null;
////                    String thresholdClassificationResult = "VEHICULAR";
////                    int thresholdIndis = -1;
////
////                    switch (thresholdResult) {
////
////                        case -1:
////                            //classify
////                            thresholdIndis = -1;
////                            break;
////                        case 10:
////                            //stationary
////                            classificationResult = "STATIONARY";
////                            thresholdClassificationResult = classificationResult;
////                            thresholdIndis = 10;
////                            break;
////                        case 0:
////                            //walking
////                            classificationResult = "WALKING";
////                            thresholdClassificationResult = classificationResult;
////                            thresholdIndis = 0;
////                            break;
////                        default:
////                            //error
////                            classificationResult = "error wrong class: " + thresholdResult;
////                            thresholdClassificationResult = classificationResult;
////                            break;
////                    }
////
////                    sayac = 0;
////                    for (i = 0; i < argSayisi2; i++) {
////                        for (j = 0; j < ozellikSayisi; j++) {
////                            instance.setValue(sayac, instanceValues[i][j]);
////                            sayac++;
////                        }
////                    }
////                    instance.setValue(sayac, label);
////
////                    int result = -50;
////                    try {
////                        result = (int) cls.classifyInstance(instance);
////                        classificationResult = instance.classAttribute().value(result);
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////
////                    double real = instance.classValue();
////                    String error = ((result) == ((int) real) ? " " : "-");
////                    double zcth = ((instanceValues[0][27] * 100) / (instanceValues[0][26] + instanceValues[0][27] + instanceValues[0][28]));
////
////                    resultString[1] += (instanceIndex) + ","
////                            + (int) real + ":"
////                            + instance.classAttribute().value((int) real) + ","
////                            + result + ":"
////                            + classificationResult + ","
////                            + ",std:" + String.format(Locale.ENGLISH, precision, instanceValues[0][12])
////                            + ",zcth:" + String.format(Locale.ENGLISH, precision, zcth)
////                            + ","
////                            + error
////                            + ","
////                            + "\n";
////
////                    resultString[0] += timeStamp
////                            + " "
////                            + thresholdClassificationResult
////                            + " "
////                            + thresholdIndis
////                            + " "
////                            + classificationResult
////                            + " "
////                            + result
////                            + " "
////                            + location
////                            + " "
////                            + location
////                            + " "
////                            + "\n";
////                }
////            }
////
////        }catch (Exception e){
////            e.printStackTrace();
////        }
////        finally {
////            scanner.close();
////        }
////
////        long duration = System.currentTimeMillis()-beginTime;
////        System.out.println(filename + " processed in " + duration/1000.0 + "sec");
////        //System.out.println(resultString);
////        return resultString;
////    }
////
////    @Override
////    public void run () {
////
////        long startTime = System.currentTimeMillis();
////        Writer writer = null;
////        String string2 = "";
////        String string1 = "";
////        esFile = Executors.newFixedThreadPool(listOfFiles.size()); // gerekenden buyuk pool
////        List<Callable<String[]>> todo = new ArrayList<Callable<String[]>>(listOfFiles.size());
////
////        for (final File file : this.listOfFiles) {
////            final String fileName = file.getName();
////            if (this.isCSV(fileName)) {
////                //System.out.println(file.getName() + " okunuyor...");
////                todo.add(new Callable<String[]>() {
////                    public String[] call() throws Exception {
////                        //String s = read(file);
////                        //System.out.println(s);
////                        return read(file);
////                        //return read(fileName);
////                    }
////                });
////            }
////        }
////        List<Future<String[]>> futures = null;
////        try {
////            futures = esFile.invokeAll(todo);
////
////            for (Future future : futures) {
////                try {
////                    String string[] = (String[]) future.get();
////                    string1 += string[0];
////                    string2 +=  string[1];
////                } catch (InterruptedException | ExecutionException e) {
////                    e.printStackTrace();
////                    System.out.println("Interrupted?");
////                }
////            }
////            //System.out.println(string2);
////            long endTime = System.currentTimeMillis();
////            long duration = endTime - startTime;
////            System.out.println("Csv2Degerler completed in " + duration / (60000.0) + "min");
////
////            File file = new File(resultPath);
////
////            file.delete();
////            file.createNewFile();
//////            writer = new BufferedWriter(new OutputStreamWriter(
//////                    new FileOutputStream(resultPath, true), "utf-8"));
////            writer = new BufferedWriter(new FileWriter(file));
////            writer.write(string1);
////            writer.flush();
////            writer.close();
////
////            file = new File(resultPath2);
////
////            int totalCount = 0;
////            int errorCount = 0;
////            float successRate;
////
////            file.delete();
////            file.createNewFile();
//////            writer = new BufferedWriter(new OutputStreamWriter(
//////                    new FileOutputStream(resultPath2, true), "utf-8"));
////            writer = new BufferedWriter(new FileWriter(file));
////            writer.write(string2);
////            writer.flush();
////            writer.close();
////
////            System.out.println("Calculating success..");
////
////            BufferedReader reader = new BufferedReader(new FileReader(file));
////            String line;
////            while ((line = reader.readLine()) != null) {
////                if (line.contains("-")) {
////                    errorCount++;
////                }
////                totalCount++;
////            }
////            reader.close();
////
////            successRate = ((float) (totalCount - errorCount) / totalCount) * 100;
////
////            System.out.println("Instances: "+ totalCount +" Success: %" + successRate + " Time: " + duration/60000.0 + "min");
////
////            writer = new BufferedWriter(new OutputStreamWriter(
////                    new FileOutputStream(resultPath2, true), "utf-8"));
////            duration = System.currentTimeMillis() - startTime;
////            writer.write("Instances: " + totalCount + "Success rate: %" + successRate + " completed in " + duration / 60000.0 + "min");
////            writer.flush();
////            writer.close();
////
////            Toast.makeText(context, "Csv2Degerler completed in " + duration / 60000.0 + "min", Toast.LENGTH_LONG).show();
////
////        } catch (InterruptedException e) {
////            e.printStackTrace();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////    }
////
////    private boolean isCSV(String fileName){return fileName.endsWith(".csv");
////    }
////    public void writeResultWithDebugValues(int instanceIndex, String realLabel, double real, int result, String classificationResult, double std, double zcth){
////        Writer writer = null;
////        try {
////            //System.out.println(journeyFilePath);
////            writer = new BufferedWriter(new OutputStreamWriter(
////                    new FileOutputStream(resultPath2, true), "utf-8"));
////
////            //double pred = cls.classifyInstance(instance); //double pred = rf.classifyInstance(test.instance(i));
////            String error = ((result) == ((int) real) ? " " : "-");
////
////            writer.write((instanceIndex) + ","
////                    + (int) real + ":"
////                    + realLabel + ","
////                    + result + ":"
////                    + classificationResult + ","
////                    + ",std:" + String.format(Locale.ENGLISH, precision, std)
////                    + ",zcth:" + String.format(Locale.ENGLISH, precision, zcth)
////                    + ","
////                    + error
////                    + ","
////                    + "\n");
////
////        } catch (IOException ex) {
////            ex.printStackTrace();
////        } finally {
////            try {
////                writer.flush();
////                writer.close();
////            } catch (Exception ex) {/*ignore*/}
////        }
////    }
////
////    public void writeResult(int instanceIndex, String realLabel, double real, int result, String classificationResult){
////        Writer writer = null;
////        try {
////            //System.out.println(journeyFilePath);
////            writer = new BufferedWriter(new OutputStreamWriter(
////                    new FileOutputStream(resultPath2, true), "utf-8"));
////
////            //double pred = cls.classifyInstance(instance); //double pred = rf.classifyInstance(test.instance(i));
////            String error = ((result) == ((int) real) ? " " : "-");
////
////            writer.write((instanceIndex) + ","
////                    + (int) real + ":"
////                    + realLabel + ","
////                    + result + ":"
////                    + classificationResult + ","
////                    + ","
////                    + error
////                    + ","
////                    + "\n");
////
////        } catch (IOException ex) {
////            ex.printStackTrace();
////        } finally {
////            try {
////                writer.flush();
////                writer.close();
////            } catch (Exception ex) {/*ignore*/}
////        }
////    }
////
////    public void writeHealableResult(String timeStamp, String thresholdClassificationResult,
////                                    int thresholdIndis, String classificationResult, int result){
////        //long startTime
////        Writer writer = null;
////
////        try {
////            //System.out.println(journeyFilePath);
////            writer = new BufferedWriter(new OutputStreamWriter(
////                    new FileOutputStream(resultPath, true), "utf-8"));
////            //writer = new BufferedWriter(new FileWriter(filename), 32768);
////
////            writer.write(
////                    timeStamp
////                            + " "
////                            + thresholdClassificationResult
////                            + " "
////                            + thresholdIndis
////                            + " "
////                            + classificationResult
////                            + " "
////                            + result
////                            + " "
////                            + "\n");
////
////            ;
////            Log.d("VEHCLA", timeStamp + "," + thresholdClassificationResult + ","
////                    + classificationResult + ":" + result + "," + "ms");
////
////        } catch (IOException ex) {
////            ex.printStackTrace();
////        } finally {
////            try {
////                writer.flush();
////                writer.close();
////            } catch (Exception ex) {/*ignore*/}
////        }
////    }
////
////    //    public String[] read(File file) {
//////
//////        long beginTime = System.currentTimeMillis();
//////        int i = 0;
//////        int lineCount = 0;
//////        int degerSayisi = 0;
//////        int r = 0;
//////        int indis = 0;
//////        String filename = file.getName();
//////        String label = filename.split("_")[1];
//////        String resultString[] = {"",""};
//////        //resultString[0] = "";
//////        //resultString[1] = "";
//////        int instanceIndex = 0;
//////
//////        double[][] degerler = new double[argSayisi2][degerlerBoyut];
//////
//////        Instance instance = new DenseInstance(argSayisi2*ozellikSayisi+1);
//////        instance.setDataset(instances);
//////
//////        BufferedReader reader = null;
//////
//////        try {
//////
//////            reader = new BufferedReader(new FileReader(file));
//////            System.out.println(filename + " is being read...");
//////            String line;
//////            reader.readLine();
//////            line = reader.readLine();
//////
//////            while (line != null) {
//////
//////                StringTokenizer st = new StringTokenizer(line,",",false);
//////
//////                if (degerSayisi < this.degerlerBoyut) {
//////                    for (i = 3; i < argSayisi2; i++) {
//////                        degerler[i][r] = Double.parseDouble(st.nextToken());
//////                    }
//////                    for (i = 0; i < 3; i++) {
//////                        indis = (i + 1) * 3;
//////                        degerler[i][r] = Math.sqrt(Math.pow(degerler[indis][r], 2)
//////                                + Math.pow(degerler[indis + 1][r], 2)
//////                                + Math.pow(degerler[indis + 2][r], 2));
//////                    }
//////                    r++;
//////                    r %= this.degerlerBoyut;
//////                    degerSayisi++;
//////                    reader.readLine();
//////
//////                } else {
//////
//////                    instanceIndex++;
//////
//////                    degerSayisi -= atlamaMiktari;
//////                    //System.arraycopy(degerler, 0, degerlerCPY, 0, degerler.length);
//////                    //degerlerCPY = degerler.clone()
//////                    double instanceValues[][] = new double[argSayisi2][ozellikSayisi];
//////                    int thresholdResult = islem(degerler, r, argSayisi2, instanceValues);
//////                    int j, sayac;
//////                    String timeStamp = newDateFormat.format(new Date());
//////                    String location = null;
//////                    String classificationResult = null;
//////                    String thresholdClassificationResult = "VEHICULAR";
//////                    int thresholdIndis = -1;
//////
//////                    switch (thresholdResult) {
//////
//////                        case -1:
//////                            //classify
//////                            thresholdIndis = -1;
//////                            break;
//////                        case 10:
//////                            //stationary
//////                            classificationResult = "STATIONARY";
//////                            thresholdClassificationResult = classificationResult;
//////                            thresholdIndis = 10;
//////                            break;
//////                        case 0:
//////                            //walking
//////                            classificationResult = "WALKING";
//////                            thresholdClassificationResult = classificationResult;
//////                            thresholdIndis = 0;
//////                            break;
//////                        default:
//////                            //error
//////                            classificationResult = "error wrong class: " + thresholdResult;
//////                            thresholdClassificationResult = classificationResult;
//////                            break;
//////                    }
//////
//////                    sayac = 0;
//////                    for (i = 0; i < argSayisi2; i++) {
//////                        for (j = 0; j < ozellikSayisi; j++) {
//////                            instance.setValue(sayac, instanceValues[i][j]);
//////                            sayac++;
//////                        }
//////                    }
//////                    instance.setValue(sayac, label);
//////
//////                    int result = -50;
//////                    try {
//////                        result = (int) cls.classifyInstance(instance);
//////                        classificationResult = instance.classAttribute().value(result);
//////                    } catch (Exception e) {
//////                        e.printStackTrace();
//////                    }
//////
//////                    double real = instance.classValue();
//////                    String error = ((result) == ((int) real) ? " " : "-");
//////                    double zcth = ((instanceValues[0][27] * 100) / (instanceValues[0][26] + instanceValues[0][27] + instanceValues[0][28]));
//////
//////                    resultString[1] += (instanceIndex) + "," // TODO Stringbuilder
//////                            + (int) real + ":"
//////                            + instance.classAttribute().value((int) real) + ","
//////                            + result + ":"
//////                            + classificationResult + ","
//////                            + ",std:" + String.format(Locale.ENGLISH, precision, instanceValues[0][12])
//////                            + ",zcth:" + String.format(Locale.ENGLISH, precision, zcth)
//////                            + ","
//////                            + error
//////                            + ","
//////                            + "\n";
//////
//////                    resultString[0] += timeStamp
//////                            + " "
//////                            + thresholdClassificationResult
//////                            + " "
//////                            + thresholdIndis
//////                            + " "
//////                            + classificationResult
//////                            + " "
//////                            + result
//////                            + " "
//////                            + location
//////                            + " "
//////                            + "\n";
//////
//////                }
//////
//////
//////            }
//////
//////            reader.close();
//////        }catch (Exception e){
//////            e.printStackTrace();
//////        }
//////
//////        long duration = System.currentTimeMillis()-beginTime;
//////        System.out.println(filename + " processed in " + duration/1000.0 + "sec");
//////        //System.out.println(resultString);
//////        return resultString;
//////    }
////
////    //    @Override
//////    public void run() {
//////        try {
//////            long startTime = System.currentTimeMillis();
//////            this.oku();
//////            long endTime = System.currentTimeMillis();
//////            long duration = endTime-startTime;
//////            System.out.println("Csv2Degerler completed in " + duration/1000.0 + " sec");
//////            Toast.makeText(context, "Csv2Degerler completed in " + duration/1000.0 + " sec", Toast.LENGTH_SHORT);
//////
//////            System.out.println("Calculating success..");
//////
//////            File file = new File(resultPath2);
//////
//////            int totalCount = 0;
//////            int errorCount = 0;
//////            float successRate;
//////
//////            Writer writer = null;
//////
//////            Scanner scanner = new Scanner(file);
//////            //scanner.useDelimiter(",");
//////            scanner.nextLine();
//////
//////            while (scanner.hasNextLine()) {
//////                if (scanner.next().contains("+")){
//////                    errorCount++;
//////                }
//////                totalCount++;
//////            }
//////
//////            successRate = ((float) (totalCount-errorCount) / totalCount ) * 100;
//////
//////            try {
//////                writer = new BufferedWriter(new OutputStreamWriter(
//////                        new FileOutputStream(resultPath2, true), "utf-8"));
//////                writer.write("Success rate: %" + successRate + " completed in: " + duration/1000.0 + " sec");
//////                Log.d("VEHCLA", "Success rate: %" + successRate + " completed in: " + duration/1000.0 + " sec");
//////            } catch (UnsupportedEncodingException e) {
//////                e.printStackTrace();
//////            } catch (IOException e) {
//////                e.printStackTrace();
//////            }
//////
//////
//////        } catch (FileNotFoundException e) {
//////            e.printStackTrace();
//////        } catch (InterruptedException e) {
//////            e.printStackTrace();
//////        }
//////    }
////
//////    public void run(){
//////        //burada degerler matrisi hazir demek
//////        //simdi bir sekilde yollamak lazim
//////        //burada siniflandirmanin bir metodu cagirilacak.
////////    	System.out.println("Running ");
//////    }
////
////    //    public void oku() throws FileNotFoundException, InterruptedException{
//////    	long startTime;
//////    	long endTime;
//////        long startTime2 = System.currentTimeMillis();
//////        long endTime2;
//////    	long duration;
//////        int instanceIndex = 0;
//////        int degerSayisi = 0;
//////        int r = 0;
//////        int i, indis;
//////
//////        Scanner scanner;
//////        String fileName;
//////        //RunnableClassify rc;
//////        //Thread t;
////////    	System.out.println(this.listOfFiles.length);
//////        for (File file : this.listOfFiles) {
//////            fileName = file.getName();
//////            if (this.isCSV(fileName)) {
//////                System.out.println(file.getName() + " is being read...");
//////                scanner = new Scanner(file);
//////                scanner.useDelimiter(",");
//////                scanner.nextLine();
//////                //like initialize
//////                label = fileName.split("_")[1];
//////
//////                while(scanner.hasNextLine()){
//////                    startTime = System.currentTimeMillis();
//////
//////                    if(degerSayisi < this.degerlerBoyut){
//////                        for(i = 3; i < argSayisi2; i++){
//////                            this.degerler[i][r] = scanner.nextDouble();
//////                        }
//////                        for(i = 0; i < 3; i++){
//////                            indis = (i + 1) * 3;
//////                            this.degerler[i][r] = Math.sqrt(Math.pow(this.degerler[indis][r], 2)
//////                                    + Math.pow(this.degerler[indis + 1][r], 2)
//////                                    + Math.pow(this.degerler[indis + 2][r], 2));
//////                        }
//////                        r++;
//////                        r %= this.degerlerBoyut;
//////                        degerSayisi++;
//////                        scanner.nextLine();
//////
//////                    }
//////                    else if ( (Thread.currentThread().isInterrupted()) ){return;}
//////
//////                    else {
//////
//////                        instanceIndex++;
//////
//////                        degerSayisi -= atlamaMiktari;
//////
//////                        //System.arraycopy(degerler, 0, degerlerCPY, 0, degerler.length);
//////
//////                        double instanceValues[][] = new double[argSayisi2][ozellikSayisi];
//////                        int thresholdResult = islem(degerler, r, argSayisi2, instanceValues);
//////
//////                        int j, sayac;
//////
//////                        String timeStamp = newDateFormat.format(new Date());
//////                        //String location = null;
//////
//////                        String classificationResult = null;
//////
//////                        String thresholdClassificationResult = "VEHICULAR";
//////
//////                        int thresholdIndis = -1;
//////
//////                        switch (thresholdResult) {
//////
//////                            case -1:
//////                                //classify
//////                                thresholdIndis = -1;
//////                                break;
//////                            case 10:
//////                                //stationary
//////                                classificationResult = "STATIONARY";
//////                                thresholdClassificationResult = classificationResult;
//////                                thresholdIndis = 10;
//////                                break;
//////                            case 0:
//////                                //walking
//////                                classificationResult = "WALKING";
//////                                thresholdClassificationResult = classificationResult;
//////                                thresholdIndis = 0;
//////                                break;
//////                            default:
//////                                //error
//////                                classificationResult = "error wrong class: " + thresholdResult;
//////                                thresholdClassificationResult = classificationResult;
//////                                break;
//////                        }
//////
//////                        sayac = 0;
//////                        for (i = 0; i < argSayisi2; i++) {
//////                            for (j = 0; j < ozellikSayisi; j++) {
//////                                instance.setValue(sayac, instanceValues[i][j]);
//////                                sayac++;
//////                            }
//////                        }
//////                        //instance.setValue(sayac, "bus");
//////                        instance.setValue(sayac, label);
//////
//////                        int result = -50;
//////                        try {
//////                            result = (int) cls.classifyInstance(instance);
//////                            classificationResult = instance.classAttribute().value(result);
//////                        } catch (Exception e) {
//////                            e.printStackTrace();
//////                        }
//////
//////                        writeHealableResult(timeStamp,thresholdClassificationResult,thresholdIndis,classificationResult,result);
//////
//////                        double zcth = ((instanceValues[0][27] * 100)/(instanceValues[0][26]+instanceValues[0][27]+instanceValues[0][28]));
//////
//////                        double real = instance.classValue(); //Bunlar değişecek
//////                        writeResultWithDebugValues(instanceIndex,real,result,classificationResult,instanceValues[0][12],zcth);
//////
//////
//////                    }
//////                }
//////                scanner.close();
//////            }
//////
//////        }
//////
//////    }
////
////}
