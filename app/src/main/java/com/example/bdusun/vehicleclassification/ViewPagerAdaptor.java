package com.example.bdusun.vehicleclassification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by bariscan on 27/05/2017.
 */

public class ViewPagerAdaptor extends PagerAdapter {
    private int resultImages[] = {
            R.drawable.walking,
//            R.drawable.minibus,
            R.drawable.car,
            R.drawable.bus,
            R.drawable.tram,
//            R.drawable.subway,
            R.drawable.metro,
            R.drawable.marmaray,
//            R.drawable.metrobus,
            R.drawable.ferry,
            R.drawable.airplane,
            R.drawable.stationary
    };

    private String resultNames[] = {
            "Walking",
//            "Minibus",
            "Car",
            "Bus",
            "Tram",
//            "Hafif Raylı Metro",
            "Metro",
            "Marmaray",
//            "Metrobus",
            "Ferry",
            "Airplane",
            "Stationary"
    };

    private static final String TAG = "ViewPagerAdaptor";
    Activity activity;
    ArrayList<Integer> results;
    ArrayList<Integer> numOfRes;
    LayoutInflater inflater;


    public ViewPagerAdaptor(Activity activity){
        this.activity = activity;
        this.results = new ArrayList<Integer>();
        this.numOfRes = new ArrayList<Integer>();
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
//        if (position >= images.length){
//            Log.d(TAG, "positions" + position);
//            position %= images.length;
//        }

        int indis = this.results.size() - position - 1;
        Log.d(TAG, "positions: " + indis);
        Log.d(TAG, "length: " + results.size());
        this.inflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.cls_viewpager_item, container, false);

        ImageView image;
        image = (ImageView) itemView.findViewById(R.id.clsImageView);
        TextView imageName = (TextView) itemView.findViewById(R.id.clsTextView);
        DisplayMetrics dis = new DisplayMetrics();
        this.activity.getWindowManager().getDefaultDisplay().getMetrics(dis);
        int height = dis.heightPixels;
        int width = dis.widthPixels;
        image.setMinimumHeight(height);
        image.setMinimumWidth(width);

        try {
            Picasso.with(this.activity.getApplicationContext())
                    //.load(images[position])
                    .load(this.resultImages[results.get(indis)])
                    .placeholder(R.mipmap.ic_launcher)
                    .error(R.mipmap.ic_launcher)
                    .into(image);
            imageName.setText(this.resultNames[results.get(indis)] + " (" + numOfRes.get(indis) + " min) ");
        }catch (Exception ex){
            Log.d(TAG, ex.getMessage());
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }


    public void addImages(int result){


        if (this.results.size() > 0){
            int oldResult = results.get(results.size() - 1);
            if (oldResult == result){
                this.numOfRes.set(numOfRes.size() - 1, numOfRes.get(numOfRes.size() - 1) + 1);
            }else {
                this.results.add(result);
                this.numOfRes.add(1);
            }
        }else{
            this.results.add(result);
            this.numOfRes.add(1);
        }

//        Log.d(TAG, "addImages: " + numOfRes.get(numOfRes.size()-1));
        this.notifyDataSetChanged();
    }


}
