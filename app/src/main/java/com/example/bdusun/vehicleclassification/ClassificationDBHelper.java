package com.example.bdusun.vehicleclassification;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by bariscan on 05/05/2017.
 */

public class ClassificationDBHelper extends SQLiteOpenHelper{

    public static final int DATA_BASE_VERSION = 1;
    public static final String DATA_BASE_NAME = "vehicle_classification.db";

    private static final String SQL_CREATE_RESULT = "CREATE TABLE " + FeedClassificationContract.FeedResult.TABLE_NAME
            + " ( " + FeedClassificationContract.FeedResult._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT + " INTEGER NOT NULL, "
            + FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT + " INTEGER NOT NULL, "
            + FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT + " INTEGER, "
            + FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT + " INTEGER, "
            + FeedClassificationContract.FeedResult.COLUMN_NAME_LATITUDE + " double, "
            + FeedClassificationContract.FeedResult.COLUMN_NAME_LONGITUDE + " double, "
            + FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP )";

    private static final String SQL_DELETE_RESULT = "DROP TABLE IF EXIST " + FeedClassificationContract.FeedResult.TABLE_NAME;

    public ClassificationDBHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_RESULT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_RESULT);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
}
