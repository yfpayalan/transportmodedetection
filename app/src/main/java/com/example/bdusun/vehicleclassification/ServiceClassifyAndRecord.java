package com.example.bdusun.vehicleclassification;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_RECORD_FOLDER;
import static com.example.bdusun.vehicleclassification.MainActivity.heal;

import static com.example.bdusun.vehicleclassification.Constants.FILE.JOURNEY_DATA_FOLDER;
import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TEST_FOLDER;


/**
 * Created by bdusun on 21/02/17.
 */

public class ServiceClassifyAndRecord extends Service implements SensorEventListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<Status> {


//    public static SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");
//    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS

    public static SimpleDateFormat newDateFormatFileName = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm");
    //public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm:ss");
    public static SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS

    public static int FREKANS = 100;
    public static double ATLAMA_ORANI = 0.6;
    public static int TOPLAMA_SURESI = 60;

    public static final String LINEAR_ACC_BROADCAST = ServiceClassifyAndRecord.class.getName()+"LinearAccBroadcast",
                               EXTRA_LINEAR_ACCZ    = "EXTRA_LINEAR_ACCZ",
                               EXTRA_LINEAR_ACCX    = "EXTRA_LINEAR_ACCX",
                               EXTRA_LINEAR_ACCY    = "EXTRA_LINEAR_ACCY",
                                EXTRA_GYROX    = "EXTRA_GYROX",
                                EXTRA_GYROY    = "EXTRA_GYROY",
                                EXTRA_GYROZ    = "EXTRA_GYROZ";
    public static final String LOCATION_BROADCAST = ServiceClassifyAndRecord.class.getName()+"LocationBroadcast",
                                EXTRA_LAT      = "EXTRA_LAT",
                                EXTRA_LONG     =  "EXTRA_LONG",
                                EXTRA_SPEED     =  "EXTRA_SPEED";



//    public static SimpleDateFormat newDateFormatFileName = new SimpleDateFormat("yyyyMMdd");
//    public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm");
    //public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm:ss");


    private ClassificationDBHelper clsResDBHelper;
    private SQLiteDatabase dbRead;
    private SQLiteDatabase dbWrite;

    private int results[];
    private int combineResults[];
    private int ids[];


    public static boolean IS_SERVICE_RECORD_RUNNING = false;

//    public static double walkingThresholdStddev = 2.0;
//    public static double stationaryThresholdStddev = 0.04;

    public static double walkingThresholdStddev;
    public static double stationaryThresholdStddev;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mGyroscope;
    private Sensor mMagnetometer;
    private Sensor mLinearAccelerometer;

    private long mLastWrite;
    private long mLastUpdateA;
    private long mLastUpdateG;
    private long mLastUpdateM;
    private long mLastUpdateL;
    private long totalTime;
    private long totalTimeDebug;

    private String textToSet = "";
    private String precision = "%.3f";
    private String dateCreated;

    private static long interval_count_stationary = 100L;
    private static long interval_write = 10L; // 50 //yazma skligi //normalde alttaki ile ayni olmali ama bu sekilde farkli deger gelme ihitmali artiyor
    private static long interval_update = 5L; // in ms 10=> 100Hz //frequency
    private static long interval_delay = 0L;
    private static int interval_GPS_time = 50000; // 10 sec GPS
    private static int interval_GPS_location = 1000; // 10 sec GPS
    private static long GPS_INTERVAL = 59000L; // 5000 -> 5 sec GPS
    private static long FATEST_INTERVAL = 30000L; // 5000 -> 5 sec GPS
    private static int DISPLACEMENT = 10; // 10 meters
    private static int size = 1000;
    int accI = 0, gyroI = 0;

    private double degerler[][];
    private int degerlerBoyut = 6000;
    private int degerlerBoyutStationary = 100;
    private int degerSayisiStationary;
    //private int baslangic;
    private int degerSayisi;
    private int atlamaMiktari;
    private int r;
    private static int instanceIndex;

    private int stationaryCount;
    private int walkingCount;

    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String lat, lon, alt, speed, accuracy,gpsInterval;

    private Date date;

    float accX;
    float gyroX;
    float accY;
    float gyroY;
    float accZ;
    float gyroZ;
    float magX;
    float magY;
    float magZ;
    float linearAccX;
    float linearAccY;
    float linearAccZ;
    float[] accValues;
    float[] magValues;
    float[] gyroValues;
    float[] linearAccValues;
    long timeInMs;
    private String longFlag; //default flag. represents vehicle stops. 'A' acceleration, 'D' deceleration
    private String lateralFlag; // defines does vehicle moves left or right ?

    private RunnableIslem runnableIslem;

    OutputStreamWriter outputStreamWriter;
    BufferedWriter bw;
    FileOutputStream fOut = null;

//    LocationManager locationManager;
    LocationListener locationListener;

    Timer mTimer;

    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;

    private String TAG = this.getClass().getSimpleName();
    private WindowManager mWindowManager;
    private LinearLayout touchLayout;
    private WindowManager wm;
    private LinearLayout ll;
    private TextView tv;

    private Thread t;

    private String journeyFilePath;
    private String csvFilePath;

    private boolean isCsvEnabled = true;
    private boolean isClassificationEnabled = true;

    public static void setWalkingThresholdStddev(float walkingThresholdStddev) {
        ServiceClassifyAndRecord.walkingThresholdStddev = walkingThresholdStddev;
    }

    public static void setStationaryThresholdStddev(float stationaryThresholdStddev) {
        ServiceClassifyAndRecord.stationaryThresholdStddev = stationaryThresholdStddev;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        dbRead = clsResDBHelper.getReadableDatabase();
        clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        dbWrite = clsResDBHelper.getWritableDatabase();
        //dateCreated = dateFormatFileName.format(new Date());

        //Get reference to SensorManager

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //Get reference to Accelerometer
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //Get reference to Gyroscope
        mGyroscope = mSensorManager
                .getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        //Get reference to Magnetometer
        mMagnetometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mLinearAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                //.addApi(ActivityRecognition.API)
                .build();
        mGoogleApiClient.connect();
        createLocationRequest();

//        // Acquire a reference to the system Location Manager
//        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
//
        // Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                Log.d(TAG, "location lat: "+lat + " , " + "lon: " + lon);
                // Called when a new location is found by the network location provider.
                lat = String.valueOf(location.getLatitude());
                lon = String.valueOf(location.getLongitude());
//                accuracy = String.valueOf(location.getAccuracy());
//                alt = String.valueOf(location.getAltitude());
//                speed = String.valueOf(location.getSpeed());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "SensorReadServiceWakelock");

        degerler = new double[12][this.degerlerBoyut];

        //baslangic=0;
        degerSayisi=0;
        //atlamaMiktari = 3600;
        r=0;
        instanceIndex = 0;
        stationaryCount = 0;
        degerSayisiStationary = 0;



        this.atlamaMiktari = (int) (FREKANS * TOPLAMA_SURESI * ATLAMA_ORANI);
        Log.d(TAG, "Atlama mik: " + this.atlamaMiktari);


    }

    /**
     * Sets the location request parameters.
     */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
//        mLocationRequest.setInterval(50000L);
        mLocationRequest.setFastestInterval(100);
//        mLocationRequest.setFastestInterval(30000L);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

//    synchronized void buildGoogleApiClient() {
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API)
//                .build();
//    }

    private void showNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

//        Intent previousIntent = new Intent(this, ForegroundService.class);
//        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
//        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
//                previousIntent, 0);
//
//        Intent playIntent = new Intent(this, ServiceSensorRecord.class);
//        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
//        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
//                playIntent, 0);
//
//        Intent nextIntent = new Intent(this, ForegroundService.class);
//        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
//        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
//                nextIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Mini Sensor Module")
                .setTicker("Mini Sensor Module")
                .setContentText("Sensor Recording")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
//                .addAction(android.R.drawable.ic_media_previous, "Previous",
//                        ppreviousIntent)
//                .addAction(android.R.drawable.ic_media_play, "Play",
//                        pplayIntent);
//                .addAction(android.R.drawable.ic_media_next, "Next",
//                        pnextIntent)
                .build();
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                notification);


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        long actualTime, tmpTime;
//Sensor.TYPE_LINEAR_ACCELERATION
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            actualTime = System.currentTimeMillis();
            tmpTime = actualTime - mLastUpdateA;

            //if (tmpTime >= interval_update) {
            date = new Date();
            totalTime += tmpTime;

            mLastUpdateA = actualTime;

            accX = event.values[0];
            accY = event.values[1];
            accZ = event.values[2];

//            Log.i("ACCELEROMETER-X : ",String.valueOf(accX));
//            Log.i("ACCELEROMETER-Y : ",String.valueOf(accY));
//            Log.i("ACCELEROMETER-Z : ",String.valueOf(accZ));

            accValues = event.values;
            //}


        }

        else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {

            actualTime = System.currentTimeMillis();
            tmpTime = actualTime - mLastUpdateG;

            //if (actualTime - mLastUpdateG >= interval_update) {
            totalTimeDebug += tmpTime;

            mLastUpdateG = actualTime;

            gyroX = event.values[0];
            gyroY = event.values[1];
            gyroZ = event.values[2];
//            Log.i("GYRO-X: ",String.valueOf(gyroX));
//            Log.i("GYRO-Y: ",String.valueOf(gyroY));
//            Log.i("GYRO-Z: ",String.valueOf(gyroZ));

            gyroValues = event.values;
            //}

        }

        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {

            actualTime = System.currentTimeMillis();
            tmpTime = actualTime - mLastUpdateM;

            //if (actualTime - mLastUpdateG >= interval_update) {
            totalTimeDebug += tmpTime;

            mLastUpdateM = actualTime;

            magX = event.values[0];
            magY = event.values[1];
            magZ = event.values[2];

            magValues = event.values;
            //}

        } else if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {

            actualTime = System.currentTimeMillis();
            tmpTime = actualTime - mLastUpdateL;

            //if (actualTime - mLastUpdateG >= interval_update) {
            totalTimeDebug += tmpTime;

            mLastUpdateL = actualTime;

            linearAccX = event.values[0];
            linearAccY = event.values[1];
            linearAccZ = event.values[2];

//            Log.i("LINEAR-ACCZ: ",String.valueOf(linearAccZ));
            sendBroadcast(linearAccZ,linearAccX,linearAccY,gyroX,gyroY,gyroZ);
            linearAccValues = event.values;
            //}

        }

    }

    /**
     * intended to send attributes for printing at main activity
     */
    private void sendBroadcast(float lx,float ly,float lz, float gyroX, float gyroY,float gyroZ)
    {
        Intent intent = new Intent(LINEAR_ACC_BROADCAST);
        intent.putExtra(EXTRA_LINEAR_ACCZ,lz);
        intent.putExtra(EXTRA_LINEAR_ACCX,lx);
        intent.putExtra(EXTRA_LINEAR_ACCY,ly);
        intent.putExtra(EXTRA_GYROX,gyroX);
        intent.putExtra(EXTRA_GYROY,gyroY);
        intent.putExtra(EXTRA_GYROZ,gyroZ);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void broadcastLocation(String lat,String lon,String speed)
    {
        Intent intent = new Intent(LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LAT,lat);
        intent.putExtra(EXTRA_LONG,lon);
        intent.putExtra(EXTRA_SPEED,speed);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        if (intent!=null) {
//
//        }
        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
//            Log.i(LOG_TAG, "Received Start Foreground Intent ");
            showNotification();
//            Toast.makeText(this, "Service Started!", Toast.LENGTH_SHORT).show();

            wakeLock.acquire();

            //String path = Environment.getExternalStorageDirectory().toString() + "/AASensor/";
//            String path = Environment.getExternalStorageDirectory().toString() + "/VehicleClassification/SensorData/Test";

            isCsvEnabled = intent.getBooleanExtra(getString(R.string.param_csv_enabled),true);
//            isClassificationEnabled = intent.getBooleanExtra(getString(R.string.param_classification_enabled),true);
            isClassificationEnabled = false;
            Log.d(TAG, "isClassificationEnabled = " + isClassificationEnabled);
            Log.d(TAG, "isCSVEnabled = " + isCsvEnabled);

            journeyFilePath = JOURNEY_DATA_FOLDER;


            if (this.isClassificationEnabled ){
                this.runnableIslem = new RunnableIslem(this.getApplicationContext());
            }




            File directory = new File(journeyFilePath);
            if (!directory.exists()) {
                if(!directory.mkdirs()){
                    Toast.makeText(getBaseContext(),"mkdirs failed",Toast.LENGTH_SHORT).show();
                }
            }

            //csvFilePath = SENSOR_DATA_TEST_FOLDER;
            //csvFilePath = SENSOR_DATA_FOLDER;
            csvFilePath = SENSOR_DATA_RECORD_FOLDER;

            directory = new File(csvFilePath);
            if (!directory.exists()) {
                if(!directory.mkdirs()){
                    Toast.makeText(getBaseContext(),"mkdirs failed",Toast.LENGTH_SHORT).show();
                }
            }

            dateCreated = dateFormatFileName.format(new Date());
            //directory = new File(SENSOR_DATA_TEST_FOLDER, "BSensor_" + dateCreated + ".csv");
            directory = new File(SENSOR_DATA_RECORD_FOLDER, "BSensor_" + dateCreated + ".csv");

            if(isCsvEnabled){
                try {
                    fOut = new FileOutputStream(directory);
                    outputStreamWriter = new OutputStreamWriter(fOut);

                    bw = new BufferedWriter(outputStreamWriter);
                    bw.write("ACCELEROMETERX,ACCELEROMETERY,ACCELEROMETERZ,GYROSCOPEX,GYROSCOPEY," +
                            "GYROSCOPEZ,MAGNETICFIELDX,MAGNETICFIELDY,MAGNETICFIELDZ," + "LINEARACCX,LINEARACCY,LINEARACCZ," +
                            "AZIMUTHDEG,PITCHDEG,ROLLDEG," +
                            "LAT,LON,ALT,SPEEDINKMH,ACCURACY,TIMEINMS,SAMPLETIME,LONGFLAG,LATFLAG\n");
                    //outputStreamWriter.close();
                } catch (IOException e) {
                    Log.e("Exception", "File open failed: " + e.toString());

                }
            }

//            try {
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, interval_GPS_time, interval_GPS_location, locationListener);
//                Log.d(TAG, "registered locationlistener");
//            } catch (SecurityException e) {
//                e.printStackTrace();
//            }


            mSensorManager.registerListener(this, mAccelerometer,
                    SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this, mGyroscope,
                    SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this, mMagnetometer,
                    SensorManager.SENSOR_DELAY_FASTEST);
            mSensorManager.registerListener(this, mLinearAccelerometer,
                    SensorManager.SENSOR_DELAY_FASTEST);

            mLastWrite = mLastUpdateL = mLastUpdateM = mLastUpdateA = mLastUpdateG = System.currentTimeMillis();


            TimerTask tt = new TimerTask() {
                @Override
                public void run() {
                    sendToClassification(isCsvEnabled,isClassificationEnabled);
                }
            };

            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(tt, interval_write + interval_delay, interval_write/*period*/);
            //   mTimer.scheduleAtFixedRate(tt2,interval_write+interval_delay,interval_count_stationary);

        }

        else if (intent.getAction().equals(
                Constants.ACTION.STOPFOREGROUND_ACTION)) {
//            Log.i(LOG_TAG, "Received Stop Foreground Intent");
            stopForeground(true);
            stopSelf();
        }
        return START_NOT_STICKY; //START_NOT_STICKY; //START_STICKY;

    }

//    /**
//     * Makes a request for location updates. Note that in this sample we merely log the
//     * {@link SecurityException}.
//     */
//    public void startService(Context context) {
//        Log.i(TAG, "Requesting location updates");
////        Utils.setRequestingLocationUpdates(this, true);
//        startService(new Intent(context, ServiceClassifyAndRecord.class));
//        try {
//            LocationServices.FusedLocationApi.requestLocationUpdates(
//                    mGoogleApiClient, mLocationRequest, ServiceClassifyAndRecord.this);
//        } catch (SecurityException unlikely) {
////            Utils.setRequestingLocationUpdates(this, false);
//            Log.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
//        }
//    }
//
//    /**
//     * Removes location updates. Note that in this sample we merely log the
//     * {@link SecurityException}.
//     */
//    public void stopService() {
//        Log.i(TAG, "Removing location updates");
//        try {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
//                    ServiceClassifyAndRecord.this);
////            Utils.setRequestingLocationUpdates(this, false);
//            stopSelf();
//        } catch (SecurityException unlikely) {
////            Utils.setRequestingLocationUpdates(this, true);
//            Log.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
//        }
//    }


    public synchronized void  countStationary(int stop){ //TODO remove synchronized
        double accMagnitude;
        double degerler2[][];
        degerler2 = this.degerler.clone();

        int i, j;
        double accXAvg = 0;
        double accYAvg = 0;
        double accZAvg = 0;
        //double accMagnitudes[] = new double[degerlerBoyutStationary];
        double accMagnitudeAvg = 0;
        double stdAccMagnitude = 0;
        double sum = 0;

        int start = ((stop - this.degerlerBoyutStationary) +this.degerlerBoyut) % this.degerlerBoyut;
        j = start;


        for (i=0; i < this.degerlerBoyutStationary; i++) {
//            accXAvg += degerler2[3][i];
//            accYAvg += degerler2[4][i];
//            accZAvg += degerler2[5][i];
            //accMagnitudes[i] = Math.sqrt(Math.pow(degerler2[3][i],2)+Math.pow(degerler2[4][i],2)+Math.pow(degerler2[5][i],2));
            //accMagnitudeAvg += accMagnitudes[i];
            accMagnitudeAvg += degerler2[0][j];
            j++;
            j %= this.degerlerBoyut;
        }
//        accXAvg /= degerlerBoyutStationary;
//        accYAvg /= degerlerBoyutStationary;
//        accZAvg /= degerlerBoyutStationary;
//
//        accMagnitude = Math.sqrt(Math.pow(accXAvg,2) + Math.pow(accYAvg,2) + Math.pow(accZAvg,2));
//        if ((accMagnitude >= 9.3) || (accMagnitude <= 10.3)){
//            stationaryCount++;
//        }
        j = start;
        accMagnitudeAvg /= degerlerBoyutStationary;
        for(i=0; i < this.degerlerBoyutStationary; i++)
        {
            sum += Math.pow((degerler2[0][j]-accMagnitudeAvg),2);
            j++;
            j %= this.degerlerBoyut;
        }
        stdAccMagnitude = Math.sqrt(sum/(this.degerlerBoyutStationary-1));
        //        Log.d("VEHCLA-SensorRead","stdAccMagnitude: "+stdAccMagnitude);

        if (stdAccMagnitude>=walkingThresholdStddev){ // 0.09
            walkingCount++;
        }
        else if (stdAccMagnitude<=stationaryThresholdStddev){ // >1
            stationaryCount++;
        }

//        Writer writer;
//        try {
//            writer = new BufferedWriter(new OutputStreamWriter(
//                    new FileOutputStream(JOURNEY_DATA_FOLDER+"1secdebug.csv", true), "utf-8"));
//            //writer.write("stycnt: " + stationaryCount + ", wlkcnt: " + walkingCount + ", stdaccmag: " + stdAccMagnitude + ", start" + start + ", stop: " +stop +"\n");
//            writer.write("stycnt: " + stationaryCount + ", wlkcnt: " + walkingCount + ", stdaccmag: " + stdAccMagnitude + ", " + dateFormatFileName.format(new Date())  +"\n");
//            writer.close();
//        }catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    public void writeToFile(){
        float azimuth=0, pitch=0, roll=0;
        float orientation[] = new float[3];

        //timeInMs += tmpTime;

        float R[] = new float[9];
        float I[] = new float[9];
        if (accValues != null && magValues != null) {
            boolean success = SensorManager.getRotationMatrix(R, I, accValues, magValues);
            if (success) {
                SensorManager.getOrientation(R, orientation);
                azimuth = (float) Math.toDegrees(orientation[0]); // orientation contains: azimuth, pitch and roll
                pitch = (float) Math.toDegrees(orientation[1]);
                roll = (float) Math.toDegrees(orientation[2]);
                //azimuth = orientation[0]; // orientation contains: azimuth, pitch and roll
                //pitch = orientation[1];
                //roll = orientation[2];
            }
        }

        try {

            //simple decision machine. longtidunial decision(acc or decc)
            if ( linearAccZ > MainActivity.MOTION_THRESHOLD)
                setLongFlag("A"); //accelerated.
            else if( linearAccZ < MainActivity.MOTION_DEC_THRESHOLD)
                setLongFlag("D"); //decelerated.
            else
                setLongFlag("S");
            // simple decision machine for lateral events.
            if (gyroZ > MainActivity.GYRO_THRESHOLD_LEFT)
            {
                setLateralFlag("LEFT");
            }
            else if(gyroZ < MainActivity.GYRO_THRESHOLD_RIGHT)
            {
                setLateralFlag("RIGHT");
            }
            else
                setLateralFlag("NO");



            //timeInMs+","+dateFormat.format(date[i])+"\n");
            bw.write(String.format(Locale.ENGLISH,precision,accX) + "," +
                    String.format(Locale.ENGLISH,precision,accY) + "," +
                    String.format(Locale.ENGLISH,precision,accZ) +"," +

                    String.format(Locale.ENGLISH,precision,gyroX)+"," +
                    String.format(Locale.ENGLISH,precision,gyroY)+"," +
                    String.format(Locale.ENGLISH,precision,gyroZ)+ "," +

                    String.format(Locale.ENGLISH,precision,magX)+"," +
                    String.format(Locale.ENGLISH,precision,magY)+"," +
                    String.format(Locale.ENGLISH,precision,magZ)+ "," +

                    String.format(Locale.ENGLISH,precision,linearAccX)+"," +
                    String.format(Locale.ENGLISH,precision,linearAccY)+"," +
                    String.format(Locale.ENGLISH,precision,linearAccZ)+ "," +

                    String.format(Locale.ENGLISH,precision,azimuth)+"," +
                    String.format(Locale.ENGLISH,precision,pitch)+"," +
                    String.format(Locale.ENGLISH,precision,roll)+ "," +

                    lat + "," + lon + "," + alt + "," +
                    speed + "," + accuracy + "," +

                    tmpTime+","+dateFormat.format(new Date())+","
                    +getLongFlag()+","+getLateralFlag()+"\n");

//                System.out.println(accX[i] + "," +accY[i] + "," + accZ[i] +","+
//                        gyroX[i]+","+ gyroY[i]+","+ gyroZ[i]+ ","+
//                        timeInMs[i]+","+timeInMsDebug[i]+" "+"\n");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    long tmpTime;

    public void sendToClassification(boolean isCsvEnabled, boolean isClassificationEnabled) {

        //timeInMs += System.currentTimeMillis() - mLastWrite;
        tmpTime = System.currentTimeMillis() - mLastWrite;
        timeInMs += tmpTime;
        mLastWrite = System.currentTimeMillis();

//        float azimuth = 0, pitch = 0, roll = 0;
//        float orientation[] = new float[3];

        dateCreated = newDateFormatFileName.format(new Date());

        journeyFilePath = JOURNEY_DATA_FOLDER + dateCreated + ".txt";
        //System.out.println(journeyFilePath);
        File directory = new File(journeyFilePath);
        try {
             //TODO HEAL 24 saatlik bu true ise bir onceki gunun dosyasina heal yapilir
            if (directory.createNewFile()){

                Log.d(TAG, "createNewFile");
                String beginDate = this.generateDate(24);
                String endDate = this.generateDate(0);
                final int numOfResults = this.getToBeHealedCount(beginDate, endDate);
                this.generateDataToBeHealed(beginDate, endDate, numOfResults);

                Thread threadResult = new Thread(){
                    public void run(){
                        System.out.println("Thread Running");
                        heal(results, numOfResults, 12);
                        Log.d(TAG, "updateSQL start");
                        updateToSQL(numOfResults, FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT, results);
                        Log.d(TAG, "updateSQL end");
                    }
                };

                threadResult.start();
                Thread threadCombined = new Thread(){
                    public void run(){
                        System.out.println("Thread Running");
                        heal(combineResults, numOfResults, 9);
                        Log.d(TAG, "updateSQL start");
                        updateToSQL(numOfResults, FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT, combineResults);
                        Log.d(TAG, "updateSQL end");
                    }
                };

                threadCombined.start();

                threadResult.join();
                threadCombined.join();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (timeInMs < 100L)
            return;

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                writeToFile();
                //writeToFile(); TODO to export sensor data !
            }
        };
        if(isCsvEnabled) {
            runnable.run();
        }

        int i;
        int indis;

//        float R[] = new float[9];
//        float I[] = new float[9];
//        if (accValues != null && magValues != null) {
//            boolean success = SensorManager.getRotationMatrix(R, I, accValues, magValues);
//            if (success) {
//                SensorManager.getOrientation(R, orientation);
//                azimuth = (float) Math.toDegrees(orientation[0]); // orientation contains: azimuth, pitch and roll
//                pitch = (float) Math.toDegrees(orientation[1]);
//                roll = (float) Math.toDegrees(orientation[2]);
//                //azimuth = orientation[0]; // orientation contains: azimuth, pitch and roll
//                //pitch = orientation[1];
//                //roll = orientation[2];
//            }
//        }

        String loc = lat + " " + lon; //TODO ","
        if (degerSayisi < this.degerlerBoyut) {

            this.degerler[3][r] = accX;
            this.degerler[4][r] = accY;
            this.degerler[5][r] = accZ;
            this.degerler[6][r] = gyroX;
            this.degerler[7][r] = gyroY;
            this.degerler[8][r] = gyroZ;
            this.degerler[9][r] = magX;
            this.degerler[10][r] = magY;
            this.degerler[11][r] = magZ;

            for (i = 0; i < 3; i++) {
                indis = (i + 1) * 3;
                this.degerler[i][r] = Math.sqrt(Math.pow(this.degerler[indis][r], 2)
                        + Math.pow(this.degerler[indis+1][r], 2)
                        + Math.pow(this.degerler[indis+2][r], 2));
            }
            r++;
            r %= this.degerlerBoyut;
            degerSayisi++;
            degerSayisiStationary++;

            if(isClassificationEnabled) {
                if ((degerSayisiStationary % degerlerBoyutStationary) == 0) {
                    Runnable run = new Runnable() {
                        @Override
                        public void run() {
                            countStationary(degerSayisiStationary);
                        }
                    };
                    run.run();
                    //degerSayisiStationary = 0;
                }
            }

//            Log.d(TAG, "degerSayisiStationary: " + degerSayisiStationary);
        } else
            {

            instanceIndex++;

//            degerSayisi = 0; //TODO degerSayisi = 0;
            this.degerSayisi -= this.atlamaMiktari;

            // send stationaryCount to RunnableIslem
            if(isClassificationEnabled)
            {
                Log.d(TAG, newDateFormat.format(new Date()) +" starting thread " + "walkStdThold: " + walkingThresholdStddev + " statStdThold: " + stationaryThresholdStddev + " lat: " + lat + " lon: " + lon);
                Log.d(TAG, "isClassificationEnabled = " + isClassificationEnabled);
                Log.d(TAG, "isCSVEnabled = " + isCsvEnabled);
                this.runnableIslem.setParamaters(instanceIndex, r, degerler, stationaryCount, walkingCount,newDateFormat.format(new Date()), dateFormat.format(new Date()), lat, lon, journeyFilePath);
                t = new Thread(this.runnableIslem, "t"+instanceIndex);
                t.start();
                stationaryCount = 0;
                walkingCount = 0;
                degerSayisiStationary = 0;
            }

        }

    }


    public int getToBeHealedCount(String beginDate, String endDate){
        String[] projection = {
                "COUNT(*)"
//                FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT
        };

        int count = 0;

        Log.d(TAG, "beginDate: " + beginDate + "endDate: " + endDate);

//        Select * from users where created_at between '2017-05-05 10:45:46' and '2017-05-05 11:46:19'
        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + "  BETWEEN ? AND ?";
        String selecttionArgs[] = {
                beginDate,
                endDate
        };

//        String sortOrder = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME;// + DESC ve artan gibi bir şey eklenebilir

        Cursor cursor = dbRead.query(
                FeedClassificationContract.FeedResult.TABLE_NAME,
                projection,
                selection,
                selecttionArgs,
                null,
                null,
                null
        );

        if (cursor.moveToNext()){
            count = cursor.getInt(cursor.getColumnIndex("COUNT(*)"));
            Log.d(TAG, "Count: " + count);
        }

        cursor.close();
        return count;
    }


    public void generateDataToBeHealed(String beginDate, String endDate, int numOfResults){

        int i = 0;

        this.results = new int[numOfResults];
        this.combineResults = new int[numOfResults];
        this.ids = new int[numOfResults];


        String[] projection = {
                FeedClassificationContract.FeedResult._ID,
                FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT,
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT,
                FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME
//                FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT
        };

//        Select * from users where created_at between '2017-05-05 10:45:46' and '2017-05-05 11:46:19'
        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + "  BETWEEN ? AND ?";
        String selecttionArgs[] = {
                beginDate,
                endDate
        };

        String sortOrder = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME;// + DESC ve artan gibi bir şey eklenebilir

        Cursor cursor = dbRead.query(
                FeedClassificationContract.FeedResult.TABLE_NAME,
                projection,
                selection,
                selecttionArgs,
                null,
                null,
                sortOrder
        );

        while ((i < numOfResults) && (cursor.moveToNext())){
            this.ids[i] = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult._ID));
            this.results[i] = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT));
            this.combineResults[i] = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT));

//            Log.d(TAG, "id: " + ids[i] + "result: " + results[i] + "combineResults: " + combineResults[i]);
            i++;
//            Log.d(TAG,"DATE TIME: " + cursor.getString(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME)));
        }
        cursor.close();
    }

    public void updateToSQL(int numOfResults, String columnName, int results[]){
        Log.d(TAG, "update to SQL 1");
        Log.d(TAG, "numOfResults: " + numOfResults);
//        Log.d("RunnableIslem", "addToSql 2");
        int i;
        int updateId;

        for (i = 0; i < numOfResults; i++){
//            Log.d(TAG, "columnNAme: " + columnName);
            ContentValues values = new ContentValues();
            values.put(columnName, results[i]);
//            values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT, combineResults[i]);
//            update(MYDATABASE_TABLE, values, KEY_ID+"="+id, null);
            updateId = dbWrite.update(FeedClassificationContract.FeedResult.TABLE_NAME, values, FeedClassificationContract.FeedResult._ID + " = " + ids[i], null);
            Log.d(TAG,columnName + " " + ids[i] + " results: " + results[i] + " updateId: " + updateId);
        }

        Log.d(TAG, "update to SQL end2");

    }

    public String generateDate(int backHours){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        backHours *= (-1);
// get start of this week in milliseconds
        cal.set(Calendar.HOUR_OF_DAY, backHours);
        return dateFormat.format(cal.getTime());
    }

    @Override
    public void onDestroy() {

        mTimer.cancel();
        mSensorManager.unregisterListener(this);

        Log.d(TAG, "isClassificationEnabled = " + isClassificationEnabled);
        Log.d(TAG, "isCSVEnabled = " + isCsvEnabled);

        if(isCsvEnabled) {

            if (this.dbRead != null){
                this.dbRead.close();
            }

            try {
                bw.close();
                outputStreamWriter.close();
                fOut.close();
                Toast.makeText(getBaseContext(), "File saved successfully!",
                        Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        if(mGoogleApiClient.isConnected()){
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

//        try{ locationManager.removeUpdates(locationListener); } catch (SecurityException e) { e.printStackTrace();}
        instanceIndex = 0;
        wakeLock.release();
        super.onDestroy();
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        try {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(location!=null){
//                location.getSpeed();
                lat = Double.toString(location.getLatitude());
                lon = Double.toString(location.getLongitude());
            }else {
                Log.d(TAG,"Location is null");
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, ServiceClassifyAndRecord.this);
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        // In this example, we merely log the suspension.
        Log.e(TAG, "GoogleApiClient connection suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // In this example, we merely log the failure.
        Log.e(TAG, "GoogleApiClient connection failed.");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, "New location: " + location);
        lat = Double.toString(location.getLatitude());
        lon = Double.toString(location.getLongitude());
        speed = Double.toString(location.getSpeed());
        Log.i(TAG,"Time in GPS: "+location.getTime());
        broadcastLocation(lat,lon,speed);
    }

    @Override
    public void onResult(@NonNull Status status) {

    }

    public String getLongFlag() {
        return longFlag;
    }

    public void setLongFlag(String longFlag) {
        this.longFlag = longFlag;
    }

    public String getLateralFlag() {
        return lateralFlag;
    }

    public void setLateralFlag(String lateralFlag) {
        this.lateralFlag = lateralFlag;
    }
}
