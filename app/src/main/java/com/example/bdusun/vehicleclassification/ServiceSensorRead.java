////package com.example.bdusun.vehicleclassification;
////
////import android.Manifest;
////import android.app.Notification;
////import android.app.PendingIntent;
////import android.app.Service;
////import android.content.Context;
////import android.content.Intent;
////import android.content.pm.PackageManager;
////import android.graphics.Bitmap;
////import android.graphics.BitmapFactory;
////import android.hardware.Sensor;
////import android.hardware.SensorEvent;
////import android.hardware.SensorEventListener;
////import android.hardware.SensorManager;
////import android.location.Location;
////import android.location.LocationListener;
////import android.location.LocationManager;
////import android.os.Bundle;
////import android.os.IBinder;
////import android.os.PowerManager;
////import android.support.annotation.Nullable;
////import android.support.v4.app.ActivityCompat;
////import android.support.v7.app.NotificationCompat;
////import android.util.Log;
////import android.view.WindowManager;
////import android.widget.LinearLayout;
////import android.widget.TextView;
////import android.widget.Toast;
////
////import java.io.BufferedWriter;
////import java.io.File;
////import java.io.IOException;
////import java.io.OutputStreamWriter;
////import java.text.SimpleDateFormat;
////import java.util.Date;
////import java.util.Timer;
////import java.util.TimerTask;
////
////import static com.example.bdusun.vehicleclassification.Constants.FILE.JOURNEY_DATA_FOLDER;
////
/////**
//// * Created by bdusun on 21/02/17.
//// */
////
////public class ServiceSensorRead extends Service implements SensorEventListener {
////
//////    public static SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");
//////    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS
////
////    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS
////
////    public static SimpleDateFormat newDateFormatFileName = new SimpleDateFormat("yyyyMMdd");
////    public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm");
////    //public static SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm:ss");
////
////    public static boolean IS_SERVICE_RECORD_RUNNING = false;
////
//////    public static double walkingThresholdStddev = 2.0;
//////    public static double stationaryThresholdStddev = 0.04;
////
////    public static double walkingThresholdStddev;
////    public static double stationaryThresholdStddev;
////
////
////    private SensorManager mSensorManager;
////    private Sensor mAccelerometer;
////    private Sensor mGyroscope;
////    private Sensor mMagnetometer;
////
////    private long mLastWrite;
////    private long mLastUpdateA;
////    private long mLastUpdateG;
////    private long mLastUpdateM;
////    private long totalTime;
////    private long totalTimeDebug;
////
////    private String textToSet = "";
////    private String precision = "%.3f";
////    private String dateCreated;
////
////    private static long interval_count_stationary = 100L;
////    private static long interval_write = 10L; // 50 //yazma skligi //normalde alttaki ile ayni olmali ama bu sekilde farkli deger gelme ihitmali artiyor
////    private static long interval_update = 5L; // in ms 10=> 100Hz //frequency
////    private static long interval_delay = 0L;
////    private static int interval_GPS_time = 50000; // 10 sec GPS
////    private static int interval_GPS_location = 1000; // 10 sec GPS
////    private static int FATEST_INTERVAL = 30000; // 5000 -> 5 sec GPS
////    private static int DISPLACEMENT = 10; // 10 meters
////    private static int size = 1000;
////    int accI = 0, gyroI = 0;
////
////    private double degerler[][];
////    private int degerlerBoyut = 6000;
////    private int degerlerBoyutStationary = 100;
////    private int degerSayisiStationary;
////    //private int baslangic;
////    private int degerSayisi;
////    private int atlamaMiktari;
////    private int r;
////    private static int instanceIndex;
////
////    private int stationaryCount;
////    private int walkingCount;
////
////    Location mLastLocation;
////    //    private GoogleApiClient mGoogleApiClient;
//////    private LocationRequest mLocationRequest;
////    private String lat, lon, alt, speed, accuracy;
////
////    private Date date;
////
////    float accX;
////    float gyroX;
////    float accY;
////    float gyroY;
////    float accZ;
////    float gyroZ;
////    float magX;
////    float magY;
////    float magZ;
////    float[] accValues;
////    float[] magValues;
////    float[] gyroValues;
////    long timeInMs;
////
////    OutputStreamWriter outputStreamWriter;
////    BufferedWriter bw;
////
////    LocationManager locationManager;
////    LocationListener locationListener;
////
////    Timer mTimer;
////
////    PowerManager powerManager;
////    PowerManager.WakeLock wakeLock;
////
////    private String TAG = this.getClass().getSimpleName();
////    private WindowManager mWindowManager;
////    private LinearLayout touchLayout;
////    private WindowManager wm;
////    private LinearLayout ll;
////    private TextView tv;
////
////    private Thread t;
////
////    private String journeyFilePath;
////
////
////    public static void setWalkingThresholdStddev(float walkingThresholdStddev) {
////        ServiceSensorRead.walkingThresholdStddev = walkingThresholdStddev;
////    }
////
////    public static void setStationaryThresholdStddev(float stationaryThresholdStddev) {
////        ServiceSensorRead.stationaryThresholdStddev = stationaryThresholdStddev;
////    }
////
////    @Override
////    public void onCreate() {
////        super.onCreate();
////
////        //dateCreated = dateFormatFileName.format(new Date());
////
////        //Get reference to SensorManager
////        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
////
////        //Get reference to Accelerometer
////        mAccelerometer = mSensorManager
////                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
////        //Get reference to Gyroscope
////        mGyroscope = mSensorManager
////                .getDefaultSensor(Sensor.TYPE_GYROSCOPE);
////        //Get reference to Magnetometer
////        mMagnetometer = mSensorManager
////                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
////
////        // Acquire a reference to the system Location Manager
////        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
////
////        // Define a listener that responds to location updates
////        locationListener = new LocationListener() {
////            public void onLocationChanged(Location location) {
////                // Called when a new location is found by the network location provider.
////                lat = String.valueOf(location.getLatitude());
////                lon = String.valueOf(location.getLongitude());
//////                accuracy = String.valueOf(location.getAccuracy());
//////                alt = String.valueOf(location.getAltitude());
//////                speed = String.valueOf(location.getSpeed());
////            }
////
////            public void onStatusChanged(String provider, int status, Bundle extras) {
////            }
////
////            public void onProviderEnabled(String provider) {
////            }
////
////            public void onProviderDisabled(String provider) {
////            }
////        };
////
////        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
////        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
////                "SensorReadServiceWakelock");
////
////        degerler = new double[12][this.degerlerBoyut];
////
////        //baslangic=0;
////        degerSayisi=0;
////        //atlamaMiktari = 3600;
////        r=0;
////        instanceIndex = 0;
////        stationaryCount = 0;
////        degerSayisiStationary = 0;
////
////    }
////
////
//////    synchronized void buildGoogleApiClient() {
//////        mGoogleApiClient = new GoogleApiClient.Builder(this)
//////                .addConnectionCallbacks(this)
//////                .addOnConnectionFailedListener(this)
//////                .addApi(LocationServices.API)
//////                .build();
//////    }
////
////    private void showNotification() {
////        //Intent notificationIntent = new Intent(this, MainActivityOld.class);
////
////        Intent notificationIntent = new Intent(this, MainActivityOld.class);
////        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
////        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
////                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
////                notificationIntent, 0);
////
//////        Intent previousIntent = new Intent(this, ForegroundService.class);
//////        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
//////        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
//////                previousIntent, 0);
//////
//////        Intent playIntent = new Intent(this, ServiceSensorRecord.class);
//////        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
//////        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
//////                playIntent, 0);
//////
//////        Intent nextIntent = new Intent(this, ForegroundService.class);
//////        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
//////        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
//////                nextIntent, 0);
////
////        Bitmap icon = BitmapFactory.decodeResource(getResources(),
////                R.mipmap.ic_launcher);
////
////        Notification notification = new NotificationCompat.Builder(this)
////                .setContentTitle("Mini Sensor Module")
////                .setTicker("Mini Sensor Module")
////                .setContentText("Sensor Recording")
////                .setSmallIcon(R.mipmap.ic_launcher)
////                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
////                .setContentIntent(pendingIntent)
////                .setOngoing(true)
//////                .addAction(android.R.drawable.ic_media_previous, "Previous",
//////                        ppreviousIntent)
//////                .addAction(android.R.drawable.ic_media_play, "Play",
//////                        pplayIntent);
//////                .addAction(android.R.drawable.ic_media_next, "Next",
//////                        pnextIntent)
////                .build();
////        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
////                notification);
////
////
//<<<<<<< HEAD
////    }
////
////    @Override
////    public void onSensorChanged(SensorEvent event) {
////
////        long actualTime, tmpTime;
////
////        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
////
////            actualTime = System.currentTimeMillis();
////            tmpTime = actualTime - mLastUpdateA;
////
////            //if (tmpTime >= interval_update) {
////            date = new Date();
////            totalTime += tmpTime;
////
////            mLastUpdateA = actualTime;
////
////            accX = event.values[0];
////            accY = event.values[1];
////            accZ = event.values[2];
////
////            accValues = event.values;
////            //}
////
////
////        }
////
////        else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
////
////            actualTime = System.currentTimeMillis();
////            tmpTime = actualTime - mLastUpdateG;
////
////            //if (actualTime - mLastUpdateG >= interval_update) {
////            totalTimeDebug += tmpTime;
////
////            mLastUpdateG = actualTime;
////
////            gyroX = event.values[0];
////            gyroY = event.values[1];
////            gyroZ = event.values[2];
////
////            gyroValues = event.values;
////            //}
////
////        }
////
////        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
////
////            actualTime = System.currentTimeMillis();
////            tmpTime = actualTime - mLastUpdateM;
////
////            //if (actualTime - mLastUpdateG >= interval_update) {
////            totalTimeDebug += tmpTime;
////
////            mLastUpdateM = actualTime;
////
////            magX = event.values[0];
////            magY = event.values[1];
////            magZ = event.values[2];
////
////            magValues = event.values;
////            //}
////
////        }
////
////
////    }
////
//=======
////    }
////
////    @Override
////    public void onSensorChanged(SensorEvent event) {
////
////        long actualTime, tmpTime;
////
////        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
////
////            actualTime = System.currentTimeMillis();
////            tmpTime = actualTime - mLastUpdateA;
////
////            //if (tmpTime >= interval_update) {
////            date = new Date();
////            totalTime += tmpTime;
////
////            mLastUpdateA = actualTime;
////
////            accX = event.values[0];
////            accY = event.values[1];
////            accZ = event.values[2];
////
////            accValues = event.values;
////            //}
////
////
////        }
////
////        else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
////
////            actualTime = System.currentTimeMillis();
////            tmpTime = actualTime - mLastUpdateG;
////
////            //if (actualTime - mLastUpdateG >= interval_update) {
////            totalTimeDebug += tmpTime;
////
////            mLastUpdateG = actualTime;
////
////            gyroX = event.values[0];
////            gyroY = event.values[1];
////            gyroZ = event.values[2];
////
////            gyroValues = event.values;
////            //}
////
////        }
////
////        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
////
////            actualTime = System.currentTimeMillis();
////            tmpTime = actualTime - mLastUpdateM;
////
////            //if (actualTime - mLastUpdateG >= interval_update) {
////            totalTimeDebug += tmpTime;
////
////            mLastUpdateM = actualTime;
////
////            magX = event.values[0];
////            magY = event.values[1];
////            magZ = event.values[2];
////
////            magValues = event.values;
////            //}
////
////        }
////
////
////    }
////
//>>>>>>> origin/master
////
////    @Override
////    public int onStartCommand(Intent intent, int flags, int startId) {
////        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
//////            Log.i(LOG_TAG, "Received Start Foreground Intent ");
////            showNotification();
//////            Toast.makeText(this, "Service Started!", Toast.LENGTH_SHORT).show();
////
////            wakeLock.acquire();
////
////            //String path = Environment.getExternalStorageDirectory().toString() + "/AASensor/";
//////            String path = Environment.getExternalStorageDirectory().toString() + "/VehicleClassification/SensorData/Test";
////
////
////            journeyFilePath = JOURNEY_DATA_FOLDER;
////
////            File directory = new File(journeyFilePath);
////            if (!directory.exists()) {
////                if(!directory.mkdirs()){
////                    Toast.makeText(getBaseContext(),"mkdirs failed",Toast.LENGTH_SHORT).show();
////                }
////            }
////
//////            directory = new File(path, "BSensor_" + dateCreated + ".csv");
////
//////            try {
//////                FileOutputStream fOut = new FileOutputStream(directory);
//////                outputStreamWriter = new OutputStreamWriter(fOut);
//////
//////                bw = new BufferedWriter(outputStreamWriter);
//////                bw.write("ACCELEROMETER X, ACCELEROMETER Y, ACCELEROMETER Z, GYROSCOPE X, GYROSCOPE Y," +
//////                        "GYROSCOPE Z, MAGNETIC FIELD X, MAGNETIC FIELD Y, MAGNETIC FIELD Z," +
//////                        "AZIMUTH (DEG), PITCH (DEG), ROLL (DEG)," +
//////                        "LAT, LON, ALT, SPEEDinKmh, Accuracy, TimeInms, yyyy-MM-dd HH:mm:ss:SSS\n");
//////                //outputStreamWriter.close();
//////            } catch (IOException e) {
//////                Log.e("Exception", "File open failed: " + e.toString());
//////
//////            }
////
////            // Register the listener with the Location Manager to receive location updates
////            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
////                // TODO: Consider calling
////                //    ActivityCompat#requestPermissions
////                // here to request the missing permissions, and then overriding
////                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
////                //                                          int[] grantResults)
////                // to handle the case where the user grants the permission. See the documentation
////                // for ActivityCompat#requestPermissions for more details.
////                //return TODO;
////                Toast.makeText(getBaseContext(),"Please permit GPS",Toast.LENGTH_SHORT);
////            }
////            else{
////                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, interval_GPS_time, interval_GPS_location, locationListener);
////                //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, interval_GPS_time, interval_GPS_location, locationListener);
////            }
////
////            mSensorManager.registerListener(this, mAccelerometer,
////                    SensorManager.SENSOR_DELAY_FASTEST);
////            mSensorManager.registerListener(this, mGyroscope,
////                    SensorManager.SENSOR_DELAY_FASTEST);
////            mSensorManager.registerListener(this, mMagnetometer,
////                    SensorManager.SENSOR_DELAY_FASTEST);
////
////            mLastWrite = mLastUpdateM = mLastUpdateA = mLastUpdateG = System.currentTimeMillis();
////
////
////            TimerTask tt = new TimerTask() {
////                @Override
////                public void run() {
////                    //writeToFile();
////                    sendToClassification();
////                }
////            };
////
//////            TimerTask tt2 = new TimerTask() {
//////                @Override
//////                public void run() {
//////                    //countStationary();
//////                }
//////            };
////
////            mTimer = new Timer();
////            mTimer.scheduleAtFixedRate(tt, interval_write + interval_delay, interval_write/*period*/);
////         //   mTimer.scheduleAtFixedRate(tt2,interval_write+interval_delay,interval_count_stationary);
////
////        }
////
////        else if (intent.getAction().equals(
////                Constants.ACTION.STOPFOREGROUND_ACTION)) {
//////            Log.i(LOG_TAG, "Received Stop Foreground Intent");
////            stopForeground(true);
////            stopSelf();
////        }
////        return START_STICKY; //START_NOT_STICKY; //START_STICKY;
////
////    }
////
////    public synchronized void  countStationary(int stop){ //TODO remove synchronized
////        double accMagnitude;
////        double degerler2[][];
////        degerler2 = this.degerler.clone();
////
////        int i;
////        double accXAvg = 0;
////        double accYAvg = 0;
////        double accZAvg = 0;
////        //double accMagnitudes[] = new double[degerlerBoyutStationary];
////        double accMagnitudeAvg = 0;
////        double stdAccMagnitude = 0;
////        double sum = 0;
////
////        int start = stop - degerlerBoyutStationary;
////
////        for (i=start; i < stop; i++) {
//////            accXAvg += degerler2[3][i];
//////            accYAvg += degerler2[4][i];
//////            accZAvg += degerler2[5][i];
////            //accMagnitudes[i] = Math.sqrt(Math.pow(degerler2[3][i],2)+Math.pow(degerler2[4][i],2)+Math.pow(degerler2[5][i],2));
////            //accMagnitudeAvg += accMagnitudes[i];
////            accMagnitudeAvg += degerler2[0][i];
////        }
//////        accXAvg /= degerlerBoyutStationary;
//////        accYAvg /= degerlerBoyutStationary;
//////        accZAvg /= degerlerBoyutStationary;
//////
//////        accMagnitude = Math.sqrt(Math.pow(accXAvg,2) + Math.pow(accYAvg,2) + Math.pow(accZAvg,2));
//////        if ((accMagnitude >= 9.3) || (accMagnitude <= 10.3)){
//////            stationaryCount++;
//////        }
////
////        accMagnitudeAvg /= degerlerBoyutStationary;
////        for(i=start; i<stop; i++)
////        {
////            sum += Math.pow((degerler2[0][i]-accMagnitudeAvg),2);
////        }
////        stdAccMagnitude = Math.sqrt(sum/(degerlerBoyutStationary-1));
//////        Log.d("VEHCLA-SensorRead","stdAccMagnitude: "+stdAccMagnitude);
////
//////        if (stdAccMagnitude<=walkingThresholdStddev){ // 0.09
//////            stationaryCount++;
//////        }
//////        else if (stdAccMagnitude>=stationaryThresholdStddev){ // >1
//////            walkingCount++;
//////        }
////
////        if (stdAccMagnitude>=walkingThresholdStddev){ // 0.09
////            walkingCount++;
////        }
////        else if (stdAccMagnitude<=stationaryThresholdStddev){ // >1
////            stationaryCount++;
////        }
////
//////        Writer writer;
//////        try {
//////            writer = new BufferedWriter(new OutputStreamWriter(
//////                    new FileOutputStream(JOURNEY_DATA_FOLDER+"1secdebug.csv", true), "utf-8"));
//////            writer.write("stycnt: " + stationaryCount + ", wlkcnt: " + walkingCount + ", stdaccmag: " + stdAccMagnitude + ", start" + start + ", stop: " +stop +"\n");
//////            writer.close();
//////        }catch (IOException e) {
//////            e.printStackTrace();
//////        }
////
////    }
////
////
////    public void sendToClassification() {
////
////        //timeInMs += System.currentTimeMillis() - mLastWrite;
////        long tmpTime = System.currentTimeMillis() - mLastWrite;
////        timeInMs += tmpTime;
////        mLastWrite = System.currentTimeMillis();
////
//////        float azimuth = 0, pitch = 0, roll = 0;
//////        float orientation[] = new float[3];
////
////        dateCreated = newDateFormatFileName.format(new Date());
////
////        journeyFilePath = JOURNEY_DATA_FOLDER + dateCreated + ".txt";
////        //System.out.println(journeyFilePath);
////        File directory = new File(journeyFilePath);
////        try {
////            directory.createNewFile();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////
////
////        if (timeInMs < 100L)
////            return;
////
////        int i;
////        int indis;
////
//////        float R[] = new float[9];
//////        float I[] = new float[9];
//////        if (accValues != null && magValues != null) {
//////            boolean success = SensorManager.getRotationMatrix(R, I, accValues, magValues);
//////            if (success) {
//////                SensorManager.getOrientation(R, orientation);
//////                azimuth = (float) Math.toDegrees(orientation[0]); // orientation contains: azimuth, pitch and roll
//////                pitch = (float) Math.toDegrees(orientation[1]);
//////                roll = (float) Math.toDegrees(orientation[2]);
//////                //azimuth = orientation[0]; // orientation contains: azimuth, pitch and roll
//////                //pitch = orientation[1];
//////                //roll = orientation[2];
//////            }
//////        }
////
////        String loc = lat + " " + lon; //TODO ","
////            if (degerSayisi < this.degerlerBoyut) {
////
////                this.degerler[3][r] = accX;
////                this.degerler[4][r] = accY;
////                this.degerler[5][r] = accZ;
////                this.degerler[6][r] = gyroX;
////                this.degerler[7][r] = gyroY;
////                this.degerler[8][r] = gyroZ;
////                this.degerler[9][r] = magX;
////                this.degerler[10][r] = magY;
////                this.degerler[11][r] = magZ;
////
////                for (i = 0; i < 3; i++) {
////                    indis = (i + 1) * 3;
////                    this.degerler[i][r] = Math.sqrt(Math.pow(this.degerler[indis][r], 2)
////                            + Math.pow(this.degerler[indis+1][r], 2)
////                            + Math.pow(this.degerler[indis+2][r], 2));
////                }
////                r++;
////                r %= this.degerlerBoyut;
////                degerSayisi++;
////                degerSayisiStationary++;
////
////                if ((degerSayisiStationary % degerlerBoyutStationary) == 0) {
////                    Runnable r = new Runnable() {
////                        @Override
////                        public void run() {
////                            countStationary(degerSayisiStationary);
////                        }
////                    };
////                    r.run();
////                    //degerSayisiStationary = 0;
////                }
////
////            } else {
////
////                instanceIndex++;
////
////                degerSayisi = 0; //TODO degerSayisi = 0;
////
////                Log.d("VEHCLA", newDateFormat.format(new Date()) +" starting thread " + "walkStdThold: " + walkingThresholdStddev + " statStdThold: " + stationaryThresholdStddev);
////
////                // send stationaryCount to RunnableIslem
////
////                t = new Thread(new RunnableIslem(instanceIndex, r, degerler, stationaryCount, walkingCount, newDateFormat.format(new Date()), dateFormat.format(new Date()), loc, lon, journeyFilePath, this.getApplicationContext()), "t"+instanceIndex);
////                t.start();
////                stationaryCount = 0;
////                walkingCount = 0;
////                degerSayisiStationary = 0;
////
////            }
////
////        }
////
////
////
////    @Override
////    public void onDestroy() {
////
////        mTimer.cancel();
////        mSensorManager.unregisterListener(this);
////
//////        if(mGoogleApiClient.isConnected()){
//////            LocationServices.FusedLocationApi.removeLocationUpdates(
//////                    mGoogleApiClient, this);
//////            mGoogleApiClient.disconnect();
//////
//////        }
////
////        // Register the listener with the Location Manager to receive location updates
////        // Request permission unnecessary
////        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
////            // TODO: Consider calling
////            //    ActivityCompat#requestPermissions
////            // here to request the missing permissions, and then overriding
////            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
////            //                                          int[] grantResults)
////            // to handle the case where the user grants the permission. See the documentation
////            // for ActivityCompat#requestPermissions for more details.
////            //return TODO;
////            Toast.makeText(getBaseContext(),"Please permit GPS",Toast.LENGTH_SHORT);
////        }
////        else {
////            locationManager.removeUpdates(locationListener);
////        }
////        instanceIndex = 0;
////        wakeLock.release();
////        super.onDestroy();
////    }
////
////
////    @Override
////    public void onAccuracyChanged(Sensor sensor, int accuracy) {
////
////    }
////
////
////    @Nullable
////    @Override
////    public IBinder onBind(Intent intent) {
////        return null;
////    }
////
////}
