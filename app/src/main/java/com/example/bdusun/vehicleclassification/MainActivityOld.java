//package com.example.bdusun.vehicleclassification;
//
//import android.Manifest;
//import android.app.ActivityManager;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.TimeUnit;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.REQUIRED_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FILENAME2;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.CLASSIFICATION_RESULT_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.HEAL_RESULT_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.JOURNEY_DATA_FOLDER;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.LOCATION_DATA_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TEST_FOLDER;
////import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TRAIN_FOLDER;
////import static com.example.bdusun.vehicleclassification.Constants.PERMISSONS.REQUEST_WRITE_EXTERNAL_STORAGE;
//
//// TODO change ask permissions in some methods to ask write permission only: ask only what's required
//// TODO GPS Module, (how to implement if no gps collected, either not allowed by the user or sth else)
//// TODO Hangi task'ın hangi parametreyi nasıl aldığı belli değil. Ayrıca İlerde parametrik de olmaları gerekiyor.
//
//public class MainActivityOld extends AppCompatActivity {
//
//    View clickedButton = null;
//
//    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
//    //final private int REQUEST_CODE_ASK_WRITE_PERMISSION = 123;
//
//    EditText edtWalkingCountThreshold;
//    EditText edtStationaryCountThreshold;
//    EditText edtCombinedCountThreshold;
//    EditText edtWalkingAndStationaryCountThreshold;
//    EditText edtWalkingStddevThreshold;
//    EditText edtStationaryStddevThreshold;
//
//    Button btnSetThresholds;
//
////    private int stationaryCountThreshold = 20;
////    private int walkingCountThreshold = 20;
////    private int walkingAndStationaryCountThreshold = 30;
////    private boolean combinedThresholdCountValue = true;
////    private float walkingStddevThreshold = 2.0f;
////    private float stationaryStddevThreshold = 0.04f;
//
//    private int stationaryCountThreshold;
//    private int walkingCountThreshold;
//    private int walkingAndStationaryCountThreshold;
//    private boolean combinedThresholdCountValue;
//    private float walkingStddevThreshold;
//    private float stationaryStddevThreshold;
//
//    // Used to load the 'native-lib' library on application startup.
//    static {
//        System.loadLibrary("native-lib");
//    }
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main_old);
//
//        Button buttonRecord = (Button) findViewById(R.id.button_record);
//        Button buttonRead = (Button) findViewById(R.id.button_read);
//        Button buttonClassifyAndRecord = (Button) findViewById(R.id.button_classifyAndRecord);
//        //Button buttonArff = (Button) findViewById(R.id.button_arff);
//        //Button buttonClassify = (Button) findViewById(R.id.button_classify);
//        //Button buttonHeal = (Button) findViewById(R.id.button_heal);
//
//        if(isMyServiceRunning(ServiceSensorRecord.class)){
//            buttonRecord.setText("Stop Recording");
//        }
//        if(isMyServiceRunning(ServiceSensorRead.class)){
//            buttonRead.setText("Stop Reading");
//        }
//        if(isMyServiceRunning(ServiceClassifyAndRecord.class)){
//            buttonClassifyAndRecord.setText("Stop ClassifyAndRecord");
//        }
//
//        int hasWriteStoragePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        int hasLocationPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
//        if((hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) ||
//                (hasLocationPermission != PackageManager.PERMISSION_GRANTED)){
//            askForPermissions();
//        }
//
//        edtWalkingCountThreshold = (EditText) findViewById(R.id.editText_walking_threshold_count);
//        edtStationaryCountThreshold = (EditText) findViewById(R.id.editText_stationary_threshold_count);
//        edtCombinedCountThreshold = (EditText) findViewById(R.id.editText_combined_threshold);
//        edtWalkingAndStationaryCountThreshold = (EditText) findViewById(R.id.editText_walkingAndStationary_threshold_count);
//        edtWalkingStddevThreshold = (EditText) findViewById(R.id.editText_walking_threshold_stddev);
//        edtStationaryStddevThreshold = (EditText) findViewById(R.id.editText_stationary_threshold_stddev);
//
//        btnSetThresholds = (Button) findViewById(R.id.button_set_thresholds);
//
//        getThresholdsFromPrefs();
//        setThresholds();
//
//        edtWalkingCountThreshold.setText(walkingCountThreshold+"");
//        edtStationaryCountThreshold.setText(stationaryCountThreshold+"");
//        edtCombinedCountThreshold.setText(combinedThresholdCountValue+"");
//        edtWalkingAndStationaryCountThreshold.setText(walkingAndStationaryCountThreshold+"");
//        edtWalkingStddevThreshold.setText(walkingStddevThreshold+"");
//        edtStationaryStddevThreshold.setText(stationaryStddevThreshold+"");
//
//    }
//
//    public void getThresholdsFromPrefs(){
//        SharedPreferences prefs = getSharedPreferences(
//                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
//
//        boolean defaultCombinedThresholdCountValue = Boolean.parseBoolean(getString(R.string.preference_threshold_combined_value_default));
//        combinedThresholdCountValue = prefs.getBoolean(getString(R.string.preference_threshold_combined_key),defaultCombinedThresholdCountValue);
//
//        int defaultWalkingAndStationaryCountThreshold = Integer.parseInt(getString(R.string.preference_threshold_walkingandstationary_count_value_default));
//        walkingAndStationaryCountThreshold = prefs.getInt(getString(R.string.preference_threshold_walkingandstationary_count_key), defaultWalkingAndStationaryCountThreshold);
//
//        int defaultStationaryCountThreshold = Integer.parseInt(getString(R.string.preference_threshold_stationary_count_value_default));
//        stationaryCountThreshold = prefs.getInt(getString(R.string.preference_threshold_stationary_count_key),defaultStationaryCountThreshold);
//
//        int defaultWalkingCountThreshold = Integer.parseInt(getString(R.string.preference_threshold_walking_count_value_default));
//        walkingCountThreshold = prefs.getInt(getString(R.string.preference_threshold_walking_count_key),defaultWalkingCountThreshold);
//
//        float defaultWalkingStddevThreshold = Float.parseFloat(getString(R.string.preference_threshold_walking_stddev_value_default));
//        walkingStddevThreshold = prefs.getFloat(getString(R.string.preference_threshold_walking_stddev_key),defaultWalkingStddevThreshold);
//
//        float defaultStationaryStddevThreshold = Float.parseFloat(getString(R.string.preference_threshold_stationary_stddev_value_default));
//        stationaryStddevThreshold = prefs.getFloat(getString(R.string.preference_threshold_stationary_stddev_key),defaultStationaryStddevThreshold);
//
//    }
//
//    public void setThresholds(){
//        ServiceSensorRead.setStationaryThresholdStddev(stationaryStddevThreshold);
//        ServiceSensorRead.setWalkingThresholdStddev(walkingStddevThreshold);
//
//        ServiceClassifyAndRecord.setStationaryThresholdStddev(stationaryStddevThreshold);
//        ServiceClassifyAndRecord.setWalkingThresholdStddev(walkingStddevThreshold);
//
//        RunnableIslem.setWalkingCountThreshold(walkingCountThreshold);
//        RunnableIslem.setStationaryCountThreshold(stationaryCountThreshold);
//        RunnableIslem.setCombinedThresholdCountValue(combinedThresholdCountValue);
//        RunnableIslem.setWalkingAndStationaryCountThreshold(walkingAndStationaryCountThreshold);
//    }
//
//    public void buttonSetThresholds(View v){
//        walkingCountThreshold = Integer.parseInt(edtWalkingCountThreshold.getText().toString());
//        stationaryCountThreshold = Integer.parseInt(edtStationaryCountThreshold.getText().toString());
//        combinedThresholdCountValue = Boolean.parseBoolean(edtCombinedCountThreshold.getText().toString());
//        walkingAndStationaryCountThreshold = Integer.parseInt(edtWalkingAndStationaryCountThreshold.getText().toString());
//        walkingStddevThreshold = Float.parseFloat(edtWalkingStddevThreshold.getText().toString());
//        stationaryStddevThreshold = Float.parseFloat(edtStationaryStddevThreshold.getText().toString());
//
//        setThresholds();
//
//        //SharedPreferences sharedPref = getPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
//        SharedPreferences sharedPref = getSharedPreferences(
//                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putInt(getString(R.string.preference_threshold_walking_count_key),walkingCountThreshold);
//        editor.putInt(getString(R.string.preference_threshold_stationary_count_key),stationaryCountThreshold);
//        editor.putBoolean(getString(R.string.preference_threshold_combined_key),combinedThresholdCountValue);
//        editor.putInt(getString(R.string.preference_threshold_walkingandstationary_count_key),walkingAndStationaryCountThreshold);
//        editor.putFloat(getString(R.string.preference_threshold_walking_stddev_key),walkingStddevThreshold);
//        editor.putFloat(getString(R.string.preference_threshold_stationary_stddev_key),stationaryStddevThreshold);
//        editor.apply();
//
//    }
//
//    public void buttonClearPreferences(View v){
//        SharedPreferences sharedPref = getSharedPreferences(
//                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.clear();
//        editor.apply();
//
//        getThresholdsFromPrefs();
//        setThresholds();
//    }
//
//    public void askForPermissions(){
//        List<String> permissionsNeeded = new ArrayList<String>();
//        final List<String> permissionsList = new ArrayList<String>();
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//            permissionsNeeded.add("GPS(ACCESS_FINE_LOCATION)");
//        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
//            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");
//
//        if (permissionsList.size() > 0) {
//            if (permissionsNeeded.size() > 0) {
//                // Need Rationale
//                String message = "You need to grant access to " + permissionsNeeded.get(0);
//                for (int i = 1; i < permissionsNeeded.size(); i++)
//                    message = message + ", " + permissionsNeeded.get(i);
//                showMessageOKCancel(message,
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                ActivityCompat.requestPermissions(MainActivityOld.this,permissionsList.toArray(new String[permissionsList.size()]),
//                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
//                            }
//                        });
//                return;
//            }
//            ActivityCompat.requestPermissions(this,permissionsList.toArray(new String[permissionsList.size()]),
//                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
//            return;
//        }
//    }
//
//    public void createFolders(){
//        createDirectory(SENSOR_DATA_TRAIN_FOLDER);
//        createDirectory(SENSOR_DATA_TEST_FOLDER);
//        createDirectory(REQUIRED_FOLDER);
//        createDirectory(CLASSIFICATION_RESULT_FOLDER);
//        createDirectory(HEAL_RESULT_FOLDER);
//        createDirectory(LOCATION_DATA_FOLDER);
//        createDirectory(JOURNEY_DATA_FOLDER);
//    }
//
//    public void createDirectory(String directoryName){
//        File directory = new File(directoryName);
//        if (!directory.exists()) {
//            if(!directory.mkdirs()){
//                Toast.makeText(getBaseContext(),"mkdir failed "+directory, Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
//    public void buttonHealClicked(){
//
//        //createDirectory(REQUIRED_FOLDER);
//        //bunlar da gerekli sayılmaz
//        createDirectory(CLASSIFICATION_RESULT_FOLDER);
//        createDirectory(HEAL_RESULT_FOLDER);
//
//        String classificationResultPath = CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME;
//        String healResultPath = HEAL_RESULT_FOLDER;
//
//        Log.d("VEHCLA",classificationResultPath);
//        Log.d("VEHCLA",healResultPath);
//
//        TaskHeal runner = new TaskHeal(this.getApplicationContext());
//        //runner.execute(trainPath,testPath,summaryPath,predictionPath); //train dosyasi konumu ve adi, test dosyasi konumu ve adi, output dosyasi konumu ve adi
//        //runner.execute(classificationResultPath,healResultPath);
//        runner.execute();
//
//    }
//
//
//    public void buttonHealClickedWrapper(View v) {
//        clickedButton = v;
//        int hasWriteStoragePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {
//            if(hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED){
//                askForPermissions();
//            } else{
//                buttonHealClicked();
//            }
//        }
//        buttonHealClicked();
//    }
//
//    static RunnableCsv2Degerler rc2d;
//    static boolean csv2degerlerRunning = false;
//    static ExecutorService es = Executors.newFixedThreadPool(1);
//
//    public void buttonCsv2DegerlerClicked(){
//
//        createDirectory(REQUIRED_FOLDER);
//        createDirectory(CLASSIFICATION_RESULT_FOLDER);
//
////        rc2d = new RunnableCsv2Degerler(this.getBaseContext(), 6000, 0.4);
////        thread= new Thread(rc2d, "VEHCLA - thread: csv2degerler");
////        thread.start();
//
//        //ExecutorService es = Executors.newFixedThreadPool(1);
//
//        if (!csv2degerlerRunning){
//            rc2d= new RunnableCsv2Degerler(getBaseContext(), 6000, 0.4);;
//            es = Executors.newFixedThreadPool(1);
//            es.submit(rc2d); // not threads
//            csv2degerlerRunning = true;
//        }
//        else {
//            es.shutdownNow();
//            try {
//                es.awaitTermination(1, TimeUnit.HOURS);
//                csv2degerlerRunning = false;
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//
//    public void buttonCsv2DegerlerClickedWrapper(View v) {
//        clickedButton = v;
//        int hasWriteStoragePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {
//            if(hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED){
//                askForPermissions();
//            } else{
//                buttonCsv2DegerlerClicked();
//            }
//        }
//        buttonCsv2DegerlerClicked();
//    }
//
//
//    public void buttonClearResultsClicked() {
//        //Button button = (Button) clickedButton;
//        // TODO Add option for file name and other parameters
//
//      //  String csvFolder = SENSOR_DATA_TRAIN_FOLDER;
//      //  String arffFolder = REQUIRED_FOLDER;
//
//        createDirectory(SENSOR_DATA_TRAIN_FOLDER);
//        createDirectory(SENSOR_DATA_TEST_FOLDER);
//        createDirectory(REQUIRED_FOLDER);
//        createDirectory(LOCATION_DATA_FOLDER);
//
//        //String locationFolder = LOCATION_DATA_FOLDER;
//
//        File file = new File(CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME);
//        boolean deleted = file.delete();
//        file = new File(CLASSIFICATION_RESULT_FOLDER + CLASSIFICATION_RESULT_FILENAME2);
//        deleted = file.delete();
//
//    }
//
//    public void buttonClearResultsClickedWrapper(View v) {
//        clickedButton = v;
//        int hasWriteStoragePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) {
//            askForPermissions();
//        }
//        buttonClearResultsClicked();
//    }
//
//
//    public void buttonSensorClassifyAndRecordClickedWrapper(View v) {
//
//        clickedButton = v;
//        int hasWriteStoragePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        int hasLocationPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
//        if((hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) ||
//                (hasLocationPermission != PackageManager.PERMISSION_GRANTED)){
//            askForPermissions();
//        } else{
//            buttonSensorClassifyAndRecordClicked();
//        }
//
//    }
//
//    public void buttonSensorClassifyAndRecordClicked() {
//        Button button = (Button) clickedButton;
//        Intent service = new Intent(MainActivityOld.this, ServiceClassifyAndRecord.class);
//        if (!ServiceClassifyAndRecord.IS_SERVICE_RECORD_RUNNING) {
//            createDirectory(JOURNEY_DATA_FOLDER);
//            createDirectory(SENSOR_DATA_TRAIN_FOLDER);
//            service.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
//            ServiceClassifyAndRecord.IS_SERVICE_RECORD_RUNNING = true;
//            button.setText("Stop ClassifyandRecord");
//        } else {
//            service.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
//            ServiceClassifyAndRecord.IS_SERVICE_RECORD_RUNNING = false;
//            button.setText("ClassifyandRecord");
//        }
//        startService(service);
//    }
//
//
//    public void buttonSensorReadClickedWrapper(View v) {
//
//        clickedButton = v;
//        int hasWriteStoragePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        int hasLocationPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
//        if((hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) ||
//                (hasLocationPermission != PackageManager.PERMISSION_GRANTED)){
//            askForPermissions();
//        } else{
//            buttonSensorReadClicked();
//        }
//
//    }
//
//    public void buttonSensorReadClicked() {
//        Button button = (Button) clickedButton;
//        Intent service = new Intent(MainActivityOld.this, ServiceSensorRead.class);
//        if (!ServiceSensorRead.IS_SERVICE_RECORD_RUNNING) {
//            createDirectory(JOURNEY_DATA_FOLDER);
//            service.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
//            ServiceSensorRead.IS_SERVICE_RECORD_RUNNING = true;
//            button.setText("Stop Sensors");
//        } else {
//            service.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
//            ServiceSensorRead.IS_SERVICE_RECORD_RUNNING = false;
//            button.setText("Start Sensors");
//        }
//        startService(service);
//    }
//
//
//
//    public void buttonRecordClickedWrapper(View v) {
//
//        clickedButton = v;
//        int hasWriteStoragePermission = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        int hasLocationPermission = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
//        if((hasWriteStoragePermission != PackageManager.PERMISSION_GRANTED) ||
//                (hasLocationPermission != PackageManager.PERMISSION_GRANTED)){
//            askForPermissions();
//        } else{
//            buttonRecordClicked();
//        }
//    }
//
//
//    public void buttonRecordClicked() {
//        Button button = (Button) clickedButton;
//        Intent service = new Intent(MainActivityOld.this, ServiceSensorRecord.class);
//        if (!ServiceSensorRecord.IS_SERVICE_RECORD_RUNNING) {
//            createDirectory(SENSOR_DATA_TEST_FOLDER);
//            service.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
//            ServiceSensorRecord.IS_SERVICE_RECORD_RUNNING = true;
//            button.setText("Stop Recording");
//        } else {
//            service.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
//            ServiceSensorRecord.IS_SERVICE_RECORD_RUNNING = false;
//            button.setText("Start Recording");
//        }
//        startService(service);
//    }
//
//    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
//        new AlertDialog.Builder(MainActivityOld.this)
//                .setMessage(message)
//                .setPositiveButton("OK", okListener)
//                .setNegativeButton("Cancel", null)
//                .create()
//                .show();
//    }
//
//    private boolean addPermission(List<String> permissionsList, String permission) {
//        if (ContextCompat.checkSelfPermission(this,permission) != PackageManager.PERMISSION_GRANTED) {
//            permissionsList.add(permission);
//            // Check for Rationale Option
//            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,permission))
//                return false;
//        }
//        return true;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        switch (requestCode) {
//            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
//            {
//                Map<String, Integer> perms = new HashMap<String, Integer>();
//                // Initial
//                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
//                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
//                // Fill with results
//                for (int i = 0; i < permissions.length; i++)
//                    perms.put(permissions[i], grantResults[i]);
//                // Check for ACCESS_FINE_LOCATION
//                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                    // All Permissions Granted
//                    createFolders();
//                } else if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                    // Permission Denied
//                    Toast.makeText(MainActivityOld.this, "Write to External Storage must be allowed, exiting", Toast.LENGTH_LONG)
//                            .show();
//                    System.exit(-1);
//                } else if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO
//                }
//            }
//            break;
//
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
//    }
//
//    private boolean isMyServiceRunning(Class<?> serviceClass) {
//        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if (serviceClass.getName().equals(service.service.getClassName())) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        // Save the user's current game state
////        savedInstanceState.putInt(STATE_SCORE, mCurrentScore);
////        savedInstanceState.putInt(STATE_LEVEL, mCurrentLevel);
//
//        // Always call the superclass so it can save the view hierarchy state
//        super.onSaveInstanceState(savedInstanceState);
//    }
//
//    @Override
//    public void onDestroy() { // writeToFile the remaining data in the buffer to file
//
//        super.onDestroy();
//
//    }
//
//
//    /**
//     * A native method that is implemented by the 'native-lib' native library,
//     * which is packaged with this application.
//     */
//    //public native String stringFromJNI();
//    //public static native double processFile(String jcsvPath, String jarffFile, String jlocationFile);
////    public static native int islem(double degerlerJ[][], int baslangic, int argSayisi, double sonuclar[][]);
//
//    //public static native double processFile(String jcsvPath, String jarffFile);
////    public static native void healFile(String jclassifcationResultPath, String joutputResultPath);
//}