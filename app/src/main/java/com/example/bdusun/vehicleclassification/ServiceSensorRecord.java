//package com.example.bdusun.vehicleclassification;
//
///**
// * Created by bdusun on 21/01/17.
// */
//
//import android.Manifest;
//import android.app.Notification;
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.Context;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.os.IBinder;
//import android.os.PowerManager;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.app.NotificationCompat;
//import android.util.Log;
//import android.view.WindowManager;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Locale;
//import java.util.Timer;
//import java.util.TimerTask;
//
//import static com.example.bdusun.vehicleclassification.Constants.FILE.SENSOR_DATA_TEST_FOLDER;
//import static com.example.bdusun.vehicleclassification.Constants.FILE.precision;
//
//
//public class ServiceSensorRecord extends Service implements SensorEventListener {
//
//    public static SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");
//    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS
//
//    public static boolean IS_SERVICE_RECORD_RUNNING = false;
//
//    private SensorManager mSensorManager;
//    private Sensor mAccelerometer;
//    private Sensor mGyroscope;
//    private Sensor mMagnetometer;
//
//    private long mLastWrite;
//    private long mLastUpdateA;
//    private long mLastUpdateG;
//    private long mLastUpdateM;
//    private long totalTime;
//    private long totalTimeDebug;
//
//    private String textToSet = "";
//    //private String precision = "%.3f";
//    private String dateCreated;
//
//    private static long interval_write = 10L; // 50 //yazma skligi //normalde alttaki ile ayni olmali ama bu sekilde farkli deger gelme ihitmali artiyor
//    private static long interval_update = 5L; // in ms 10=> 100Hz //frequency
//    private static long interval_delay = 0L;
//    private static int interval_GPS = 10000; // 10 sec GPS
//    private static int FATEST_INTERVAL = 5000; // 5 sec GPS
//    private static int DISPLACEMENT = 10; // 10 meters
//    private static int size = 1000;
//    int accI = 0, gyroI = 0;
//
//
//
//    Location mLastLocation;
//    //    private GoogleApiClient mGoogleApiClient;
////    private LocationRequest mLocationRequest;
//    private String lat, lon, alt, speed, accuracy;
//
//    private Date date;
//
//    float accX;
//    float gyroX;
//    float accY;
//    float gyroY;
//    float accZ;
//    float gyroZ;
//    float magX;
//    float magY;
//    float magZ;
//    float[] accValues;
//    float[] magValues;
//    long timeInMs;
//
//    OutputStreamWriter outputStreamWriter;
//    BufferedWriter bw;
//
//    LocationManager locationManager;
//    LocationListener locationListener;
//
//    Timer mTimer;
//
//    PowerManager powerManager;
//    PowerManager.WakeLock wakeLock;
//
//    private String TAG = this.getClass().getSimpleName();
//    private WindowManager mWindowManager;
//    private LinearLayout touchLayout;
//    private WindowManager wm;
//    private LinearLayout ll;
//    private TextView tv;
//
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
//        dateCreated = dateFormatFileName.format(new Date());
//
//        //Get reference to SensorManager
//        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//
//        //Get reference to Accelerometer
//        mAccelerometer = mSensorManager
//                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        //Get reference to Gyroscope
//        mGyroscope = mSensorManager
//                .getDefaultSensor(Sensor.TYPE_GYROSCOPE);
//        //Get reference to Magnetometer
//        mMagnetometer = mSensorManager
//                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
//
//        // Acquire a reference to the system Location Manager
//        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
//
//        // Define a listener that responds to location updates
//        locationListener = new LocationListener() {
//            public void onLocationChanged(Location location) {
//                // Called when a new location is found by the network location provider.
//                lat = String.valueOf(location.getLatitude());
//                lon = String.valueOf(location.getLongitude());
//                accuracy = String.valueOf(location.getAccuracy());
//                alt = String.valueOf(location.getAltitude());
//                speed = String.valueOf(location.getSpeed());
//            }
//
//            public void onStatusChanged(String provider, int status, Bundle extras) {}
//
//            public void onProviderEnabled(String provider) {}
//
//            public void onProviderDisabled(String provider) {}
//        };
//
//        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
//        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//                "SensorRecordServiceWakelock");
//
//    }
//
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
////            Log.i(LOG_TAG, "Received Start Foreground Intent ");
//            showNotification();
//
//            wakeLock.acquire();
//
//            String path = SENSOR_DATA_TEST_FOLDER;
//
//            File directory = new File(path);
//            if (!directory.exists()) {
//                if(!directory.mkdirs()){
//                    Toast.makeText(getBaseContext(),"mkdirs failed",Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            directory = new File(path, "BSensor_" + dateCreated + ".csv");
//
//            try {
//                FileOutputStream fOut = new FileOutputStream(directory);
//                outputStreamWriter = new OutputStreamWriter(fOut);
//
//                bw = new BufferedWriter(outputStreamWriter);
//                bw.write("ACCELEROMETER X, ACCELEROMETER Y, ACCELEROMETER Z, GYROSCOPE X, GYROSCOPE Y," +
//                        "GYROSCOPE Z, MAGNETIC FIELD X, MAGNETIC FIELD Y, MAGNETIC FIELD Z," +
//                        "AZIMUTH (DEG), PITCH (DEG), ROLL (DEG)," +
//                        "LAT, LON, ALT, SPEEDinKmh, Accuracy, TimeInms, yyyy-MM-dd HH:mm:ss:SSS\n");
//                //outputStreamWriter.close();
//            } catch (IOException e) {
//                Log.e("Exception", "File open failed: " + e.toString());
//
//            }
//
//            // Register the listener with the Location Manager to receive location updates
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                //return TODO;
//                Toast.makeText(getBaseContext(),"Please permit GPS",Toast.LENGTH_SHORT);
//            }
//            else{
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locationListener);
//            }
//
//
//            mSensorManager.registerListener(this, mAccelerometer,
//                    SensorManager.SENSOR_DELAY_FASTEST);
//            mSensorManager.registerListener(this, mGyroscope,
//                    SensorManager.SENSOR_DELAY_FASTEST);
//            mSensorManager.registerListener(this, mMagnetometer,
//                    SensorManager.SENSOR_DELAY_FASTEST);
//
//            mLastWrite = mLastUpdateM = mLastUpdateA = mLastUpdateG = System.currentTimeMillis();
//
//
//            TimerTask tt = new TimerTask() {
//                @Override
//                public void run() {
//                    writeToFile();
//                }
//            };
//
//            mTimer = new Timer();
//            mTimer.scheduleAtFixedRate(tt, interval_write + interval_delay, interval_write/*period*/);
//
//
//        }
//
////         else if (intent.getAction().equals(Constants.ACTION.PLAY_ACTION)) {
//////            Log.i(LOG_TAG, "Clicked Play");
////
////            Toast.makeText(this, "Clicked Play!", Toast.LENGTH_SHORT).show();
////        }
//
//        else if (intent.getAction().equals(
//                Constants.ACTION.STOPFOREGROUND_ACTION)) {
////            Log.i(LOG_TAG, "Received Stop Foreground Intent");
//            stopForeground(true);
//            stopSelf();
//        }
//        return START_STICKY;
//    }
//
//
//    private void showNotification() {
//        Intent notificationIntent = new Intent(this, MainActivityOld.class);
//        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//                notificationIntent, 0);
//
////        Intent previousIntent = new Intent(this, ForegroundService.class);
////        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
////        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
////                previousIntent, 0);
////
////        Intent playIntent = new Intent(this, ServiceSensorRecord.class);
////        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
////        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
////                playIntent, 0);
////
////        Intent nextIntent = new Intent(this, ForegroundService.class);
////        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
////        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
////                nextIntent, 0);
//
//        Bitmap icon = BitmapFactory.decodeResource(getResources(),
//                R.mipmap.ic_launcher);
//
//        Notification notification = new NotificationCompat.Builder(this)
//                .setContentTitle("Mini Sensor Module")
//                .setTicker("Mini Sensor Module")
//                .setContentText("Sensor Recording")
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
//                .setContentIntent(pendingIntent)
//                .setOngoing(true)
////                .addAction(android.R.drawable.ic_media_previous, "Previous",
////                        ppreviousIntent)
////                .addAction(android.R.drawable.ic_media_play, "Play",
////                        pplayIntent);
////                .addAction(android.R.drawable.ic_media_next, "Next",
////                        pnextIntent)
//                .build();
//        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
//                notification);
//
//
//    }
//
//    @Override
//    public void onSensorChanged(SensorEvent event) {
//
//        long actualTime, tmpTime;
//
//
//        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//
//            actualTime = System.currentTimeMillis();
//            tmpTime = actualTime - mLastUpdateA;
//
//            //if (tmpTime >= interval_update) {
//            date = new Date();
//            totalTime += tmpTime;
//
//            mLastUpdateA = actualTime;
//
//            accX = event.values[0];
//            accY = event.values[1];
//            accZ = event.values[2];
//
//            accValues = event.values;
//            //}
//
//        }
//
//        else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
//
//            actualTime = System.currentTimeMillis();
//            tmpTime = actualTime - mLastUpdateG;
//
//            //if (actualTime - mLastUpdateG >= interval_update) {
//            totalTimeDebug += tmpTime;
//
//            mLastUpdateG = actualTime;
//
//            gyroX = event.values[0];
//            gyroY = event.values[1];
//            gyroZ = event.values[2];
//            //}
//
//        }
//
//        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
//
//            actualTime = System.currentTimeMillis();
//            tmpTime = actualTime - mLastUpdateM;
//
//            //if (actualTime - mLastUpdateG >= interval_update) {
//            totalTimeDebug += tmpTime;
//
//            mLastUpdateM = actualTime;
//
//            magX = event.values[0];
//            magY = event.values[1];
//            magZ = event.values[2];
//
//            magValues = event.values;
//            //}
//
//        }
//
//    }
//
//
//    public void writeToFile(){
//
//        //timeInMs += System.currentTimeMillis() - mLastWrite;
//        long tmpTime = System.currentTimeMillis() - mLastWrite;
//        timeInMs += tmpTime;
//        mLastWrite = System.currentTimeMillis();
//
//        float azimuth=0, pitch=0, roll=0;
//        float orientation[] = new float[3];
//
//        if (timeInMs<100L)
//            return;
//
//        float R[] = new float[9];
//        float I[] = new float[9];
//        if (accValues != null && magValues != null) {
//            boolean success = SensorManager.getRotationMatrix(R, I, accValues, magValues);
//            if (success) {
//                SensorManager.getOrientation(R, orientation);
//                azimuth = (float) Math.toDegrees(orientation[0]); // orientation contains: azimuth, pitch and roll
//                pitch = (float) Math.toDegrees(orientation[1]);
//                roll = (float) Math.toDegrees(orientation[2]);
//                //azimuth = orientation[0]; // orientation contains: azimuth, pitch and roll
//                //pitch = orientation[1];
//                //roll = orientation[2];
//            }
//        }
//
//        try {
//
//            //timeInMs+","+dateFormat.format(date[i])+"\n");
//            bw.write(String.format(Locale.ENGLISH,precision,accX) + "," +
//                    String.format(Locale.ENGLISH,precision,accY) + "," +
//                    String.format(Locale.ENGLISH,precision,accZ) +"," +
//
//                    String.format(Locale.ENGLISH,precision,gyroX)+"," +
//                    String.format(Locale.ENGLISH,precision,gyroY)+"," +
//                    String.format(Locale.ENGLISH,precision,gyroZ)+ "," +
//
//                    String.format(Locale.ENGLISH,precision,magX)+"," +
//                    String.format(Locale.ENGLISH,precision,magY)+"," +
//                    String.format(Locale.ENGLISH,precision,magZ)+ "," +
//
//                    String.format(Locale.ENGLISH,precision,azimuth)+"," +
//                    String.format(Locale.ENGLISH,precision,pitch)+"," +
//                    String.format(Locale.ENGLISH,precision,roll)+ "," +
//
//                    lat + "," + lon + "," + alt + "," +
//                    speed + "," + accuracy + "," +
//
//                    tmpTime+","+dateFormat.format(new Date())+"\n");
//
////                System.out.println(accX[i] + "," +accY[i] + "," + accZ[i] +","+
////                        gyroX[i]+","+ gyroY[i]+","+ gyroZ[i]+ ","+
////                        timeInMs[i]+","+timeInMsDebug[i]+" "+"\n");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    @Override
//    public void onDestroy() {
//
//        mTimer.cancel();
//        mSensorManager.unregisterListener(this);
//
//        // close the file
//        try {
//            bw.close();
//            outputStreamWriter.close();
//            Toast.makeText(getBaseContext(), "File saved successfully!",
//                    Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // Register the listener with the Location Manager to receive location updates
//        // Request permission unnecessary
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            //return TODO;
//            Toast.makeText(getBaseContext(),"Please permit GPS",Toast.LENGTH_SHORT);
//        }
//        else {
//            locationManager.removeUpdates(locationListener);
//        }
//        wakeLock.release();
//        super.onDestroy();
//    }
//
//
//    @Override
//    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//
//    }
//
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//}
