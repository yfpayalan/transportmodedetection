package com.example.bdusun.vehicleclassification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.HorizontalBarChart;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


//@SuppressWarnings("deprecation")
/**
 * Created by bariscan on 26/04/2017.
 */


public class StatisticsActivity extends AppCompatActivity{

    private static final String TAG = "StatisticsActivity";

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS

    private ClassificationDBHelper clsResDBHelper;
    private SQLiteDatabase db;
    //buraktan
//    private ViewPager mViewPager;

//    BarChart classificationBarChart;

    private TabHost classificationResults_tabHost;
    private TabHost.TabSpec tab1;
    private TabHost.TabSpec tab2;
    private TabHost.TabSpec tab3;

    private HorizontalBarChart haftalik_barChart;
    private HorizontalBarChart aylik_barChart;
    private HorizontalBarChart yillik_barChart;

    private  Calendar cal = Calendar.getInstance();

    ArrayList<BarEntry> barEntries;
    private ArrayList<BarEntry> valueSet1;

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        this.clsResDBHelper = new ClassificationDBHelper(getApplicationContext());
        this.db = clsResDBHelper.getReadableDatabase();

        this.valueSet1 = new ArrayList<>();


        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        Toolbar toolbar = (Toolbar) findViewById(R.id.statistics_toolbar);
//        setSupportActionBar(toolbar);
        initializeLayout();
//
//
//
        this.createGraph(haftalik_barChart, "Haftalık", Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 1);
        this.createGraph(aylik_barChart, "Aylık", Calendar.DAY_OF_MONTH, 1);
        this.createGraph(yillik_barChart, "Yıllık", Calendar.DAY_OF_YEAR, 1);

//        this.generateResults("2017-05-05 10:45:46", "2018-05-05 10:45:46");


//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
//        cal.clear(Calendar.MINUTE);
//        cal.clear(Calendar.SECOND);
//        cal.clear(Calendar.MILLISECOND);

//// get start of this week in milliseconds
//        cal.set(Calendar.DAY_OF_MONTH, 1);
//        Log.d(TAG, dateFormat.format(cal.getTime()));


//        Log.d(TAG, String.valueOf(Calendar.DAY_OF_WEEK));
//        Log.d(TAG, String.valueOf(Calendar.HOUR_OF_DAY));
//        Log.d(TAG, String.valueOf(dateFormat.getCalendar().getFirstDayOfWeek()
//        ));
//        Log.d(TAG, String.valueOf(dateFormat.));


// start of the next week

//        cal.add(Calendar.WEEK_OF_YEAR, 1);

//        System.out.println("Start of the next week:   " + cal.getTime());
//        System.out.println("... in milliseconds:      " + cal.getTimeInMillis());

        //listView = (ListView) findViewById(R.id.listview_gunluk);

    }


    public void createGraph(HorizontalBarChart barChart, String description, int timeUnit, int firstDay){
        BarData data = new BarData(getXAxisValues(), getDataSet(timeUnit, firstDay));
        barChart.setData(data);
        barChart.setDescription(description);
        barChart.animateXY(2000, 2000);
        barChart.setAutoScaleMinMaxEnabled(true);
        barChart.invalidate();
        barChart.getLegend().setEnabled(false);
    }


    private void initializeLayout(){
        this.haftalik_barChart = (HorizontalBarChart) findViewById(R.id.aracSayisi_haftalik_barChart);
        this.aylik_barChart = (HorizontalBarChart) findViewById(R.id.aracSayisi_aylik_barChart);
        this.yillik_barChart = (HorizontalBarChart) findViewById(R.id.aracSayisi_yillik_barChart);

        this.classificationResults_tabHost = (TabHost) findViewById(R.id.classificationResults_TabHost);

        this.classificationResults_tabHost.setup();

        TabHost.TabSpec spec;

//        listView = (ListView) findViewById(R.id.listview_gunluk);
//
//
//        //Tab 1
//        TabHost.TabSpec spec = this.classificationResults_tabHost.newTabSpec("Günluk");
//        spec.setContent(R.id.gunluk_tab4);
//        spec.setIndicator("Günlük");
//        this.classificationResults_tabHost.addTab(spec);

        //Tab 1
        spec = this.classificationResults_tabHost.newTabSpec("Haftalık");
        spec.setContent(R.id.haftalik_tab1);
        spec.setIndicator("Weekly");
        this.classificationResults_tabHost.addTab(spec);

        //Tab 2
        spec = this.classificationResults_tabHost.newTabSpec("Aylık");
        spec.setContent(R.id.aylik_tab2);
        spec.setIndicator("Monthly");
        this.classificationResults_tabHost.addTab(spec);

        //Tab 3
        spec = this.classificationResults_tabHost.newTabSpec("Yıllık");
        spec.setContent(R.id.yillik_tab3);
        spec.setIndicator("Yearly");
        this.classificationResults_tabHost.addTab(spec);
    }
//

    private ArrayList<BarDataSet> getDataSet(int timeUnit, int firstDay) {
        ArrayList<BarDataSet> dataSets = null;



        this.generateResults(this.generateBeginDate(timeUnit, firstDay));

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Brand 1");
        barDataSet1.setColor(Color.rgb(0, 155, 0));
//        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Brand 2");
//        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
//        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Walking");
        xAxis.add("Car");
        xAxis.add("Bus");
        xAxis.add("Tram");
        xAxis.add("Metro");
        xAxis.add("Marmaray");
        xAxis.add("Ferry");
        xAxis.add("Airplane");
        xAxis.add("Stationary");

        return xAxis;
    }


    public void generateResults(String beginDate){
        String[] projection = {
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT,
//                FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT
                "COUNT(*)"
        };




//        Select * from users where created_at between '2017-05-05 10:45:46' and '2017-05-05 11:46:19'
        String selection = FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME + "  BETWEEN ? AND ?";
        String selecttionArgs[] = {
                beginDate,
                dateFormat.format(new Date())
        };


//        String []selectArg = {"1"};
        Cursor cursor = db.query(
                FeedClassificationContract.FeedResult.TABLE_NAME,
                projection,
                selection,
                selecttionArgs,
//                null,
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT,//TODO: burası değişebilir parametre olarak gelir bence
                null,
                FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT
        );

        List itemIds = new ArrayList<>();
        int values[] = new int[7];
        int i;
        for (i = 0; i < 7; i++){
            values[i] = 0;
        }

        while (cursor.moveToNext()){
//            int itemId = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT));
            int countResult = cursor.getInt(cursor.getColumnIndex("COUNT(*)"));
//            itemIds.add(itemId);
            Log.d(TAG, "count Result: " + countResult);
            int result = cursor.getInt(cursor.getColumnIndex(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_HEAL_RESULT));
            Log.d(TAG, "result: " + result);
            values[result] += countResult;

        }
        cursor.close();
        valueSet1.clear();
        for (i = 0; i < 7; i++){
            this.addNewValue(i, (float) values[i]);
        }


    }

    public String generateBeginDate(int timeUnit, int firstDay){
        cal.set(Calendar.HOUR_OF_DAY, 0); // ! clear would not reset the hour of day !
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

// get start of this week in milliseconds
        cal.set(timeUnit, firstDay);
//        cal.set(timeUnit, cal.getFirstDayOfWeek() + 1);
////        Log.d(TAG, );
//        System.out.println("Start of this week:       " + cal.getTime());
//        System.out.println("... in milliseconds:      " + cal.getTimeInMillis());
        return dateFormat.format(cal.getTime());
    }


    public void addNewValue(int indis, float value){
        BarEntry v1e1 = new BarEntry(value, indis);
        valueSet1.add(v1e1);
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.db != null){
            this.db.close();
        }
    }


}
