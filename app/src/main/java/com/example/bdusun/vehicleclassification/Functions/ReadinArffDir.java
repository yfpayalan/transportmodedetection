package com.example.bdusun.vehicleclassification.Functions;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.bdusun.vehicleclassification.ClassInfo;
import com.example.bdusun.vehicleclassification.ModelCreateActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.example.bdusun.vehicleclassification.Constants.FILE.ARFF_FOLDER;
import static com.example.bdusun.vehicleclassification.MainActivity.heal;

/**
 * Created by bariscan on 28/12/2017.
 */

public class ReadinArffDir extends AsyncTask<String, String, String>{
    private static final String TAG = "ReadinArffDir";

    private ProgressDialog progressDialog;
    public static final int PROGRESS_BAR_TYPE = 0;
    private String date;
    private File file;

    private boolean isDownloaded;
    ModelCreateActivity mContext;
    private List<ClassInfo> classInfoList;

    private static int argSayisi = 12;
    private static int ozellikSayisi = 29;


    private int allNumOfResults[];
    private int totalResults;
    private int islenenResults;



    public ReadinArffDir(Context mContext, String date, List<ClassInfo> classInfoList) {
        this.mContext = (ModelCreateActivity) mContext;
        this.date = date;
        this.classInfoList = classInfoList;
        progressDialog = new ProgressDialog(this.mContext);
        progressDialog.setMessage("Okunuyor");
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
        isDownloaded = false;
        this.islenenResults = 0;
//        this.progressDialog = ((MainActivity) mContext).getProgressDialog();


    }



    private void generateClassInfoList(){
        List<String> filenames = getFilenames(ARFF_FOLDER, this.date);
        int i;
        int numOfFiles = filenames.size();
        this.allNumOfResults = new int[numOfFiles];
        Log.d(TAG, "numOfFiles: " + numOfFiles);

        this.totalResults = this.calculateNumOfResults(filenames);

        for(i = 0; i < numOfFiles; i++){
            this.addFromFileToClassInfoList(filenames.get(i), this.allNumOfResults[i]);
        }

    }


    private int calculateNumOfResults(List<String> filenames){

        int numOfResults = 0;
        int totalResults = 0;
        int i;
        int numOfFiles = filenames.size();
        File file;
        BufferedReader reader = null;
        for (i = 0; i < numOfFiles; i++){
            numOfResults = 0;
            try {
                file = new File(ARFF_FOLDER + filenames.get(i));

                reader = new BufferedReader(new FileReader(file));
                String satir = reader.readLine();

                while (satir!=null) {
                    numOfResults++;
                    satir = reader.readLine();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            totalResults += numOfResults;
            this.allNumOfResults[i] = numOfResults;
        }


        return totalResults;

    }

    private void addFromFileToClassInfoList(String fileName, int numOfResults){
        int i, j, k;
        double features[][];
        int toplamOzellikSayisi = argSayisi*ozellikSayisi;
        int results[];
        int indis = 0;

        int indexOfClass;
        String dateTime[];
//        int numOfResults;
        ClassInfo classInfo = new ClassInfo();
        Scanner scanner;


//        try {
//            results = healFile(fileName);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }



        try {




            results = new int[numOfResults];
            dateTime = new String[numOfResults];
            features = new double[numOfResults][argSayisi*ozellikSayisi];
//            Log.d(TAG, "readAFileAndInsertSQL: " + fileName);
            scanner = new Scanner(new File(ARFF_FOLDER + fileName));
            scanner.useDelimiter(" ");

            String tmpS;
            String tmpS2;
            int tmpI;
            for (k = 0; k < numOfResults; k++){
//                Log.d(TAG, "islenen: " + this.islenenResults + " total: " + this.totalResults);
                publishProgress("" + (this.islenenResults*100)/this.totalResults);
                //TODO: burada debug yapılacak
                //ozellikleri alıyor.
                for(indis=0; indis<toplamOzellikSayisi; indis++) {

                    features[k][indis] = Double.parseDouble(scanner.next());
//                        Log.d(TAG, "nextDouble: " + scanner.next());
//                    indis++;
                }

                results[k] = scanner.nextInt();

                dateTime[k] = scanner.nextLine();
                this.islenenResults++;

//                indis = 0;

            }

            healFile(results);

            for (k = 0; k < numOfResults; k++){

                indexOfClass = results[k];
                if (classInfo.getIndexOfClass() != indexOfClass){
                    if (classInfo.getIndexOfClass() != -1){
                        this.classInfoList.add(classInfo);
                    }

                    classInfo = new ClassInfo();
                    classInfo.setDateTime(dateTime[k]);
                }
                classInfo.addClass(features[k]);
                classInfo.setIndexOfClass(indexOfClass);
            }
            while(scanner.hasNextLine()){

            }

            if (classInfo.getIndexOfClass() != -1){
                this.classInfoList.add(classInfo);
            }

            scanner.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void healFile(int results[]) throws FileNotFoundException {
        int numOfResult = results.length;
//        int i, j, k;
//        int results[];
//        Scanner scanner = new Scanner(new File(ARFF_FOLDER + fileName));
//        scanner.useDelimiter(" ");
//        while (scanner.hasNextLine()){
//            scanner.nextLine();
//            numOfResult++;
//        }
//
//        results = new int[numOfResult];
//
//        scanner = new Scanner(new File(ARFF_FOLDER + fileName));
//        scanner.useDelimiter(" ");
//
//        for (k = 0; k < numOfResult; k++){
//            for(i=0; i<argSayisi; i++) {
//                for(j=0; j<ozellikSayisi; j++){
//                    scanner.next();
//                }
//            }
//            results[k] = scanner.nextInt();
//            scanner.nextLine();
//        }

        heal(results, numOfResult, 12);
//        return results;
    }


    private List<String> getFilenames(String directory, String whichDate) {

        List<String> files = new ArrayList<String>();
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles!=null){
            for (File file:listOfFiles) {
                String filename = file.getName();
//                Log.d(TAG,"fileName: " + filename);
//                Log.d(TAG,"whichDate: " + whichDate);
                if (filename.startsWith(whichDate)) {
                    //System.out.println("File " + listOfFiles[i].getName());
                    files.add(filename);
                }
            }
            return files;
        }
        else
            return null;
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d(TAG, "Başladı");
        generateClassInfoList();


//        this.isDownloaded = true;


        return null;
    }



    @Override
    protected void onPostExecute(String file_url) {
        super.onPostExecute(file_url);

        Log.d(TAG, "işlem sonlandı");
        this.progressDialog.dismiss();
        mContext.loadAdaptor();




    }

    @Override
    protected void onProgressUpdate(String... values) {
//        Log.d(TAG, values[0]);
        this.progressDialog.setProgress((int) Double.parseDouble(values[0]));
    }
}
