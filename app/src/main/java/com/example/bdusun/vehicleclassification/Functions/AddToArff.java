package com.example.bdusun.vehicleclassification.Functions;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.bdusun.vehicleclassification.ClassInfo;
import com.example.bdusun.vehicleclassification.ModelCreateActivity;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.example.bdusun.vehicleclassification.Constants.FILE.REQUIRED_FOLDER;

/**
 * Created by bariscan on 30/12/2017.
 */

public class AddToArff extends AsyncTask<String, String, String> {
    private static final String TAG = "AddToArff";

    private SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");

    private ProgressDialog progressDialog;
    public static final int PROGRESS_BAR_TYPE = 0;

    ModelCreateActivity mContext;
    private List<ClassInfo> classInfoList;

    private String resultNames[] = {
            "walking",
            "minibus",
            "car",
            "bus",
            "tram",
            "metroK",
            "metroH",
            "marmaray",
            "metrobus",
            "ferry",
            "airplane",
            "stationary"
    };






    public AddToArff(Context mContext, List<ClassInfo> classInfoList) {
        this.classInfoList = classInfoList;
        this.mContext = (ModelCreateActivity) mContext;
        progressDialog = new ProgressDialog(this.mContext);
        progressDialog.setMessage("Arff'ye ekleniyor..");
//        progressDialog.setIndeterminate(false);
//        progressDialog.setMax(100);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
//        this.progressDialog = ((MainActivity) mContext).getProgressDialog();


    }








    @Override
    protected String doInBackground(String... params) {
        Log.d(TAG, "Başladı");
//        Log.d(TAG, "create Model");

        Writer writer = null;
        int i;

        String allArffPath = REQUIRED_FOLDER + "all.arff";
        String oldArffPath = REQUIRED_FOLDER + "all_" + dateFormatFileName.format(new Date()) + ".arff";

        try {
            this.mContext.fileClone(allArffPath, oldArffPath);
        } catch (IOException e) {
            e.printStackTrace();
        }



//

        for (ClassInfo classInfo:this.classInfoList){
            Log.d(TAG, "sinif indisi: " + classInfo.getIndexOfClass());
            Log.d(TAG, "ekle_onay: " + classInfo.isEkleOnay());
            if (classInfo.isEkleOnay()){
                Log.d(TAG, "tarih: " + classInfo.getDateTime());
//TODO: burda stationary ve walking ise ekleme şeklinde bir kontrol yapabiliiiz hatta yapmak zorunda olabiliriz
                try {
                    writer = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(allArffPath, true), "utf-8"));

                    for (double [] results:classInfo.getFeatures()){
                        for (double result:results){
                            writer.write(result + ",");
                        }
                        writer.write(resultNames[classInfo.getIndexOfClass()]+"\n");
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }


//        this.isDownloaded = true;


        return null;
    }



    @Override
    protected void onPostExecute(String file_url) {
        super.onPostExecute(file_url);

        Log.d(TAG, "işlem sonlandı");
        this.progressDialog.dismiss();



    }

//    @Override
//    protected void onProgressUpdate(String... values) {
//        this.progressDialog.setProgress((int) Double.parseDouble(values[0]));
//    }
}