package com.example.bdusun.vehicleclassification;

import android.provider.BaseColumns;

/**
 * Created by bariscan on 01/05/2017.
 */

public class FeedClassificationContract {
    private FeedClassificationContract(){}

    public static class FeedResult implements BaseColumns{
        public static final String TABLE_NAME = "classification_results";
        public static final String COLUMN_NAME_RESULT = "result";
        public static final String COLUMN_NAME_COMBINE_RESULT = "combine_result";
        public static final String COLUMN_NAME_HEAL_RESULT = "heal_result";
        public static final String COLUMN_NAME_COMBINE_HEAL_RESULT = "combine_heal_result";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_DATE_TIME = "created_at";
    }
}
