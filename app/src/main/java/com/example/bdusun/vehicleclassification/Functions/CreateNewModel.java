package com.example.bdusun.vehicleclassification.Functions;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.bdusun.vehicleclassification.ModelCreateActivity;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;

import static com.example.bdusun.vehicleclassification.Constants.FILE.REQUIRED_FOLDER;

/**
 * Created by bariscan on 29/12/2017.
 */

public class CreateNewModel extends AsyncTask<String, String, String> {
    private static final String TAG = "CreateNewModel";

    private SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");

    private ProgressDialog progressDialog;
    public static final int PROGRESS_BAR_TYPE = 0;

    ModelCreateActivity mContext;






    public CreateNewModel(Context mContext) {
        this.mContext = (ModelCreateActivity) mContext;
        progressDialog = new ProgressDialog(this.mContext);
        progressDialog.setMessage("Model Oluşturuluyor..");
//        progressDialog.setIndeterminate(false);
//        progressDialog.setMax(100);
//        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
//        this.progressDialog = ((MainActivity) mContext).getProgressDialog();


    }








    @Override
    protected String doInBackground(String... params) {
        Log.d(TAG, "Başladı");
        Log.d(TAG, "create Model");

        String modelPath = REQUIRED_FOLDER + "all.model";
//        String yeni = REQUIRED_FOLDER + "yeni.model";
//        Log.d(TAG, yeni);
        String oldModelPath = REQUIRED_FOLDER + "all_" + this.dateFormatFileName.format(new Date()) + ".model";
        try {
            this.mContext.fileClone(modelPath,oldModelPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader reader = null;
        int argSayisi = 12;
        int ozellikSayisi = 29;
        try {
            reader = new BufferedReader(new FileReader(REQUIRED_FOLDER + "all.arff"));
            Instances train = new Instances(reader);
            train.setClassIndex(argSayisi*ozellikSayisi);

            reader.close();
//            Log.d(TAG, "train: " + train.get(0));

            RandomForest rf = new RandomForest();
            rf.buildClassifier(train);



//            Log.d(TAG, "model: " + rf);
            ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream(modelPath));
            oos.writeObject(rf);
            oos.flush();
            oos.close();

//            weka.core.SerializationHelper.write(yeni, rf);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


//        this.createModel_Button.setText("Model Oluşturuldu");
//        this.createModel_Button.setClickable(true);
//        Toast.makeText(this,"Model oluşturuldu",Toast.LENGTH_LONG).show();
        Log.d(TAG, "create Model END");


//        this.isDownloaded = true;


        return null;
    }



    @Override
    protected void onPostExecute(String file_url) {
        super.onPostExecute(file_url);

        Log.d(TAG, "işlem sonlandı");
        this.progressDialog.dismiss();



    }

//    @Override
//    protected void onProgressUpdate(String... values) {
//        this.progressDialog.setProgress((int) Double.parseDouble(values[0]));
//    }
}
