package com.example.bdusun.vehicleclassification;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import weka.classifiers.Classifier;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import static com.example.bdusun.vehicleclassification.Constants.FILE.SOURCE_PATH;
import static com.example.bdusun.vehicleclassification.Constants.FILE.TRAIN_PATH;
import static com.example.bdusun.vehicleclassification.MainActivity.islem;

/**
 * Created by bdusun on 22/02/17.
 */

public class RunnableIslem implements Runnable {
    private static final String TAG = "RunnableIslem";


    private String combineResultNames[] = {
            "Walking",
//            "Minibus",
            "Car",
            "Bus",
            "Tram",
//            "Hafif Raylı Metro",
            "Metro",
            "Marmaray",
//            "Metrobus",
            "Ferry",
            "Airplane",
            "Stationary"
    };

    public static String forModelCreatingPath;

    //private long beginTime = System.currentTimeMillis();
    private int baslangic;
    private double degerler[][];
    private static int argSayisi = 12;
    private static int ozellikSayisi = 29;
    private int instanceIndex;
    public static int stationaryCountThreshold = 20;
    public static int walkingCountThreshold = 20;
    public static int walkingAndStationaryCountThreshold = 30;
    public static boolean combinedThresholdCountValue = true;

    public static SimpleDateFormat dateFormatFileName = new SimpleDateFormat("yyyyMMdd_HHmmss");
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS"); // YYYY-MM-DD HH-MI-SS_SSS
    public static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    private ConverterUtils.DataSource source = null;
    private Instances instances = null;
    private Classifier cls = null;
    private Writer writer = null;
    private Writer writerToCreateModel = null;
    private String timeStamp;
    private String sqlTimeStamp;
    private String journeyFilePath;
    private String location;
    private String lat;
    private String lon;

    private int stationaryCount;
    private int walkingCount;

    private Context mContext;

    // DEBUG
    private Writer writer2 = null;
    private String journeyFilePath2;

    public static void setStationaryCountThreshold(int stationaryCountThreshold) {
        RunnableIslem.stationaryCountThreshold = stationaryCountThreshold;
    }

    public static void setWalkingCountThreshold(int walkingCountThreshold) {
        RunnableIslem.walkingCountThreshold = walkingCountThreshold;
    }

    public static void setWalkingAndStationaryCountThreshold(int walkingAndStationaryCountThreshold) {
        RunnableIslem.walkingAndStationaryCountThreshold = walkingAndStationaryCountThreshold;
    }

    public static void setCombinedThresholdCountValue(boolean combinedThresholdCountValue) {
        RunnableIslem.combinedThresholdCountValue = combinedThresholdCountValue;
    }

    public RunnableIslem(  Context context) {


        try {
/*
            File train = new File(TRAIN_PATH);
            File sourceFile = new File(SOURCE_PATH);
            if (! train.exists() )
                throw new Resources.NotFoundException();
            if (! sourceFile.exists() )
                throw new Resources.NotFoundException();

            cls = (Classifier) weka.core.SerializationHelper.read(TRAIN_PATH);
            //cls = (Classifier) weka.core.SerializationHelper.read(getResources().openRawResource(R.raw.cd271116ov1.model));
            source = new ConverterUtils.DataSource(SOURCE_PATH);
            instances = source.getDataSet();

            instances.setClassIndex(argSayisi*ozellikSayisi);
            this.mContext = context;
*/
        }
        catch (Resources.NotFoundException nfe)
        {
            Toast.makeText(context,"TRAIN_PATH or SOURCE_PATH not created !",Toast.LENGTH_SHORT);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setParamaters(int instanceIndex, int baslangic, double degerler[][], int stationaryCount, int walkingCount, String timeStamp, String sqlTimeStamp, String lat, String lon, String journeyFilePath){
        this.journeyFilePath = journeyFilePath;
        this.baslangic = baslangic;
        this.degerler = degerler;
        this.instanceIndex = instanceIndex;
        this.timeStamp = timeStamp;
        this.sqlTimeStamp = sqlTimeStamp;

        this.lat = lat;
        this.lon = lon;
        this.location = lat + " " + lon;
        this.stationaryCount = stationaryCount;
        this.walkingCount = walkingCount;
    }

    @Override
    public void run() {

        double instanceValues[][] = new double[argSayisi][ozellikSayisi];
        int i,j,sayac;
        int result;
        String classificationResult = null;

        String thresholdClassificationResult = "VEHICULAR";

        try {
            writerToCreateModel = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(forModelCreatingPath, true), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

//        if (stationaryCount > stationaryCountThreshold) {
//            result = 11;
//            islem(degerler.clone(), baslangic, argSayisi, instanceValues);
//        }
//        else if (walkingCount > walkingCountThreshold) {
//            result = 12;
//            islem(degerler.clone(), baslangic, argSayisi, instanceValues);
//        }
        if (stationaryCount + walkingCount >= walkingAndStationaryCountThreshold) {
            if (stationaryCount>walkingCount){
                result = 11;
            }
            else {
                result = 12;
            }
        }
        else {
            result = islem(degerler.clone(), baslangic, argSayisi, instanceValues);
        }


        int thresholdIndis = -1;

        switch (result) {

            case -1:
                //classify
                thresholdIndis = -1;
                break;
            case 10:
                //stationary
                classificationResult = "STATIONARY";
                thresholdClassificationResult = classificationResult;
                thresholdIndis = 11; //10
                break;
            case 11:
                //stationary
                classificationResult = "STATIONARY2";
                thresholdClassificationResult = classificationResult;
                thresholdIndis = 11;//10
                break;
            case 12:
                //stationary
                classificationResult = "WALKING2";
                thresholdClassificationResult = classificationResult;
                thresholdIndis = 0;
                break;

            case 0:
                //walking
                classificationResult = "WALKING";
                thresholdClassificationResult = classificationResult;
                thresholdIndis = 0;
                break;
            default:
                //error
                classificationResult = "error wrong class: " + result;
                thresholdClassificationResult = classificationResult;
                break;
        }

        //bu case -1'de olacak, debug amacli burda
        Instance instance = new DenseInstance(argSayisi*ozellikSayisi+1);
        instance.setDataset(instances);
        sayac = 0;
        for(i=0; i<argSayisi; i++) {
            for(j=0; j<ozellikSayisi; j++){
                try {
                    writerToCreateModel.write(instanceValues[i][j] + " ");
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                Log.d(TAG, "" + instanceValues[i][j]);
                instance.setValue(sayac, instanceValues[i][j]);
                sayac++;
            }
        }
        instance.setValue(sayac, "bus");
        try {
            result = (int) cls.classifyInstance(instance);
            classificationResult = instance.classAttribute().value(result);
        } catch (Exception e) {
            e.printStackTrace();
        }




        int combineResult;
        if (thresholdIndis == -1){
            combineResult = this.findCombineResult(result);
            try {
                writerToCreateModel.write(result + " ");
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.addToSQL(result, combineResult);
//            Log.d("RunnableIslem", "-1");

            Intent intent = new Intent("resim");
            intent.putExtra("result", combineResult);

//                intent.putExtra("coordinates", location.getLongitude() + "  " + location.getLatitude());
            mContext.sendBroadcast(intent);
//            MainActivity.cls_ViewPagerAdaptor.addImages(result);
        }else {
            try {
                writerToCreateModel.write(thresholdIndis + " ");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (thresholdIndis == 0){
                combineResult = 0;
            }else {
                combineResult = 8;
            }

            this.addToSQL(thresholdIndis, combineResult);

            Intent intent = new Intent("resim");
            intent.putExtra("result", combineResult);
//                intent.putExtra("coordinates", location.getLongitude() + "  " + location.getLatitude());
            mContext.sendBroadcast(intent);
//            Log.d("RunnableIslem", String.valueOf(thresholdIndis));
//            MainActivity.cls_ViewPagerAdaptor.addImages(thresholdIndis);
        }

        try {
            writerToCreateModel.write(timeFormat.format(new Date())+ "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                writerToCreateModel.close();
            } catch (Exception ex) {/*ignore*/}
        }
//        this.addToSQL(result, Integer.parseInt(null), Double.parseDouble(lat), Double.parseDouble(lon));
        //TODO debugClassificationResult classificationResult ile yer degistirecek
        try {
//            System.out.println(journeyFilePath);
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(journeyFilePath, true), "utf-8"));

//            writer.write(
//                    timeStamp
//                    + ","
//                    + thresholdClassificationResult
//                    + ","
//                    + classificationResult
//                    + ":"
//                    + result
//                    + ","
//                    + location
//                    + "\n");

            writer.write(
                    timeStamp
                            + " "
                            + thresholdClassificationResult
                            + " "
                            + thresholdIndis
                            + " "
                            + classificationResult
                            + " "
                            + result
                            + " "
                            + combineResultNames[combineResult]
                            + " "
                            + combineResult
                            + " "
                            + location
                            + "\n");

//            Log.d("VEHCLA", timeStamp + "," + thresholdClassificationResult + ","
//                    + classificationResult + ":" + result + "," + location);

            Log.d("VEHCLA", timeStamp + "," + thresholdClassificationResult + ","
                    + classificationResult + ":" + result + "," + location + ",W&SCntTh: " +walkingAndStationaryCountThreshold + ",walkCnt: " + walkingCount + ",statCnt: " +stationaryCount);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {/*ignore*/}
        }

        //DEBUG
        this.journeyFilePath2 = journeyFilePath + "_debug.csv";
        //System.out.println(journeyFilePath2);
        File directory = new File(journeyFilePath2);
        try {
            if (directory.createNewFile()) {
                try {

                    writer2 = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(journeyFilePath2, true), "utf-8"));

                    writer2.write(
                            "TSTAMP, MIN, MAX, AVG, STDSAPMA, USTORTFR, ORTORTFR, " +
                                    "ALTORTFR, ZCTH, STYCNT, WLKCNT, THLABEL, LABEL:num" + "\n");

                    //Log.d("VEHCLA", timeStamp + " - " + classificationResult);

                } catch (IOException ex) {
                    ex.printStackTrace();
                } finally {
                    try {
                        writer2.close();
                    } catch (Exception ex) {/*ignore*/}
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            writer2 = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(journeyFilePath2, true), "utf-8"));

            //aritmetikOrtalama,stdSapma,ustMax_Ortalama_Freq,ortMax_Ortalama_Freq,altMax_Ortalama_Freq
            // ACC_MAGNITUDE
            writer2.write(timeStamp+",");
            writer2.write(instanceValues[0][4]+","); //MIN
            writer2.write(instanceValues[0][5]+","); //MAX
            writer2.write(instanceValues[0][7]+","); //AVG
            writer2.write(instanceValues[0][12]+","); //STD
            writer2.write(instanceValues[0][26]+","); //ustMax_Ortalama_Freq
            writer2.write(instanceValues[0][27]+","); //ortMax_Ortalama_Freq
            writer2.write(instanceValues[0][28]+","); //altMax_Ortalama_Freq
            double zcth = ((instanceValues[0][27] * 100)/(instanceValues[0][26]+instanceValues[0][27]+instanceValues[0][28]));
            writer2.write(zcth+",");
            writer2.write(stationaryCount+",");
            writer2.write(walkingCount+",");
            writer2.write(thresholdClassificationResult+",");
            writer2.write(classificationResult+":"+result+",");
            //writer2.write(classificationResult+",");
            writer2.write("\n");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                writer2.close();
            } catch (Exception ex) {/*ignore*/}
        }

    }


    public long addToSQL(int result, int combineResult){
        Log.d("RunnableIslem", "addToSql 1");
        ClassificationDBHelper clsResDBHelper = new ClassificationDBHelper(this.mContext);
        SQLiteDatabase db = clsResDBHelper.getWritableDatabase();
        Log.d("RunnableIslem", "addToSql 2");


        ContentValues values = new ContentValues();
        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_RESULT, result);
        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_COMBINE_RESULT, combineResult);
//        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_HEAL_RESULT, healResult);


        Log.d("RunnableIslem", "Lat: " + this.lat);
        Log.d("RunnableIslem", "Lon: " + this.lon);
        if (this.lat == null){
            Log.d("RunnableIslem", "empty: ");
        }
        if ((this.lat != null) && this.lon != null){
            Log.d("RunnableIslem", "addToSql 3xx");
            values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_LATITUDE, Double.parseDouble(this.lat));
            values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_LONGITUDE, Double.parseDouble(this.lon));
        }
        values.put(FeedClassificationContract.FeedResult.COLUMN_NAME_DATE_TIME, this.sqlTimeStamp);
        //TODO: paremetrik olmalı mı bu tarihler fix mi olmalı böyle olsada olur mu
        Log.d("RunnableIslem", "addToSql 3");
        long newRowId = db.insert(FeedClassificationContract.FeedResult.TABLE_NAME, null, values);
        Log.d("RunnableIslem", "addToSql 4");
        db.close();
        return newRowId;
    }

    public int findCombineResult(int result){

        int combineResult = result;

        if (result == 2){
            combineResult = 1;
        }else if ((result == 1) || (result == 3) || (result == 8)){
            combineResult = 2;
        }else if (result == 4){
            combineResult = 3;
        }else if ((result == 5) || (result == 6)){
            combineResult = 4;
        }else if (result == 7){
            combineResult = 5;
        }else if (result == 9){
            combineResult = 6;
        }else if (result == 10){
            combineResult = 7;
        }else if (result == 11){
            combineResult = 8;
        }
        return combineResult;
    }


    public static void setForModelCreatingPath(String forModelCreatingPath) {
        RunnableIslem.forModelCreatingPath = forModelCreatingPath;
    }
}
