//
//  main.c
//  LabelMixedData
//
//  Created by Burak Düşün on 22/10/16.
//  Copyright © 2016 burakdusun. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#define LINESIZE 10000
#define LABELSIZE 50
#define TIMESIZE 50
#define DATESIZE 11
#define FNAMESIZE 500
#define MINUTES 2 //How many minutes to skip
#define MTOS 60 // minute to seconds
#define FREQUENCY 100 //Frequency in Hz
#define ERROR_FILEOPEN 33
#define CSVOFFSET 4


int isCSV(char *dosyaAdi)
{
    int i = (int) strlen(dosyaAdi);
    i--;
    if ((dosyaAdi[i]=='v')&&(dosyaAdi[--i]=='s')&&(dosyaAdi[--i]=='c')&&(dosyaAdi[--i]=='.'))
    { return 1; }
    else
    { return 0; }
}

void strRemoveFromLast(char *string, int offset) {
    int i = (int) strlen(string);
    string[i-offset] = string[i];
}

int removeNewline(char *string) {
    int i=0;
    while(string[i++]);
    string[i-2] = string[i-1];
    return i-2; // return size of the new word including null
}

void fileCheck(FILE *file){
    if (file == NULL) {
        printf("Could not open file!\n");
        exit(ERROR_FILEOPEN);
    }
}

void extractDate(char *filename, char *date) {
    int i=0,j=0;
    while(filename[i++] != '_');
    while(filename[i++] != '_');
    while(filename[i] != '_'){
        date[j] = filename[i];
        i++;
        j++;
    }
    date[j] = '\0';
}




int main(int argc, const char * argv[]) {
    
    int skip=2;
    int currentSkip = 0;
    int folNum=0;
    int nextLabelBeginTimeSize;
    
    int labelCount;
    int i,j,k;
    int commaCount;
    
    int rowCount;
 
    char *folderNumber;
    char firstLine[LINESIZE];
    char labelColumn[] = ", LABEL\n";
    char nextLabelBeginTime[TIMESIZE];
    char label[LABELSIZE];
    char filename[FNAMESIZE];
    char filenameRead[FNAMESIZE];
    char filenameWrite1[FNAMESIZE];
    char filenameWrite2[FNAMESIZE];
    char line[LINESIZE];
    char foldername[FNAMESIZE];
    char *fileNumber;
    char date[TIMESIZE];
    FILE *fRead = NULL;
    FILE *fWrite1 = NULL;
    FILE *fWrite2 = NULL;
    
    DIR *dp;
    struct dirent *ep;
    
    mkdir("labelled",0777);
    mkdir("unlabelled",0777);
    
    dp = opendir ("./unlabelled/");
    if (dp == NULL)
    { exit(1); }
    
    skip = 0;
    printf("Enter amount of time to skip after each label change ");
    scanf("%d", &skip);
    skip *= FREQUENCY * MTOS;
    
    while ((ep = readdir (dp))) {
        
        printf("%s\n",ep->d_name);
        
        if (isCSV(ep->d_name)) {
            
            
            printf("Enter total num of labels: ");
            scanf("%d",&labelCount);
            
            strcpy(filename, ep->d_name);
            strRemoveFromLast(filename, CSVOFFSET);

            strcpy(filenameRead,"unlabelled/");
            strcat(filenameRead,ep->d_name);
            
            strcpy(filenameWrite1,"labelled/");
            strcat(filenameWrite1,filename);
            strcat(filenameWrite1,"_labelled.csv");
            
            fRead = fopen(filenameRead, "r");
            fWrite1 = fopen(filenameWrite1, "w");
            
            
            i=1;
            printf("Enter label #%d: ",i);
            scanf("%s",label);
            //labelSize = strlen(label);
            
            strcpy(filenameWrite2,"labelled/");
            strcat(filenameWrite2,filename);
            strcat(filenameWrite2,"/");
            strcat(filenameWrite2,label);
            strcat(filenameWrite2,"_");
            strcat(filenameWrite2,label);
            strcat(filenameWrite2,"_");
            //strcat(filenameWrite2,label);
            extractDate(filename, date);
            strcat(filenameWrite2, date);
            strcat(filenameWrite2,"_");
            asprintf(&folderNumber, "%02d", folNum);
            strcat(filenameWrite2, folderNumber);
            strcat(filenameWrite2,"_");
            asprintf(&fileNumber, "%02d", i);
            strcat(filenameWrite2, fileNumber);
            strcat(filenameWrite2, ".csv");
            
            //printf("fname2=%s\n\n",filenameWrite2);
            
            
            strcpy(foldername, "labelled/");
            strcat(foldername, filename);
            //printf("%s", foldername);
            mkdir(foldername, 0777);
            

            fWrite2 = fopen(filenameWrite2, "w");
            fileCheck(fWrite2);
            
            printf("Enter beginning time of the next label (HH:MM) ");
            scanf("%s",nextLabelBeginTime);
            printf("labelBeginTime %s\n",nextLabelBeginTime);
            nextLabelBeginTimeSize = (int) strlen(nextLabelBeginTime);
            
            
            i=0;
            commaCount=0;
            fgets(line, LINESIZE, fRead);
            while(line[i++]){
                if(line[i] == ',')
                    commaCount++;
            }
            //printf("Commacount=%d\n",commaCount);
            strcpy(firstLine, line);
            printf("%s",firstLine);
            fputs(firstLine, fWrite2);
            removeNewline(line);
            strcat(line,labelColumn);
            fputs(line,fWrite1);
            
            i=1;
            rowCount=0;
            
            while( (fgets(line, LINESIZE, fRead)) ) {
                
                rowCount++;
                k=0;
                j=0;
                while(k<commaCount){
                    if(line[j++] == ','){
                        k++;
                    }
                }
                k=0;
                j += DATESIZE;
                
                while(line[j] == nextLabelBeginTime[k]){
                    j++;
                    k++;
                }
                
                if (k == nextLabelBeginTimeSize) {
                    
                    fclose(fWrite2);
                    
                    printf("time %s matched!\n",nextLabelBeginTime);
                    i++;
                    printf("Enter label #%d: ",i);
                    scanf("%s",label);
                    //labelSize = strlen(label);
                    
                    strcpy(filenameWrite2,"labelled/");
                    strcat(filenameWrite2,filename);
                    strcat(filenameWrite2,"/");
                    strcat(filenameWrite2,label);
                    strcat(filenameWrite2,"_");
                    strcat(filenameWrite2,label);
                    strcat(filenameWrite2,"_");
                    //strcat(filenameWrite2,label);
                    extractDate(filename, date);
                    strcat(filenameWrite2, date);
                    strcat(filenameWrite2,"_");
                    asprintf(&folderNumber, "%02d", folNum);
                    strcat(filenameWrite2, folderNumber);
                    strcat(filenameWrite2,"_");
                    asprintf(&fileNumber, "%02d", i);
                    strcat(filenameWrite2, fileNumber);
                    strcat(filenameWrite2, ".csv");
                    
                    
                    fWrite2 = fopen(filenameWrite2, "w");
                    fputs(firstLine, fWrite2);
                    
                    if (i<labelCount){
                        printf("i=%d, Enter beginning time of the next label (HH:MM) ",i);
                        scanf("%s",nextLabelBeginTime);
                        nextLabelBeginTimeSize = (int) strlen(nextLabelBeginTime);
                    }
                    else{
                        nextLabelBeginTime[0] = '!'; // kalan datanin tamami labellanmali
                    }
                    
                    while( ((++currentSkip)<skip) && (fgets(line, LINESIZE, fRead)) );
                    currentSkip=0;
                    
                }
                
                fputs(line, fWrite2);
                
                removeNewline(line);
                strcat(line, ", ");
                strcat(line, label);
                strcat(line, "\n");
                
                fputs(line, fWrite1);
                
                
            }
            
            folNum++;

            fclose(fRead);
            fclose(fWrite1);
            fclose(fWrite2);
            
            
        }
        
    }
    
    
    return 0;
    
}
