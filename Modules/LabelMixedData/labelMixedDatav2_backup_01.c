//
//  main.c
//  LabelMixedData
//
//  Created by Burak Düşün on 22/10/16.
//  Copyright © 2016 burakdusun. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#define LINESIZE 10000
#define LABELSIZE 50
#define TIMESIZE 50
#define DATESIZE 11
#define FNAMESIZE 500
//#define MINUTES 2 //How many minutes to skip
#define MTOS 60 // minute to seconds
#define FREQUENCY 100 //Frequency in Hz
#define ERROR_FILEOPEN 33
#define CSVOFFSET 4


int isCSV(char *dosyaAdi)
{
    int i = (int) strlen(dosyaAdi);
    i--;
    if ((dosyaAdi[i]=='v')&&(dosyaAdi[--i]=='s')&&(dosyaAdi[--i]=='c')&&(dosyaAdi[--i]=='.'))
    { return 1; }
    else
    { return 0; }
}

void strRemoveFromLast(char *string, int offset) {
    int i = (int) strlen(string);
    string[i-offset] = string[i];
}

int removeNewline(char *string) {
    int i=0;
    while(string[i++]);
    string[i-2] = string[i-1];
    return i-2; // return size of the new word including null
}

void ptrCheck(void *ptr){
    if (ptr == NULL) {
        printf("Could not open file!\n");
        exit(ERROR_FILEOPEN);
    }
}

void extractDate(char *filename, char *date) {
    int i=0,j=0;
    while(filename[i++] != '_');
    while(filename[i++] != '_');
    while(filename[i] != '_'){
        date[j] = filename[i];
        i++;
        j++;
    }
    date[j] = '\0';
}



int main(int argc, const char * argv[]) {
    
    int folNum=0;
    int labelBeginTimeSize;
    int labelEndTimeSize;
    
    int labelCount;
    int i,j,k;
    int commaCount;
    
    int skip;
    int write;
    
    char *folderNumber;
    char firstLine[LINESIZE];
    char labelColumn[] = ", LABEL\n";
    char labelBeginTime[TIMESIZE];
    char labelEndTime[TIMESIZE];
    char label[LABELSIZE];
    char filename[FNAMESIZE];
    char filenameRead[FNAMESIZE];
    char filenameWrite1[FNAMESIZE];
    char filenameWrite2[FNAMESIZE];
    char line[LINESIZE];
    char foldername[FNAMESIZE];
    char *fileNumber;
    char date[TIMESIZE];
    FILE *fRead = NULL;
    FILE *fWrite1 = NULL;
    FILE *fWrite2 = NULL;
    
    DIR *dp;
    struct dirent *ep;
    
    mkdir("labelled",0777);
    mkdir("unlabelled",0777);
    
    dp = opendir ("./unlabelled/");
    ptrCheck(dp);
    
    while ((ep = readdir (dp))) {
        
        printf("%s\n",ep->d_name);
        
        if (isCSV(ep->d_name)) {
            
            printf("Enter total num of labels: ");
            scanf("%d",&labelCount);
            
            strcpy(filename, ep->d_name);
            strRemoveFromLast(filename, CSVOFFSET);
            
            strcpy(filenameRead,"unlabelled/");
            strcat(filenameRead,ep->d_name);
            
            strcpy(filenameWrite1,"labelled/");
            strcat(filenameWrite1,filename);
            strcat(filenameWrite1,"_labelled.csv");
            
            fRead = fopen(filenameRead, "r");
            ptrCheck(fread);
            fWrite1 = fopen(filenameWrite1, "w");
            ptrCheck(fWrite1);
            
            i=0;
            commaCount=0;
            fgets(line, LINESIZE, fRead);
            while(line[i++]){
                if(line[i] == ',')
                    commaCount++;
            }
            
            //printf("Commacount=%d\n",commaCount);
            strcpy(firstLine, line);
            printf("%s",firstLine);
            removeNewline(line);
            strcat(line,labelColumn);
            fputs(line,fWrite1);
            
            for(i=1; i<=labelCount; i++){
                
                fgets(line, LINESIZE, fRead);
                
                printf("Enter label #%d: ",i);
                scanf("%s",label);
                
                strcpy(filenameWrite2,"labelled/");
                strcat(filenameWrite2,filename);
                strcat(filenameWrite2,"/");
                strcat(filenameWrite2,label);
                strcat(filenameWrite2,"_");
                strcat(filenameWrite2,label);
                strcat(filenameWrite2,"_");
                //strcat(filenameWrite2,label);
                extractDate(filename, date);
                strcat(filenameWrite2, date);
                strcat(filenameWrite2,"_");
                asprintf(&folderNumber, "%02d", folNum);
                strcat(filenameWrite2, folderNumber);
                free(folderNumber);
                strcat(filenameWrite2,"_");
                asprintf(&fileNumber, "%02d", i);
                strcat(filenameWrite2, fileNumber);
                free(fileNumber);
                strcat(filenameWrite2, ".csv");
                
                strcpy(foldername, "labelled/");
                strcat(foldername, filename);
                //printf("%s", foldername);
                mkdir(foldername, 0777);
                
                fWrite2 = fopen(filenameWrite2, "w");
                ptrCheck(fWrite2);
                
                fputs(firstLine, fWrite2);
                
                printf("i=%d, Enter BEGINING time of the label (HH:MM or HH:MM:SS or HH:MM:SS:sss) ", i);
                scanf("%s",labelBeginTime);
                labelBeginTimeSize = (int) strlen(labelBeginTime);
                
                skip=1;
                while(skip) {
                    
                    k=0;
                    j=0;
                    while(k<commaCount){
                        if(line[j++] == ','){
                            k++;
                        }
                    }
                    k=0;
                    j += DATESIZE;
                    
                    while(line[j] == labelBeginTime[k]){
                        j++;
                        k++;
                    }
                    
                    if( k==labelBeginTimeSize){
                        skip = 0;
                    }
                    else{
                        fgets(line, LINESIZE, fRead);
                    }
                }
                
                printf("i=%d, Enter END time of the label (HH:MM) ", i);
                scanf("%s",labelEndTime);
                labelEndTimeSize = (int) strlen(labelEndTime);
                
                write=1;
                while(write) {
                    
                    fgets(line, LINESIZE, fRead);
                    
                    k=0;
                    j=0;
                    while(k<commaCount){
                        if(line[j++] == ','){
                            k++;
                        }
                    }
                    k=0;
                    j += DATESIZE;
                    
                    while(line[j] == labelEndTime[k]){
                        j++;
                        k++;
                    }
                    
                    fputs(line, fWrite2);
                    
                    removeNewline(line);
                    strcat(line, ", ");
                    strcat(line, label);
                    strcat(line, "\n");
                    
                    fputs(line, fWrite1);
                    
                    if (k == labelEndTimeSize) {
                        printf("time %s matched labelendtime!\n\n", labelEndTime);
                        fclose(fWrite2);
                        write=0;
                    }
                    
                }
                
            }
            
            folNum++;
            
            fclose(fRead);
            fclose(fWrite1);
            
        }
        
    }
    
    
    return 0;
    
}
